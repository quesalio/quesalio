package com.resultados.quesalio.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.resultados.quesalio.R
import com.resultados.quesalio.entities.GameItem
import com.resultados.quesalio.extras.Config
import kotlinx.android.synthetic.main.item_play.view.*
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import android.support.v4.util.Pair as UtilPair


internal class GamePlayAdapter(private val gamesList: MutableList<GameItem>) : RecyclerView.Adapter<GamePlayAdapter.ViewHolder>() {
    var itemCounter: Int = 0
    var finalList = mutableListOf<String>()
    var randomList = mutableListOf<GameItem>()
    override fun onBindViewHolder(holder: GamePlayAdapter.ViewHolder, position: Int) {
        holder.bindItems(gamesList.get(position))
    }

    override fun getItemCount(): Int = gamesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamePlayAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_play, parent, false)
        return ViewHolder(view)
    }

    fun setRandomValues(randomListPlay: MutableList<GameItem>) {
        randomList = randomListPlay
        finalList.clear()
        itemCounter = 0
        gamesList.forEach { gl ->
            gl.selected = false
        }
        randomListPlay.forEach { gameItem ->
            finalList.add(gameItem.number)
        }
        notifyDataSetChanged()
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: GameItem) = itemView.apply {
            itemPlayText.text = item.number
            this.setOnClickListener {
                if (item.selected) {
                    finalList.remove(item.number)
                    this.background = ContextCompat.getDrawable(context, R.drawable.background_rounded_item_play)
                    itemPlayText.textColor = ContextCompat.getColor(context, R.color.fondoSorteos)
                    item.selected = false
                    itemCounter--
                } else {
                    when (item.type) {
                        Config.LOTO_TYPE_JACK -> {
                            if (itemCounter > 1) {
                                context.toast(context.getString(R.string.lblSelectMoreThanTwo))
                                return@setOnClickListener
                            }
                        }
                        Config.LOTO_TYPE -> {
                            if (itemCounter > 5) {
                                context.toast(context.getString(R.string.lblSelectMoreThanSix))
                                return@setOnClickListener
                            }
                        }
                        Config.QUINI_TYPE -> {
                            if (itemCounter > 5) {
                                context.toast(context.getString(R.string.lblSelectMoreThanSix))
                                return@setOnClickListener
                            }
                        }
                        Config.BRINCO_TYPE -> {
                            if (itemCounter > 5) {
                                context.toast(context.getString(R.string.lblSelectMoreThanSix))
                                return@setOnClickListener
                            }
                        }
                        else -> {
                            if (itemCounter > 7) {
                                context.toast(context.getString(R.string.lblSelectMoreThanEight))
                                return@setOnClickListener
                            }
                        }
                    }
                    finalList.add(item.number)
                    this.background = ContextCompat.getDrawable(context, R.drawable.background_rounded_item_play_selected)
                    itemPlayText.textColor = ContextCompat.getColor(context, android.R.color.white)
                    item.selected = true
                    itemCounter++
                }
            }

            if (randomList.size > 0) {
                if (finalList.contains(item.number) && !item.selected) {
                    var toAdd = true
                    when (item.type) {
                        Config.LOTO_TYPE_JACK -> {
                            if (itemCounter > 1) {
                                context.toast(context.getString(R.string.lblSelectMoreThanTwo))
                                toAdd = false
                            }
                        }
                        Config.LOTO_TYPE -> {
                            if (itemCounter > 5) {
                                context.toast(context.getString(R.string.lblSelectMoreThanSix))
                                toAdd = false
                            }
                        }
                        Config.QUINI_TYPE -> {
                            if (itemCounter > 5) {
                                context.toast(context.getString(R.string.lblSelectMoreThanSix))
                                toAdd = false
                            }
                        }
                        Config.BRINCO_TYPE -> {
                            if (itemCounter > 5) {
                                context.toast(context.getString(R.string.lblSelectMoreThanSix))
                                toAdd = false
                            }
                        }
                        else -> {
                            if (itemCounter > 7) {
                                context.toast(context.getString(R.string.lblSelectMoreThanEight))
                                toAdd = false
                            }
                        }
                    }
                    if (toAdd) {
                        this.background = ContextCompat.getDrawable(context, R.drawable.background_rounded_item_play_selected)
                        itemPlayText.textColor = ContextCompat.getColor(context, android.R.color.white)
                        item.selected = true
                        itemCounter++
                        randomList.remove(item)
                    }
                } else {
                    finalList.remove(item.number)
                    this.background = ContextCompat.getDrawable(context, R.drawable.background_rounded_item_play)
                    itemPlayText.textColor = ContextCompat.getColor(context, R.color.fondoSorteos)
                    item.selected = false
                    itemCounter--
                    randomList.remove(item)
                }
            }
        }

    }
}