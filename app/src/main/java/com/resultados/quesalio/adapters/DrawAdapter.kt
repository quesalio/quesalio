package com.resultados.quesalio.adapters

import android.content.Intent
import android.os.Bundle
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.bumptech.glide.Glide
import com.resultados.quesalio.R
import com.resultados.quesalio.activities.DetailQuinielaActivity
import com.resultados.quesalio.entities.Draw
import com.resultados.quesalio.entities.Fecha
import com.resultados.quesalio.extras.DrawDiffCallback
import kotlinx.android.synthetic.main.item_draw.view.*
import spencerstudios.com.bungeelib.Bungee
import android.support.v4.util.Pair as UtilPair


internal class DrawAdapter(private val chosenIds: String, private val chosenDate: Fecha, private val drawType: String) : ListAdapter<Draw, DrawAdapter.ViewHolder>(DrawDiffCallback()) {
    override fun onBindViewHolder(holder: DrawAdapter.ViewHolder, position: Int) {
        holder.bindItems(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_draw, parent, false)
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: Draw) = with(itemView) {
            var thereAreNumbers = false
            val anim = AlphaAnimation(0.1f, 1.0f)
            anim.duration = 500
            anim.repeatMode = Animation.REVERSE
            anim.repeatCount = Animation.INFINITE

            item.primeraNumbers?.forEach { number ->
                number.number()?.let { num ->
                    if (!num.equals("1")) {
                        number.value()?.let {
                            if (it.isNotEmpty()) {
                                thereAreNumbers = true
                            }
                        }
                    }
                }
            }

            item.matutinaNumbers?.forEach { number ->
                number.number()?.let { num ->
                    if (!num.equals("1")) {
                        number.value()?.let {
                            if (it.isNotEmpty()) {
                                thereAreNumbers = true
                            }
                        }
                    }
                }
            }

            item.vespertinaNumbers?.forEach { number ->
                number.number()?.let { num ->
                    if (!num.equals("1")) {
                        number.value()?.let {
                            if (it.isNotEmpty()) {
                                thereAreNumbers = true
                            }
                        }
                    }
                }
            }

            item.nocturnaNumbers?.forEach { number ->
                number.number()?.let { num ->
                    if (!num.equals("1")) {
                        number.value()?.let {
                            if (it.isNotEmpty()) {
                                thereAreNumbers = true
                            }
                        }
                    }
                }
            }

            itemView.apply {
                txtCityItemDraw.text = item.city
                txtValueItemDraw.text = item.value
                if (item.value.equals("----") && thereAreNumbers) {
                    try {
                        Glide.with(context).load(context.getString(R.string.shaking)).into(imageMeaningItemDraw)
                    } catch (ignored: Exception) {
                    }
                    txtValueItemDraw?.animation = anim
                } else {
                    try {
                        Glide.with(context).load(context.getString(R.string.url_meanings) + item.meaning_url).into(imageMeaningItemDraw)
                    } catch (e: Exception) {
                        Glide.with(context).load(R.drawable.empty).into(imageMeaningItemDraw)
                    }
                }
            }

            this.setOnClickListener {
                val bundle = Bundle()
                val numbers = mutableListOf<Int>()
                val values = mutableListOf<String>()
                item.primeraNumbers?.let { numb ->
                    numbers.clear()
                    values.clear()
                    numb.forEach {
                        //numbers.add(it.number().toString())
                        numbers.add(it.number()!!.toInt())
                    }
                    numb.forEach {
                        values.add(it.value().toString())
                    }
                }
                item.matutinaNumbers?.let { numb ->
                    numbers.clear()
                    values.clear()
                    numb.forEach {
                        numbers.add(it.number()!!.toInt())
                    }
                    numb.forEach {
                        values.add(it.value().toString())
                    }
                }
                item.vespertinaNumbers?.let { numb ->
                    numbers.clear()
                    values.clear()
                    numb.forEach {
                        numbers.add(it.number()!!.toInt())
                    }
                    numb.forEach {
                        values.add(it.value().toString())
                    }
                }
                item.nocturnaNumbers?.let { numb ->
                    numbers.clear()
                    values.clear()
                    numb.forEach {
                        numbers.add(it.number()!!.toInt())
                    }
                    numb.forEach {
                        values.add(it.value().toString())
                    }
                }
                bundle.putString("id_sorteo", item.id)
                bundle.putString("nombre_sorteo", drawType)
                bundle.putString("provincia", item.city)
                bundle.putString("dia", chosenDate.letras)
                bundle.putString("fecha", chosenDate.numeros)
                bundle.putString("id_quiniela", item.id_draw)
                bundle.putString("id_loteria", item.id_loteria)
                bundle.putString("id_elegidos", chosenIds)
                //bundle.putStringArray("numeros", numbers.toTypedArray())
                bundle.putIntArray("numeros", numbers.toIntArray());
                bundle.putStringArray("valores", values.toTypedArray())
                bundle.putString("ciudad", item.city)
                bundle.putString("significado", item.meaning)
                bundle.putString("significado_url", item.meaning_url)
                bundle.putString("significado_numero", item.meaning_number)
                val intent = Intent(context, DetailQuinielaActivity::class.java)
                intent.putExtras(bundle)
                context.startActivity(intent)
                Bungee.slideLeft(context)

            }
        }
    }
}