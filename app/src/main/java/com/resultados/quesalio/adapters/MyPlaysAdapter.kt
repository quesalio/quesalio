package com.resultados.quesalio.adapters

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.resultados.quesalio.R
import com.resultados.quesalio.entities.Play
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils
import com.resultados.quesalio.extras.Utiles
import com.resultados.quesalio.extras.database
import kotlinx.android.synthetic.main.item_my_play.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import android.support.v4.util.Pair as UtilPair


internal class MyPlaysAdapter(private val gamesList: MutableList<Play>) : RecyclerView.Adapter<MyPlaysAdapter.ViewHolder>() {
    var numbersPoceada = mutableListOf<String>()
    var currentDatePoceada: Date? = Date()
    var numbersPoceadaPlus = mutableListOf<String>()
    var currentDatePoceadaPlus: Date? = Date()
    var numbersBrinco = mutableListOf<String>()
    var currentDateBrinco: Date? = Date()
    var numbersQuini = mutableListOf<String>()
    var numbersQuiniSegundaTradicional = mutableListOf<String>()
    var numbersQuiniSiempreSale = mutableListOf<String>()
    var numbersQuiniRevancha = mutableListOf<String>()
    var numbersQuiniPremioExtra = mutableListOf<String>()
    var jackPotsPrimeraTradicional = mutableListOf<String>()
    var currentDateQuini: Date? = Date()

    var lotoPrize = ""
    var brincoPrize = ""
    var quiniPrize = ""
    var poceadaPrize = ""
    var poceadaPlusPrize = ""

    var poceadaDay = ""
    var poceadaHour = ""
    var poceadaPlusDay = ""
    var poceadaPlusHour = ""
    var brincoDay = ""
    var brincoHour = ""
    var lotoDay = ""
    var lotoHour = ""
    var quiniDay = ""
    var quiniHour = ""

    var numbersLoto = mutableListOf<String>()
    var numbersLotoSiempreSale = mutableListOf<String>()
    var numbersLotoDesquite = mutableListOf<String>()
    var currentDateLoto: Date? = Date()
    val df = DecimalFormat("#.#")

    override fun onBindViewHolder(holder: MyPlaysAdapter.ViewHolder, position: Int) {
        holder.bindItems(gamesList.get(position))
    }

    override fun getItemCount(): Int = gamesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPlaysAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_my_play, parent, false)
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: Play) = itemView.apply {
            val anotherItem = gamesList.get(adapterPosition)
            btnDeleteMyPlay.setOnClickListener {
                context.database.use {
                    //delete(Config.PLAYS_TABLE, whereClause = "id = {playId}", args = "playId" to anotherItem.id)
                    delete(Config.PLAYS_TABLE, whereClause = "id = ${anotherItem.id}")
                    gamesList.remove(anotherItem)
                    notifyItemRemoved(adapterPosition)
                    context.toast(context.getString(R.string.deletedMyPlay))
                }
            }
            df.roundingMode = RoundingMode.CEILING
            val title = when (item.type) {
                Config.QUINI_TYPE -> {
                    "QUINI 6"
                }
                Config.BRINCO_TYPE -> {
                    "BRINCO"
                }
                Config.LOTO_TYPE -> {
                    "LOTO"
                }
                Config.POCEADA_PLUS_TYPE -> {
                    "POCEADA PLUS"
                }
                Config.POCEADA_TYPE -> {
                    "POCEADA"
                }
                else -> {
                    ""
                }
            }
            imageThumb.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.down_36dpx2))
            txtPrizeMyPlay.text = context.getString(R.string.lblNoPrize)
            titleMyPlay.text = title
            titleMyPlay.textColor = ContextCompat.getColor(context, android.R.color.white)
            lblLastDraw.textColor = ContextCompat.getColor(context, android.R.color.white)
            imageThumb.setColorFilter(ContextCompat.getColor(context, android.R.color.white))
            txtHitsMyPlay.textColor = ContextCompat.getColor(context, android.R.color.white)
            dateMyPlay.textColor = ContextCompat.getColor(context, android.R.color.white)
            dateNextDraw.visibility = View.VISIBLE
            var numbers: List<String>
            var jacks = listOf<String>()
            when (item.type) {
                Config.LOTO_TYPE -> {
                    numbers = item.numbers.split("|")
                    jacks = numbers.get(1).split(",")
                    jacks = jacks.sortedWith(compareBy { it.toInt() })
                    numbers = numbers.get(0).split(",")
                }
                else ->
                    numbers = item.numbers.split(",")
            }
            numbers = numbers.sortedWith(compareBy { it.toInt() })
            numbersMyPlay.removeAllViews()
            val inflater = LayoutInflater.from(context)
            layoutExtra.removeAllViews()
            when (item.type) {
                Config.POCEADA_TYPE -> {
                    if (poceadaDay.isNotEmpty() && poceadaHour.isNotEmpty()) {
                        dateNextDraw.text = "$poceadaDay ${poceadaHour}Hs"
                        dateNextDraw.textColor = ContextCompat.getColor(context, R.color.blueFont)
                    } else {
                        dateNextDraw.visibility = View.GONE
                    }
                    if (poceadaPrize.isNotEmpty()) {
                        textNextDraw.text = Utiles.formatearNumero(poceadaPrize)
                        textNextDraw.textColor = ContextCompat.getColor(context, R.color.blueFont)
                    }
                    layoutExtra.removeAllViews()
                    layoutExtra.visibility = View.GONE
                    numbersMyJack.removeAllViews()
                    numbersMyJack.visibility = View.GONE
                    watchText.setTextColor(ContextCompat.getColor(context, R.color.blueFont))
                    shieldText.setTextColor(ContextCompat.getColor(context, R.color.blueFont))
                    watchImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reloj_azul_poceada))
                    shieldImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rutina_azul_poceada))
                    layoutBackground1.backgroundColor = ContextCompat.getColor(context, R.color.blueFont)
                    layoutBackground2.backgroundColor = ContextCompat.getColor(context, R.color.blueFont)
                    layoutBackground3.backgroundColor = ContextCompat.getColor(context, R.color.blueFont)
                    numbers.forEach { number ->
                        val view = inflater.inflate(R.layout.item_my_number, numbersMyPlay, false)
                        val numberMyPlay = view.findViewById<TextView>(R.id.numberMyPlay)
                        if (numbersPoceada.contains(number.trim())) {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_green)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                        } else {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_white)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, R.color.blueFont))
                        }
                        numberMyPlay.text = number
                        numbersMyPlay.addView(view)
                    }
                    if (numbersPoceada.size > 0) {
                        val calendar = Calendar.getInstance()
                        calendar.time = currentDatePoceada
                        dateMyPlay.text = ListUtils.getDateMyPlays(calendar)
                        val intersection = numbersPoceada.intersect(item.numbers.split(","))
                        val occurrences: Float = ((intersection.size * 100.0) / item.numbers.split(",").size).toFloat()
                        val remaining: Float = (100 - occurrences)
                        val entries = arrayListOf<PieEntry>()
                        entries.add(PieEntry(remaining))
                        entries.add(PieEntry(occurrences))
                        val set = PieDataSet(entries, "")
                        set.setColors(intArrayOf(android.R.color.white, R.color.green_pie), context)
                        val data = PieData(set)
                        data.setDrawValues(false)
                        pieChart.description.isEnabled = false
                        pieChart.legend.isEnabled = false
                        val textOccurrences = "${df.format(occurrences)} %"
                        val sb = SpannableStringBuilder(textOccurrences)
                        sb.setSpan(RelativeSizeSpan(1.6f), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        sb.setSpan(ForegroundColorSpan(Color.WHITE), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        pieChart.centerText = sb
                        pieChart.isDrawHoleEnabled = true
                        pieChart.holeRadius = 85f
                        pieChart.setHoleColor(android.R.color.transparent)
                        pieChart.data = data
                        pieChart.invalidate()
                        txtHitsMyPlay.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        if (intersection.size == 6 || intersection.size == 7 || intersection.size == 8) {
                            txtPrizeMyPlay.text = context.getString(R.string.lblYesPrize)
                            imageThumb.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mano_arriba_blanca))
                        }
                    }
                }
                Config.POCEADA_PLUS_TYPE -> {
                    titleMyPlay.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    lblLastDraw.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    txtHitsMyPlay.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    txtPrizeMyPlay.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    imageThumb.setColorFilter(ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay))
                    dateMyPlay.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    if (poceadaPlusDay.isNotEmpty() && poceadaPlusHour.isNotEmpty()) {
                        dateNextDraw.text = "$poceadaPlusDay ${poceadaPlusHour}Hs"
                        dateNextDraw.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    } else {
                        dateNextDraw.visibility = View.GONE
                    }
                    if (poceadaPlusPrize.isNotEmpty()) {
                        textNextDraw.text = Utiles.formatearNumero(poceadaPlusPrize)
                        textNextDraw.textColor = ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay)
                    }
                    layoutExtra.removeAllViews()
                    layoutExtra.visibility = View.GONE
                    numbersMyJack.removeAllViews()
                    numbersMyJack.visibility = View.GONE
                    watchText.setTextColor(ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay))
                    shieldText.setTextColor(ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay))
                    watchImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reloj_vrede_poceadaplus))
                    shieldImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rutina_verde_poceadaplus))
                    layoutBackground1.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayPoceadaPlus)
                    layoutBackground2.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayPoceadaPlus)
                    layoutBackground3.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayPoceadaPlus)
                    numbers.forEach { number ->
                        val view = inflater.inflate(R.layout.item_my_number, numbersMyPlay, false)
                        val numberMyPlay = view.findViewById<TextView>(R.id.numberMyPlay)
                        if (numbersPoceadaPlus.contains(number)) {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_green)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                        } else {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_white)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, R.color.blueFont))
                        }
                        numberMyPlay.text = number
                        numbersMyPlay.addView(view)
                    }
                    if (numbersPoceadaPlus.size > 0) {
                        val calendar = Calendar.getInstance()
                        calendar.time = currentDatePoceadaPlus
                        dateMyPlay.text = ListUtils.getDateMyPlays(calendar)
                        val intersection = numbersPoceadaPlus.intersect(item.numbers.split(","))
                        val occurrences: Float = ((intersection.size * 100.0) / item.numbers.split(",").size).toFloat()
                        val remaining: Float = (100 - occurrences)
                        val entries = arrayListOf<PieEntry>()
                        entries.add(PieEntry(remaining))
                        entries.add(PieEntry(occurrences))
                        val set = PieDataSet(entries, "")
                        set.setColors(intArrayOf(android.R.color.white, R.color.green_pie), context)
                        val data = PieData(set)
                        data.setDrawValues(false)
                        pieChart.description.isEnabled = false
                        pieChart.legend.isEnabled = false
                        val textOccurrences = "${df.format(occurrences)} %"
                        val sb = SpannableStringBuilder(textOccurrences)
                        sb.setSpan(RelativeSizeSpan(1.6f), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        sb.setSpan(ForegroundColorSpan(Color.WHITE), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        pieChart.centerText = sb
                        pieChart.isDrawHoleEnabled = true
                        pieChart.holeRadius = 85f
                        pieChart.setHoleColor(android.R.color.transparent)
                        pieChart.data = data
                        pieChart.invalidate()
                        txtHitsMyPlay.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        if (intersection.size == 5 || intersection.size == 6 || intersection.size == 7 || intersection.size == 8) {
                            txtPrizeMyPlay.text = context.getString(R.string.lblYesPrize)
                            imageThumb.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mano_arriba_blanca))
                            imageThumb.setColorFilter(ContextCompat.getColor(context, R.color.textPoceadaPlusDetailPlay))
                        }
                    }
                }
                Config.BRINCO_TYPE -> {
                    if (brincoDay.isNotEmpty() && brincoHour.isNotEmpty()) {
                        dateNextDraw.text = "$brincoDay ${brincoHour}Hs"
                        dateNextDraw.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco)
                    } else {
                        dateNextDraw.visibility = View.GONE
                    }
                    if (brincoPrize.isNotEmpty()) {
                        textNextDraw.text = Utiles.formatearNumero(brincoPrize)
                        textNextDraw.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco)
                    }
                    layoutExtra.removeAllViews()
                    layoutExtra.visibility = View.GONE
                    numbersMyJack.removeAllViews()
                    numbersMyJack.visibility = View.GONE
                    watchText.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco))
                    shieldText.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco))
                    watchImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reloj_azul_brinco))
                    shieldImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rutina_azul_oscuro))
                    layoutBackground1.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco)
                    layoutBackground2.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco)
                    layoutBackground3.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco)
                    numbers.forEach { number ->
                        val view = inflater.inflate(R.layout.item_my_number, numbersMyPlay, false)
                        val numberMyPlay = view.findViewById<TextView>(R.id.numberMyPlay)
                        if (numbersBrinco.contains(number)) {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_green)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                        } else {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_white)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, R.color.blueFont))
                        }
                        numberMyPlay.text = number
                        numbersMyPlay.addView(view)
                    }
                    if (numbersBrinco.size > 0) {
                        val calendar = Calendar.getInstance()
                        calendar.time = currentDateBrinco
                        dateMyPlay.text = ListUtils.getDateMyPlays(calendar)

                        val intersection = numbersBrinco.intersect(item.numbers.split(","))
                        val occurrences: Float = ((intersection.size * 100.0) / item.numbers.split(",").size).toFloat()
                        val remaining: Float = (100 - occurrences)

                        val entries = arrayListOf<PieEntry>()
                        entries.add(PieEntry(remaining))
                        entries.add(PieEntry(occurrences))
                        val set = PieDataSet(entries, "")
                        set.setColors(intArrayOf(android.R.color.white, R.color.green_pie), context)
                        val data = PieData(set)
                        data.setDrawValues(false)
                        pieChart.description.isEnabled = false
                        pieChart.legend.isEnabled = false
                        val textOccurrences = "${df.format(occurrences)} %"
                        val sb = SpannableStringBuilder(textOccurrences)
                        sb.setSpan(RelativeSizeSpan(1.6f), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        sb.setSpan(ForegroundColorSpan(Color.WHITE), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        pieChart.centerText = sb
                        pieChart.isDrawHoleEnabled = true
                        pieChart.holeRadius = 85f
                        pieChart.setHoleColor(android.R.color.transparent)
                        pieChart.data = data
                        pieChart.invalidate()
                        txtHitsMyPlay.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        if (intersection.size == 3 || intersection.size == 4 || intersection.size == 5 || intersection.size == 6) {
                            txtPrizeMyPlay.text = context.getString(R.string.lblYesPrize)
                            imageThumb.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mano_arriba_blanca))
                        }
                    }
                }
                Config.QUINI_TYPE -> {
                    if (quiniDay.isNotEmpty() && quiniHour.isNotEmpty()) {
                        dateNextDraw.text = "$quiniDay ${quiniHour}Hs"
                        dateNextDraw.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    } else {
                        dateNextDraw.visibility = View.GONE
                    }
                    if (quiniPrize.isNotEmpty()) {
                        textNextDraw.text = Utiles.formatearNumero(quiniPrize)
                        textNextDraw.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    }
                    if (numbersQuini.size > 0) {
                        numbersQuini.forEach { number ->
                            if (!numbersQuiniPremioExtra.contains(number)) {
                                numbersQuiniPremioExtra.add(number)
                            }
                        }
                    }
                    if (numbersQuiniSegundaTradicional.size > 0) {
                        numbersQuiniSegundaTradicional.forEach { number ->
                            if (!numbersQuiniPremioExtra.contains(number)) {
                                numbersQuiniPremioExtra.add(number)
                            }
                        }
                    }
                    if (numbersQuiniRevancha.size > 0) {
                        numbersQuiniRevancha.forEach { number ->
                            if (!numbersQuiniPremioExtra.contains(number)) {
                                numbersQuiniPremioExtra.add(number)
                            }
                        }
                    }

                    layoutExtra.removeAllViews()
                    layoutExtra.visibility = View.GONE
                    numbersMyJack.removeAllViews()
                    numbersMyJack.visibility = View.GONE
                    layoutExtra.visibility = View.VISIBLE
                    layoutExtra.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayQuini)
                    imageThumb.setColorFilter(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    lblLastDraw.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    titleMyPlay.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    watchText.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    shieldText.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    watchImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reloj_rojo_loto_quini))
                    shieldImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rutina_rojo_quini_loto))
                    layoutBackground1.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayQuini)
                    layoutBackground2.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayQuini)
                    layoutBackground3.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayQuini)
                    var listSelected = numbersQuini
                    var intPrimeraTradicional = 0
                    var intSegundaTradicional = 0
                    var intSiempreSaleTradicional = 0
                    var intRevanchaTradicional = 0
                    var intPremioExtraTradicional = 0

                    if (numbersQuini.size > 0) {
                        val calendar = Calendar.getInstance()
                        calendar.time = currentDateQuini
                        dateMyPlay.text = ListUtils.getDateMyPlays(calendar)
                        dateMyPlay.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                        var intersection = numbersQuini.intersect(numbers)
                        var occurrences: Float = ((intersection.size * 100.0) / numbers.size).toFloat()
                        var remaining: Float = (100 - occurrences)
                        intPrimeraTradicional = intersection.size
                        if (numbersQuiniSegundaTradicional.size > 0) {
                            val intersectionST = numbersQuiniSegundaTradicional.intersect(numbers)
                            val occurrencesST: Float = ((intersectionST.size * 100.0) / numbers.size).toFloat()
                            val remainingST: Float = (100 - occurrencesST)
                            if (occurrences < occurrencesST) {
                                occurrences = occurrencesST
                                remaining = remainingST
                                intersection = intersectionST
                                listSelected = numbersQuiniSegundaTradicional
                            }
                            intSegundaTradicional = intersectionST.size
                        }
                        if (numbersQuiniSiempreSale.size > 0) {
                            val intersectionSS = numbersQuiniSiempreSale.intersect(numbers)
                            val occurrencesSS: Float = ((intersectionSS.size * 100.0) / numbers.size).toFloat()
                            val remainingSS: Float = (100 - occurrencesSS)
                            if (occurrences < occurrencesSS) {
                                occurrences = occurrencesSS
                                remaining = remainingSS
                                intersection = intersectionSS
                                listSelected = numbersQuiniSiempreSale
                            }
                            intSiempreSaleTradicional = intersectionSS.size
                        }
                        if (numbersQuiniRevancha.size > 0) {
                            val intersectionR = numbersQuiniRevancha.intersect(numbers)
                            val occurrencesR: Float = ((intersectionR.size * 100.0) / numbers.size).toFloat()
                            val remainingR: Float = (100 - occurrencesR)
                            if (occurrences < occurrencesR) {
                                occurrences = occurrencesR
                                remaining = remainingR
                                intersection = intersectionR
                                listSelected = numbersQuiniRevancha
                            }
                            intRevanchaTradicional = intersectionR.size
                        }
                        if (numbersQuiniPremioExtra.size > 0) {
                            val intersectionPE = numbersQuiniPremioExtra.intersect(numbers)
                            val occurrencesPE: Float = ((intersectionPE.size * 100.0) / numbers.size).toFloat()
                            val remainingPE: Float = (100 - occurrencesPE)
                            if (occurrences < occurrencesPE) {
                                occurrences = occurrencesPE
                                remaining = remainingPE
                                intersection = intersectionPE
                                listSelected = numbersQuiniPremioExtra
                            }
                            intPremioExtraTradicional = intersectionPE.size
                        }
                        val entries = arrayListOf<PieEntry>()
                        entries.add(PieEntry(remaining))
                        entries.add(PieEntry(occurrences))
                        val set = PieDataSet(entries, "")
                        set.setColors(intArrayOf(android.R.color.white, R.color.green_pie), context)
                        val data = PieData(set)
                        data.setDrawValues(false)
                        pieChart.description.isEnabled = false
                        pieChart.legend.isEnabled = false
                        val textOccurrences = "${df.format(occurrences)} %"
                        val sb = SpannableStringBuilder(textOccurrences)
                        sb.setSpan(RelativeSizeSpan(1.6f), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        //sb.setSpan(ForegroundColorSpan(Color.WHITE), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        sb.setSpan(ForegroundColorSpan(Color.RED), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        pieChart.centerText = sb
                        pieChart.isDrawHoleEnabled = true
                        pieChart.holeRadius = 85f
                        pieChart.setHoleColor(android.R.color.transparent)
                        pieChart.data = data
                        pieChart.invalidate()
                        txtHitsMyPlay.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        txtHitsMyPlay.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                        txtPrizeMyPlay.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)

                        if (
                                (intPrimeraTradicional == 4 || intPrimeraTradicional == 5 || intPrimeraTradicional == 6) ||
                                (intSegundaTradicional == 4 || intSegundaTradicional == 5 || intSegundaTradicional == 6) ||
                                (intRevanchaTradicional == 6) ||
                                (intSiempreSaleTradicional == 6) ||
                                (intPremioExtraTradicional == 6)
                        ) {
                            txtPrizeMyPlay.text = context.getString(R.string.lblYesPrize)
                            imageThumb.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mano_arriba_blanca))
                        }
                    }
                    numbers.forEach { number ->
                        val view = inflater.inflate(R.layout.item_my_number, numbersMyPlay, false)
                        val numberMyPlay = view.findViewById<TextView>(R.id.numberMyPlay)
                        if (listSelected.contains(number)) {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_green)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                        } else {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_white)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                        }
                        numberMyPlay.text = number
                        numbersMyPlay.addView(view)
                    }
                    val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0, 24, 0, 8)
                    val paramsDown = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    paramsDown.setMargins(0, 0, 0, 20)
                    val textSizeTitle = 15.5f
                    val textSizeItem = 24f
                    val paddingTopTitle = 20
                    val lblTitleTradicional = TextView(context)
                    lblTitleTradicional.text = context.getString(R.string.lblTradicionalFirstDraw)
                    lblTitleTradicional.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    lblTitleTradicional.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitleTradicional.textSize = textSizeTitle
                    lblTitleTradicional.layoutParams = params
                    lblTitleTradicional.setPadding(0, paddingTopTitle, 0, 0)

                    val lblTitleSegundaTradicional = TextView(context)
                    lblTitleSegundaTradicional.text = context.getString(R.string.lblTradicionalSecondDraw)
                    lblTitleSegundaTradicional.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    lblTitleSegundaTradicional.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitleSegundaTradicional.textSize = textSizeTitle
                    lblTitleSegundaTradicional.layoutParams = params
                    lblTitleSegundaTradicional.setPadding(0, paddingTopTitle, 0, 0)

                    val lblTitleRevancha = TextView(context)
                    lblTitleRevancha.text = context.getString(R.string.lblDesquite)
                    lblTitleRevancha.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    lblTitleRevancha.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitleRevancha.textSize = textSizeTitle
                    lblTitleRevancha.layoutParams = params
                    lblTitleRevancha.setPadding(0, paddingTopTitle, 0, 0)

                    val lblTitleSiempreSale = TextView(context)
                    lblTitleSiempreSale.text = context.getString(R.string.lblSiempreSale)
                    lblTitleSiempreSale.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    lblTitleSiempreSale.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitleSiempreSale.textSize = textSizeTitle
                    lblTitleSiempreSale.layoutParams = params
                    lblTitleSiempreSale.setPadding(0, paddingTopTitle, 0, 0)

                    val lblTitlePremioExtra = TextView(context)
                    lblTitlePremioExtra.text = context.getString(R.string.lblPremioExtra)
                    lblTitlePremioExtra.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    lblTitlePremioExtra.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitlePremioExtra.textSize = textSizeTitle
                    lblTitlePremioExtra.layoutParams = params
                    lblTitlePremioExtra.setPadding(0, paddingTopTitle, 0, 0)

                    val splitted = numbers
                    if (numbersQuini.size > 0) {
                        val intersection = numbersQuini.intersect(splitted)
                        layoutExtra.addView(lblTitleTradicional)
                        val tradicional = TextView(context)
                        tradicional.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        tradicional.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                        tradicional.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        tradicional.textSize = textSizeItem
                        tradicional.layoutParams = paramsDown
                        layoutExtra.addView(tradicional)
                    }
                    if (numbersQuiniSegundaTradicional.size > 0) {
                        val intersection = numbersQuiniSegundaTradicional.intersect(numbers)
                        layoutExtra.addView(lblTitleSegundaTradicional)
                        val segundaTradicional = TextView(context)
                        segundaTradicional.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        segundaTradicional.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                        segundaTradicional.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        segundaTradicional.textSize = textSizeItem
                        segundaTradicional.layoutParams = paramsDown
                        layoutExtra.addView(segundaTradicional)
                    }
                    if (numbersQuiniRevancha.size > 0) {
                        val intersection = numbersQuiniRevancha.intersect(splitted)
                        layoutExtra.addView(lblTitleRevancha)
                        val revancha = TextView(context)
                        revancha.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        revancha.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                        revancha.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        revancha.layoutParams = paramsDown
                        revancha.textSize = textSizeItem
                        layoutExtra.addView(revancha)
                    }
                    if (numbersQuiniSiempreSale.size > 0) {
                        val intersection = numbersQuiniSiempreSale.intersect(splitted)
                        layoutExtra.addView(lblTitleSiempreSale)
                        val siempreSale = TextView(context)
                        siempreSale.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        siempreSale.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                        siempreSale.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        siempreSale.layoutParams = paramsDown
                        siempreSale.textSize = textSizeItem
                        layoutExtra.addView(siempreSale)
                    }
                    if (numbersQuiniPremioExtra.size > 0) {
                        val intersection = numbersQuiniPremioExtra.intersect(splitted)
                        layoutExtra.addView(lblTitlePremioExtra)
                        val premioExtra = TextView(context)
                        premioExtra.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        premioExtra.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                        premioExtra.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        premioExtra.layoutParams = paramsDown
                        premioExtra.textSize = textSizeItem
                        layoutExtra.addView(premioExtra)
                    }
                }
                Config.LOTO_TYPE -> {
                    if (lotoDay.isNotEmpty() && lotoHour.isNotEmpty()) {
                        dateNextDraw.text = "$lotoDay ${lotoHour}Hs"
                        dateNextDraw.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    } else {
                        dateNextDraw.visibility = View.GONE
                    }
                    if (lotoPrize.isNotEmpty()) {
                        textNextDraw.text = Utiles.formatearNumero(lotoPrize)
                        textNextDraw.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    }
                    layoutExtra.visibility = View.VISIBLE
                    layoutExtra.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    imageThumb.setColorFilter(ContextCompat.getColor(context, android.R.color.white))
                    lblLastDraw.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                    titleMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                    watchText.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    shieldText.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                    watchImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.reloj_rojo_loto_quini))
                    shieldImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rutina_rojo_quini_loto))
                    layoutBackground1.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    layoutBackground2.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    layoutBackground3.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)


                    var listSelected = numbersLoto
                    var intersectionTradicional = 0

                    if (numbersLoto.size > 0) {
                        val calendar = Calendar.getInstance()
                        calendar.time = currentDateLoto
                        dateMyPlay.text = ListUtils.getDateMyPlays(calendar)
                        dateMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                        var intersection = numbersLoto.intersect(numbers)
                        //var intersection = numbersLoto.intersect(item.numbers.split(","))
                        var occurrences: Float = ((intersection.size * 100.0) / numbers.size).toFloat()
                        //var occurrences: Float = ((intersection.size * 100.0) / item.numbers.split(",").size).toFloat()
                        var remaining: Float = (100 - occurrences)
                        intersectionTradicional = intersection.size

                        if (numbersLotoSiempreSale.size > 0) {
                            val intersectionSS = numbersLotoSiempreSale.intersect(numbers)
                            val occurrencesSS: Float = ((intersectionSS.size * 100.0) / numbers.size).toFloat()
                            val remainingSS: Float = (100 - occurrencesSS)
                            if (occurrences < occurrencesSS) {
                                occurrences = occurrencesSS
                                remaining = remainingSS
                                intersection = intersectionSS
                                listSelected = numbersLotoSiempreSale
                            }
                        }
                        if (numbersLotoDesquite.size > 0) {
                            val intersectionD = numbersLotoDesquite.intersect(numbers)
                            val occurrencesD: Float = ((intersectionD.size * 100.0) / numbers.size).toFloat()
                            val remainingD: Float = (100 - occurrencesD)
                            if (occurrences < occurrencesD) {
                                occurrences = occurrencesD
                                remaining = remainingD
                                intersection = intersectionD
                                listSelected = numbersLotoDesquite
                            }
                        }
                        val entries = arrayListOf<PieEntry>()
                        entries.add(PieEntry(remaining))
                        entries.add(PieEntry(occurrences))
                        val set = PieDataSet(entries, "")
                        set.setColors(intArrayOf(android.R.color.white, R.color.green_pie), context)
                        val data = PieData(set)
                        data.setDrawValues(false)
                        pieChart.description.isEnabled = false
                        pieChart.legend.isEnabled = false
                        val textOccurrences = "${df.format(occurrences)} %"
                        val sb = SpannableStringBuilder(textOccurrences)
                        sb.setSpan(RelativeSizeSpan(1.6f), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        sb.setSpan(ForegroundColorSpan(Color.WHITE), 0, textOccurrences.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        pieChart.centerText = sb
                        pieChart.isDrawHoleEnabled = true
                        pieChart.holeRadius = 85f
                        pieChart.setHoleColor(android.R.color.transparent)
                        pieChart.data = data
                        pieChart.invalidate()
                        txtHitsMyPlay.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        txtHitsMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                    }
                    numbers.forEach { number ->
                        val view = inflater.inflate(R.layout.item_my_number, numbersMyPlay, false)
                        val numberMyPlay = view.findViewById<TextView>(R.id.numberMyPlay)
                        if (listSelected.contains(number)) {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_green)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                        } else {
                            numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_white)
                            numberMyPlay.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                        }
                        numberMyPlay.text = number
                        numbersMyPlay.addView(view)
                    }
                    val paramsJacks = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    if (jacks.size > 0) {
                        numbersMyJack.visibility = View.VISIBLE
                        numbersMyJack.removeAllViews()
                        jacks.forEachIndexed { index, jack ->
                            val layout = LinearLayout(context)
                            layout.layoutParams = paramsJacks
                            val label = TextView(context)
                            label.text = "Jack ${index + 1}"
                            label.textSize = 17f
                            label.typeface = ResourcesCompat.getFont(context, R.font.roboto)
                            label.textColor = ContextCompat.getColor(context, android.R.color.white)
                            label.gravity = Gravity.CENTER_VERTICAL
                            label.setPadding(0, 16, 0, 0)
                            layout.addView(label)
                            val view = inflater.inflate(R.layout.item_my_number, numbersMyPlay, false)
                            val numberMyPlay = view.findViewById<TextView>(R.id.numberMyPlay)
                            if (jackPotsPrimeraTradicional.contains(jack)) {
                                numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_green)
                                numberMyPlay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
                            } else {
                                numberMyPlay.setBackgroundResource(R.drawable.background_rounded_item_my_play_white)
                                numberMyPlay.setTextColor(ContextCompat.getColor(context, R.color.backgroundMyPlayLoto))
                            }
                            numberMyPlay.text = jack
                            layout.addView(numberMyPlay)
                            numbersMyJack.addView(layout)
                        }
                        val intersectionJack = jackPotsPrimeraTradicional.intersect(jacks).size
                        if (intersectionTradicional == 6 && intersectionJack == 2 ||
                                intersectionTradicional == 6 && intersectionJack == 1 ||
                                intersectionTradicional == 6 ||
                                intersectionTradicional == 5 && intersectionJack == 2 ||
                                intersectionTradicional == 5 && intersectionJack == 1 ||
                                intersectionTradicional == 5 ||
                                intersectionTradicional == 4 && intersectionJack == 2 ||
                                intersectionTradicional == 4 && intersectionJack == 1 ||
                                intersectionTradicional == 3 && intersectionJack == 2) {
                            txtPrizeMyPlay.text = context.getString(R.string.lblYesPrize)
                            imageThumb.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.mano_arriba_blanca))
                        }
                    }

                    val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0, 18, 0, 8)
                    val paramsDown = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    paramsDown.setMargins(0, 0, 0, 12)
                    val textSizeTitle = 15.5f
                    val textSizeItem = 24f
                    val setPaddingTopTitle = 20
                    val lblTitleDesquite = TextView(context)
                    lblTitleDesquite.text = context.getString(R.string.lblDesquite)
                    lblTitleDesquite.textColor = ContextCompat.getColor(context, android.R.color.white)
                    lblTitleDesquite.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitleDesquite.layoutParams = params
                    lblTitleDesquite.textSize = textSizeTitle
                    lblTitleDesquite.setPadding(0, setPaddingTopTitle, 0, 0)

                    val lblTitleSiempreSale = TextView(context)
                    lblTitleSiempreSale.text = context.getString(R.string.lblSiempreSale)
                    lblTitleSiempreSale.textColor = ContextCompat.getColor(context, android.R.color.white)
                    lblTitleSiempreSale.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                    lblTitleSiempreSale.layoutParams = params
                    lblTitleSiempreSale.gravity = Gravity.CENTER_VERTICAL
                    lblTitleSiempreSale.textSize = textSizeTitle
                    lblTitleSiempreSale.setPadding(0, setPaddingTopTitle, 0, 0)

                    val splitted = numbers

                    if (numbersLotoDesquite.size > 0) {
                        val intersection = numbersLotoDesquite.intersect(splitted)
                        layoutExtra.addView(lblTitleDesquite)
                        val desquite = TextView(context)
                        desquite.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        desquite.textColor = ContextCompat.getColor(context, android.R.color.white)
                        desquite.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        desquite.gravity = Gravity.CENTER_VERTICAL
                        desquite.layoutParams = paramsDown
                        desquite.textSize = textSizeItem
                        layoutExtra.addView(desquite)
                    }
                    if (numbersLotoSiempreSale.size > 0) {
                        val intersection = numbersLotoSiempreSale.intersect(splitted)
                        layoutExtra.addView(lblTitleSiempreSale)
                        val siempreSale = TextView(context)
                        siempreSale.text = "${intersection.size} " + when (intersection.size) {
                            1 -> context.getString(R.string.lblHit)
                            else -> context.getString(R.string.lblHits)
                        }
                        siempreSale.textColor = ContextCompat.getColor(context, android.R.color.white)
                        siempreSale.typeface = ResourcesCompat.getFont(context, R.font.latoregular)
                        siempreSale.gravity = Gravity.CENTER_VERTICAL
                        siempreSale.layoutParams = paramsDown
                        siempreSale.textSize = textSizeItem
                        layoutExtra.addView(siempreSale)
                    }
                }
            }
        }
    }

    fun addPoceada(numbers: MutableList<String>, date: Date?) {
        numbersPoceada = numbers
        currentDatePoceada = date
        notifyDataSetChanged()
    }

    fun addPoceadaPlus(numbers: MutableList<String>, date: Date?) {
        numbersPoceadaPlus = numbers
        currentDatePoceada = date
        notifyDataSetChanged()
    }

    fun addBrinco(numbers: MutableList<String>, date: Date?) {
        numbersBrinco = numbers
        currentDateBrinco = date
        notifyDataSetChanged()
    }

    fun addQuini(numbers: MutableList<String>, date: Date?) {
        numbersQuini = numbers
        currentDateQuini = date
        notifyDataSetChanged()
    }

    fun addQuiniExtras(segundaTradicional: MutableList<String>, siempreSale: MutableList<String>, revancha: MutableList<String>) {
        numbersQuiniSegundaTradicional = segundaTradicional
        numbersQuiniRevancha = revancha
        numbersQuiniSiempreSale = siempreSale
        notifyDataSetChanged()
    }

    fun addLoto(numbers: MutableList<String>, date: Date?) {
        numbersLoto = numbers
        currentDateQuini = date
        notifyDataSetChanged()
    }

    fun addLotoExtras(siempreSale: MutableList<String>, desquite: MutableList<String>, jackpotsTradicional: MutableList<String>) {
        numbersLotoDesquite = desquite
        numbersLotoSiempreSale = siempreSale
        jackPotsPrimeraTradicional = jackpotsTradicional
        notifyDataSetChanged()
    }

    fun addLotoPrize(prize: String) {
        lotoPrize = prize
        notifyDataSetChanged()
    }

    fun addQuiniPrize(prize: String) {
        quiniPrize = prize
        notifyDataSetChanged()
    }

    fun addPoceadaPlusPrize(prize: String) {
        poceadaPlusPrize = prize
        notifyDataSetChanged()
    }

    fun addPoceadaPrize(prize: String) {
        poceadaPrize = prize
        notifyDataSetChanged()
    }

    fun addBrincoPrize(prize: String) {
        brincoPrize = prize
        notifyDataSetChanged()
    }

    fun addPoceadaSchedule(day: String, hour: String) {
        poceadaDay = day
        poceadaHour = hour
        notifyDataSetChanged()
    }

    fun addPoceadaPlusSchedule(day: String, hour: String) {
        poceadaPlusDay = day
        poceadaPlusHour = hour
        notifyDataSetChanged()
    }

    fun addBrincoSchedule(day: String, hour: String) {
        brincoDay = day
        brincoHour = hour
        notifyDataSetChanged()
    }

    fun addLotoSchedule(day: String, hour: String) {
        lotoDay = day
        lotoHour = hour
        notifyDataSetChanged()
    }

    fun addQuiniSchedule(day: String, hour: String) {
        quiniDay = day
        quiniHour = hour
        notifyDataSetChanged()
    }
}