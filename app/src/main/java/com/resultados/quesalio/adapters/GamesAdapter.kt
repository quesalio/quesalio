package com.resultados.quesalio.adapters

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.resultados.quesalio.R
import com.resultados.quesalio.activities.*
import com.resultados.quesalio.entities.Game
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.Utiles
import kotlinx.android.synthetic.main.item_game.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor
import spencerstudios.com.bungeelib.Bungee
import android.support.v4.util.Pair as UtilPair


internal class GamesAdapter(private val gamesList: MutableList<Game>) : RecyclerView.Adapter<GamesAdapter.ViewHolder>() {
    var dateGameQuini: String = ""
    var dateGameBrinco: String = ""
    var dateGameLoto: String = ""
    var dateGamePoceada: String = ""
    var dateGamePoceadaPlus: String = ""
    override fun onBindViewHolder(holder: GamesAdapter.ViewHolder, position: Int) {
        holder.bindItems(gamesList.get(position))
    }

    override fun getItemCount(): Int = gamesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_game, parent, false)
        return ViewHolder(view)
    }

    fun addDateGameQuini(date: String) {
        dateGameQuini = date
        notifyDataSetChanged()
    }

    fun addDateGameBrinco(date: String) {
        dateGameBrinco = date
        notifyDataSetChanged()
    }

    fun addDateGameLoto(date: String) {
        dateGameLoto = date
        notifyDataSetChanged()
    }

    fun addDateGamePoceada(date: String) {
        dateGamePoceada = date
        notifyDataSetChanged()
    }

    fun addDateGamePoceadaPlus(date: String) {
        dateGamePoceadaPlus = date
        notifyDataSetChanged()
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: Game) = itemView.apply {
            val utiles = Utiles(context)
            when (item.type) {
                Config.QUINI_TYPE -> {
                    containerItemGame.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayQuini)
                    titleGame.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    prizeGame.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    takenPrizeGame.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    dateGame.textColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    dateGame.text = dateGameQuini
                }
                Config.BRINCO_TYPE -> {
                    containerItemGame.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayBrinco)
                    dateGame.text = dateGameBrinco
                }
                Config.LOTO_TYPE -> {
                    containerItemGame.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayLoto)
                    dateGame.text = dateGameLoto
                }
                Config.POCEADA_PLUS_TYPE -> {
                    containerItemGame.backgroundColor = ContextCompat.getColor(context, R.color.backgroundMyPlayPoceadaPlus)
                    dateGame.text = dateGamePoceadaPlus
                }
                Config.POCEADA_TYPE -> {
                    containerItemGame.backgroundColor = ContextCompat.getColor(context, R.color.backgroundPoceada)
                    dateGame.text = dateGamePoceada
                }
            }

            val title = when (item.type) {
                Config.QUINI_TYPE -> {
                    "QUINI 6"
                }
                Config.BRINCO_TYPE -> {
                    "BRINCO"
                }
                Config.LOTO_TYPE -> {
                    "LOTO"
                }
                Config.POCEADA_PLUS_TYPE -> {
                    "POCEADA PLUS"
                }
                Config.POCEADA_TYPE -> {
                    "POCEADA"
                }
                else -> {
                    ""
                }
            }
            titleGame.text = title
            try {
                prizeGame.text = Utiles.formatearNumero(item.prize)
            } catch (ignored: Exception) {
            }
            if (!item.isTaken) {
                takenPrizeGame.text = context.getString(R.string.lblVacancy)
            } else {
                //takenPrizeGame.text = context.getString(R.string.lblTaken)
                takenPrizeGame.text = utiles.obtenerNumeroGanadores(item.winners)
            }
            //dateGame.text = item.date

            containerItemGame.setOnClickListener {
                goToActivity(item, context)
            }
            imageItemGame.setOnClickListener {
                goToActivity(item, context)
            }
        }

        fun goToActivity(item: Game, context: Context) {
            when (item.type) {
                Config.QUINI_TYPE -> {
                    val intent = Intent(context, QuiniPlayActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    context.startActivity(intent)
                    Bungee.slideLeft(context)
                }
                Config.BRINCO_TYPE -> {
                    val intent = Intent(context, BrincoPlayActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    context.startActivity(intent)
                    Bungee.slideLeft(context)
                }
                Config.LOTO_TYPE -> {
                    val intent = Intent(context, LotoPlayActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    context.startActivity(intent)
                    Bungee.slideLeft(context)
                }
                Config.POCEADA_PLUS_TYPE -> {
                    val intent = Intent(context, PoceadaPlusPlayActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    context.startActivity(intent)
                    Bungee.slideLeft(context)
                }
                Config.POCEADA_TYPE -> {
                    val intent = Intent(context, PoceadaPlayActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    context.startActivity(intent)
                    Bungee.slideLeft(context)
                }
                else -> {
                    ""
                }
            }
        }

    }
}