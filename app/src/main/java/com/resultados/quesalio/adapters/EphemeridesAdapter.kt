package com.resultados.quesalio.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.resultados.quesalio.R
import com.resultados.quesalio.entities.Ephemerides
import com.resultados.quesalio.extras.Config
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_ephemerides.view.*
import android.support.v4.util.Pair as UtilPair


internal class EphemeridesAdapter(private val ephemeridesList: MutableList<Ephemerides>) : RecyclerView.Adapter<EphemeridesAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: EphemeridesAdapter.ViewHolder, position: Int) {
        holder.bindItems(ephemeridesList.get(position))
    }

    override fun getItemCount(): Int = ephemeridesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EphemeridesAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_ephemerides, parent, false)
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: Ephemerides) = with(itemView) {
            itemView.apply {
                numberEphemerides.text = item.number
                txtEphemerides.text = item.description.toUpperCase()
                if (!item.image.isEmpty()) {
                    Picasso.with(context)
                            .load(Config.EPHEMERIDES_IMAGE_URL + item.image)
                            .into(imageEphemerides)
                }
            }
        }


    }


}