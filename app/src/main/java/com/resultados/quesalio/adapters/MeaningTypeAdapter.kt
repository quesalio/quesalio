package com.resultados.quesalio.adapters

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.resultados.quesalio.R
import com.resultados.quesalio.activities.DetailMeaningActivity
import com.resultados.quesalio.activities.DetailMeaningNameActivity
import com.resultados.quesalio.entities.MeaningType
import kotlinx.android.synthetic.main.item_meaning_type.view.*
import spencerstudios.com.bungeelib.Bungee
import android.support.v4.util.Pair as UtilPair


internal class MeaningTypeAdapter(private val meaningTypeList: MutableList<MeaningType>) : RecyclerView.Adapter<MeaningTypeAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: MeaningTypeAdapter.ViewHolder, position: Int) {
        holder.bindItems(meaningTypeList.get(position))
    }

    override fun getItemCount(): Int = meaningTypeList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeaningTypeAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_meaning_type, parent, false)
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: MeaningType) = with(itemView) {
            txtItemMeaningType.text = item.name
            this.setOnClickListener { _ ->
                val intent: Intent
                val bundle = Bundle()
                bundle.putString("id", item.id)
                bundle.putString("name", item.name)
                if (item.isName) {
                    intent = Intent(context, DetailMeaningNameActivity::class.java)
                } else {
                    intent = Intent(context, DetailMeaningActivity::class.java)
                }
                intent.let {
                    it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    it.putExtras(bundle)
                    context.startActivity(it)
                    Bungee.slideLeft(context)
                }

            }
        }
    }
}