package com.resultados.quesalio.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.resultados.quesalio.R
import com.resultados.quesalio.entities.Recomendacion
import com.resultados.quesalio.extras.Utiles
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_recomendation.view.*
import android.support.v4.util.Pair as UtilPair


internal class RecomendationAdapter(private val recomendationList: MutableList<Recomendacion>, private val context: Context) : RecyclerView.Adapter<RecomendationAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: RecomendationAdapter.ViewHolder, position: Int) {
        holder.bindItems(recomendationList.get(position))
    }

    override fun getItemCount(): Int = recomendationList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecomendationAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recomendation, parent, false)
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: Recomendacion) = with(itemView) {
            itemView.apply {
                txtNameRecomendation.text = item.nombre
                txtNumberRecomendation.text = item.recomendacion
                if (item.imagen.isNullOrEmpty()) {
                    loadImageWithNoUser(imageProfileRecomendationAdapter)
                } else {
                    Picasso
                            .with(context)
                            .load(item.imagen)
                            .transform(Utiles.CircleTransform())
                            .centerCrop()
                            .resize(200, 200)
                            .into(imageProfileRecomendationAdapter, object : Callback {
                                override fun onSuccess() {}
                                override fun onError() {
                                    loadImageWithNoUser(imageProfileRecomendationAdapter)
                                }
                            })
                }
            }
        }

        private fun loadImageWithNoUser(imageProfile: ImageView) {
            val avatarToLoad = (Math.random() * 4).toInt() + 1
            val urlToLoad = context.getString(R.string.url_site) + "img_working/no_avatar" + avatarToLoad + ".png"
            Picasso
                    .with(context)
                    .load(urlToLoad)
                    .transform(Utiles.CircleTransform())
                    .centerCrop()
                    .resize(200, 200)
                    .into(imageProfile)
        }

    }


}