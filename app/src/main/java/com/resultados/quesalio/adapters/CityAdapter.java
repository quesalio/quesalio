package com.resultados.quesalio.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.resultados.quesalio.R;

import java.util.List;


public class CityAdapter extends ArrayAdapter {
    private Context context;
    private List<String> ciudades;

    public CityAdapter(Context context, List<String> ciudades) {
        super(context, R.layout.item_ciudad, ciudades);
        this.context = context;
        this.ciudades = ciudades;
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.item_ciudad, parent, false);
        String ciudad = String.format("%-15s", ciudades.get(position));
        //String ciudad = ciudades.get(position);
        //String ciudad = centerString(ciudades.get(position));
        TextView txtCiudad = layout.findViewById(R.id.txtCiudad);
        txtCiudad.setText(ciudad);
        //txtCiudad.setText(ciudades.get(position));
        return layout;
    }

    private static String centerString(String s) {
        return String.format("%-" + 25 + "s", String.format("%" + (s.length() + (25 - s.length()) / 2) + "s", s));
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
