package com.resultados.quesalio.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.resultados.quesalio.R
import com.resultados.quesalio.entities.MeaningTypeDetail
import kotlinx.android.synthetic.main.item_meaning.view.*
import android.support.v4.util.Pair as UtilPair


internal class MeaningAdapter(private val meaningList: MutableList<MeaningTypeDetail>) : RecyclerView.Adapter<MeaningAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: MeaningAdapter.ViewHolder, position: Int) {
        holder.bindItems(meaningList.get(position))
    }

    override fun getItemCount(): Int = meaningList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeaningAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_meaning, parent, false)
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: MeaningTypeDetail) = with(itemView) {
            txtNumberItemMeaning.text = item.number
            txtMeaningItemMeaning.text = item.meaning
            val id = context.resources.getIdentifier(item.image, "drawable", context.packageName)
            imageItemMeaning.setImageResource(id)
        }
    }
}