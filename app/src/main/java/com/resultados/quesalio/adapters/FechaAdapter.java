package com.resultados.quesalio.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.resultados.quesalio.R;
import com.resultados.quesalio.entities.Fecha;

import java.util.List;

public class FechaAdapter extends ArrayAdapter {
    private Context context;
    private List<Fecha> fechas;

    public FechaAdapter(Context context, List<Fecha> fechas) {
        super(context, R.layout.item_fecha, fechas);
        this.context = context;
        this.fechas = fechas;
    }

    private View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.item_fecha, parent, false);
        Fecha fecha = fechas.get(position);
        TextView txtFecha = layout.findViewById(R.id.txtFecha);
        txtFecha.setText(fecha.getLetras());
        return layout;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }
}
