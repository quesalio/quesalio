package com.resultados.quesalio.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.resultados.quesalio.R
import com.resultados.quesalio.entities.Detalle
import android.support.v4.util.Pair as UtilPair


internal class DrawDetailAdapter(private val drawListLeft: MutableList<Detalle>, private val drawListRight: MutableList<Detalle>) : RecyclerView.Adapter<DrawDetailAdapter.ViewHolder>() {
    private val LAYOUT_ONE = 0
    private val LAYOUT_TWO = 1

    override fun onBindViewHolder(holder: DrawDetailAdapter.ViewHolder, position: Int) {
        holder.bindItems(drawListLeft.get(position), drawListRight.get(position))
    }

    override fun getItemCount(): Int = drawListLeft.size

    override fun getItemViewType(position: Int): Int {
        if (position == 0)
            return LAYOUT_ONE
        else
            return LAYOUT_TWO
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawDetailAdapter.ViewHolder {
        lateinit var view: View
        if (viewType == LAYOUT_ONE) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_sorteo_ganador_adapter, parent, false)
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_sorteo_adapter, parent, false)
        }
        return ViewHolder(view)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(itemLeft: Detalle, itemRight: Detalle) = with(itemView) {
            itemView.apply {
                val txtNumeroIzquierda = this.findViewById<TextView>(R.id.txtNumeroIzquierda)
                val txtNumeroDerecha = this.findViewById<TextView>(R.id.txtNumeroDerecha)
                val txtValorIzquierda = this.findViewById<TextView>(R.id.txtValorIzquierda)
                val txtValorDerecha = this.findViewById<TextView>(R.id.txtValorDerecha)
                txtNumeroDerecha.setTextColor(ContextCompat.getColor(context, R.color.green))

                txtNumeroIzquierda.text = "${itemLeft.numero}"
                txtNumeroDerecha.text = "${itemRight.numero}"
                txtValorIzquierda.text = itemLeft.valor
                txtValorDerecha.text = itemRight.valor
            }
        }
    }
}