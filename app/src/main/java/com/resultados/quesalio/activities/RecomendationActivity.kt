package com.resultados.quesalio.activities

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.CheckRecomendacionesQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.SaveRecomendationMutation
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_recomendation.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.longToast
import org.json.JSONException
import spencerstudios.com.bungeelib.Bungee
import java.util.*

class RecomendationActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private lateinit var gso: GoogleSignInOptions
    private val RC_SIGN_IN = 9001
    private var mGoogleApiClient: GoogleApiClient? = null
    private var uriImage: Uri? = null
    private var nombre: String? = ""
    private var idUsuario: String? = ""
    private var callbackManager: CallbackManager? = null
    private var accessTokenTracker: AccessTokenTracker? = null
    private var image_facebook: String? = null
    private var es_facebook: Boolean = false
    private var es_google: Boolean = false
    private var there_is_token: Boolean? = false
    private var adLayout: LinearLayout? = null
    private var dialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recomendation)
        setSupportActionBar(toolbarActivityRecomendation)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
        //facebook
        loginButtonRecomendation
        loginButtonRecomendation.setReadPermissions(Arrays.asList("email", "public_profile"))
        // If using in a fragment
        //loginButtonRecomendation.fragment = this
        // Other app specific specialization
        callbackManager = CallbackManager.Factory.create()
        // Callback registration
        loginButtonRecomendation.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                processFacebookLogin(loginResult.accessToken)
            }

            override fun onCancel() {
                longToast(R.string.cancel_login)
            }

            override fun onError(exception: FacebookException) {
                Log.e("error", exception.toString())
            }
        })

        //google
        signInButtonRecomendation.setSize(SignInButton.SIZE_STANDARD)
        signInButtonRecomendation.setScopes(gso.scopeArray)
        signInButtonRecomendation.setOnClickListener(this)
        setGooglePlusButtonText(signInButtonRecomendation, "Iniciar sesión con Google")
        signOutButtonRecomendation.setOnClickListener(this)
        btnRecomendation.setOnClickListener(this)
        //Log.e("date", "${Config.SDF_RECOMENDATION.format(Date())}")
    }

    private fun processFacebookLogin(accessToken: AccessToken) {
        val parameters = Bundle()
        parameters.putString("fields", "id,name,email,gender, picture.width(200).height(200)")
        GraphRequest(
                accessToken,
                "/me",
                parameters,
                HttpMethod.GET,
                GraphRequest.Callback { response ->
                    try {
                        val `object` = response.jsonObject
                        idUsuario = `object`.getString("id").toString()
                        nombre = `object`.getString("name").toString()
                        val picture = `object`.getJSONObject("picture")
                        val data = picture.getJSONObject("data")
                        es_facebook = true
                        es_google = false
                        image_facebook = data.getString("url")
                        uriImage = null
                        updateUI(true)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
        ).executeAsync()

    }

    protected fun setGooglePlusButtonText(signInButton: SignInButton, buttonText: String) {
        for (i in 0 until signInButton.childCount) {
            val v = signInButton.getChildAt(i)
            if (v is TextView) {
                v.text = buttonText
                return
            }
        }
    }


    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsRecomendation)
        val opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient)
        if (opr.isDone) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            val result = opr.get()
            handleSignInResult(result)
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            dialog = indeterminateProgressDialog(message = getString(R.string.lbl_wait_please), title = getString(R.string.lbl_getting_data))
            opr.setResultCallback { googleSignInResult ->
                dialog?.let { pd ->
                    if (pd.isShowing) {
                        pd.dismiss()
                    }
                }
                handleSignInResult(googleSignInResult)
            }
        }
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Recomendaciones", "Recomendaciones")
    }

    override fun onResume() {
        super.onResume()
        //Ads.showBanner(activity, adLayout!!)
        //val mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity!!)
        //mFirebaseAnalytics.setCurrentScreen(activity!!, "Recomendación", "Recomendación")
        accessTokenTracker = object : AccessTokenTracker() {
            override fun onCurrentAccessTokenChanged(
                    oldAccessToken: AccessToken,
                    currentAccessToken: AccessToken?) {
                if (currentAccessToken != null) {
                    there_is_token = true
                }
            }
        }
        val accessToken = AccessToken.getCurrentAccessToken()
        accessTokenTracker?.startTracking()
        if (accessToken != null) {
            processFacebookLogin(accessToken)
        }
    }


    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    private fun updateUI(signedIn: Boolean) {
        if (signedIn) {
            loginBlockRecomendation.visibility = View.GONE
            layoutNumbersRecomendation.visibility = View.VISIBLE
        } else {
            loginBlockRecomendation.visibility = View.VISIBLE
            layoutNumbersRecomendation.visibility = View.GONE
        }
        dialog?.let { pd ->
            if (pd.isShowing) {
                pd.dismiss()
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.signInButtonRecomendation -> signIn()
            R.id.signOutButtonRecomendation -> signOut()
            R.id.btnRecomendation -> check()
            else -> {
            }
        }
    }

    private fun check() {
        MyApplication.sInstance.apolloClient.query(CheckRecomendacionesQuery.builder().id_user(idUsuario!!).build()).enqueue(object : ApolloCall.Callback<CheckRecomendacionesQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<CheckRecomendacionesQuery.Data>) {
                runOnUiThread {
                    response.data()?.check_recomendations()?.let { amount ->
                        if (amount < 5) {
                            makeRecomendation()
                        } else {
                            longToast(R.string.lblMaxAmountOfRecomendations)
                            return@runOnUiThread
                        }
                    }
                }
            }

        })

    }

    private fun makeRecomendation() {
        val recomendation = txtRecomendation1.text.toString().trim()
        val size = recomendation.length
        if (size < 2) {
            longToast(R.string.lblShowMoreChars)
            return
        } else {
            MyApplication.sInstance.apolloClient.mutate(
                    SaveRecomendationMutation.builder()
                            .date(Config.SDF_RECOMENDATION.format(Date()))
                            .image(if (uriImage != null) uriImage!!.toString() else image_facebook)
                            .recomendation(recomendation)
                            .name(nombre)
                            .is_facebook(if (es_facebook) "1" else "0")
                            .is_google(if (es_google) "1" else "0")
                            .id_user(idUsuario).build()).enqueue(object : ApolloCall.Callback<SaveRecomendationMutation.Data>() {
                override fun onFailure(e: ApolloException) {

                }

                override fun onResponse(response: Response<SaveRecomendationMutation.Data>) {
                    response.data()?.save_recomendation()?.id().let {
                        val intent = Intent(this@RecomendationActivity, ShowRecomendationActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                        startActivity(intent)
                        Bungee.slideLeft(this@RecomendationActivity)
                        finish()
                    }
                }

            })
        }
    }

    private fun signIn() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }

    }

    private fun signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
            // [START_EXCLUDE]
            //mGoogleApiClient.disconnect();
            updateUI(false)
            // [END_EXCLUDE]
        }
    }

    private fun revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback {
            // [START_EXCLUDE]
            updateUI(false)
            // [END_EXCLUDE]
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val acct = result.signInAccount
            uriImage = acct!!.photoUrl
            nombre = acct.displayName
            idUsuario = acct.id
            image_facebook = null
            es_google = true
            es_facebook = false
            uriImage?.let {
                Picasso.with(this).load(it).resize(100, 100).centerCrop().into(imageProfileRecomendation)
            }
            updateUI(true)
        } else {
            if (!there_is_token!!) {
                updateUI(false)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            mGoogleApiClient!!.stopAutoManage(this)
            mGoogleApiClient!!.disconnect()
        } catch (e: Exception) {
        }

        if (accessTokenTracker != null) {
            accessTokenTracker!!.stopTracking()
        }

    }

    override fun onPause() {
        try {
            mGoogleApiClient!!.stopAutoManage(this)
            mGoogleApiClient!!.disconnect()
        } catch (e: Exception) {
        }

        super.onPause()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }


}
