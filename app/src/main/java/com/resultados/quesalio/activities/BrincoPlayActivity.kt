package com.resultados.quesalio.activities

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.GamePlayAdapter
import com.resultados.quesalio.entities.GameItem
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.database
import kotlinx.android.synthetic.main.activity_brinco_play.*
import org.jetbrains.anko.toast
import spencerstudios.com.bungeelib.Bungee

class BrincoPlayActivity : AppCompatActivity() {

    val numberList = mutableListOf<GameItem>()
    var finalList = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brinco_play)
        setSupportActionBar(toolbarActivityBrincoPlay)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        containerListBrincoPlay.layoutManager = GridLayoutManager(this, 10)
        containerListBrincoPlay.setHasFixedSize(true)
        for (i in 0..39) {
            val value: String = if (i < 10) "0$i" else "$i"
            val item = GameItem(value)
            item.type = Config.BRINCO_TYPE
            numberList.add(item)
        }
        val adapter = GamePlayAdapter(numberList)
        containerListBrincoPlay.adapter = adapter
        btnContinueBrincoPlay.setOnClickListener {
            finalList = adapter.finalList
            if (finalList.size < 6) {
                toast(getString(R.string.lblSelectSixNumbers))
                return@setOnClickListener
            }
            database.use {
                val numbers = finalList.joinToString(",")
                val values = ContentValues()
                values.put("type", Config.BRINCO_TYPE)
                values.put("numbers", numbers)
                this.insert(Config.PLAYS_TABLE, null, values)
            }
            val intent = Intent(this, PlayCompletedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            finish()
        }
        imageAzarBrinco.setOnClickListener {
            val randomList = mutableListOf<GameItem>()
            do {
                val randomInteger = (0..39).shuffled().first()
                val value: String = if (randomInteger < 10) "0$randomInteger" else "$randomInteger"
                val item = GameItem(value)
                item.type = Config.BRINCO_TYPE
                if (!randomList.contains(item)) {
                    randomList.add(item)
                }
            } while (randomList.size < 6)
            adapter.setRandomValues(randomList)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsBrincoPlay)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Jugada Brinco", "Jugada Brinco")
    }
}
