package com.resultados.quesalio.activities

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.TextView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.*
import com.resultados.quesalio.adapters.DrawDetailAdapter
import com.resultados.quesalio.entities.Detalle
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils.sortMatutinasSingle
import com.resultados.quesalio.extras.ListUtils.sortNocturnasSingle
import com.resultados.quesalio.extras.ListUtils.sortPrimerasSingle
import com.resultados.quesalio.extras.ListUtils.sortVespertinasSingle
import kotlinx.android.synthetic.main.activity_detail_quiniela.*
import kotlinx.android.synthetic.main.view_detail_first.*
import org.jetbrains.anko.runOnUiThread
import spencerstudios.com.bungeelib.Bungee


class DetailQuinielaActivity : AppCompatActivity() {
    private lateinit var idDraw: String
    private lateinit var currentDate: String
    private lateinit var currentDateString: String
    private lateinit var drawName: String
    private lateinit var province: String
    private lateinit var idQuiniela: String
    private lateinit var idLoteria: String
    private lateinit var chosenIds: String
    private lateinit var inflater: LayoutInflater
    private lateinit var city: String
    private lateinit var meaning: String
    private lateinit var meaningUrl: String
    private lateinit var meaningNumber: String
    private lateinit var numbers: IntArray
    private lateinit var values: Array<String>
    private var adsNormal: Ads? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_quiniela)
        supportPostponeEnterTransition()

        setSupportActionBar(toolbarDetailquiniela)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val bundle = intent.extras
        bundle?.let {
            idDraw = it.getString("id_sorteo")!!
            currentDate = it.getString("fecha")!!
            currentDateString = it.getString("dia", "")
            drawName = it.getString("nombre_sorteo", "")
            province = it.getString("provincia", "")
            idQuiniela = it.getString("id_quiniela", "")
            idLoteria = it.getString("id_loteria", "")
            chosenIds = it.getString("id_elegidos", "")
            //numbers = it.getStringArray("numeros")!!
            numbers = it.getIntArray("numeros")!!
            values = it.getStringArray("valores")!!
            city = it.getString("ciudad", "")
            meaning = it.getString("significado", "")
            meaningUrl = it.getString("significado_url", "")
            meaningNumber = it.getString("significado_numero", "")
            lblDateDetail.text = currentDateString
            updateDraw()
        }
    }

    private fun updateDraw() {
        inflater = LayoutInflater.from(this)
        containerQuinielaDetail?.removeAllViews()
        var nombre = drawName.substring(0, 1).toUpperCase() + drawName.substring(1)
        when (drawName) {
            Config.PRIMERA_TYPE -> {
                nombre = "La $nombre"
                getFirstDraw(Config.PRIMERA_TYPE, nombre)
                getPrimera(Config.PRIMERA_HOUR, nombre)
            }
            Config.MATUTINA_TYPE -> {
                getFirstDraw(Config.MATUTINA_TYPE, nombre)
                getMatutina(Config.MATUTINA_HOUR, nombre)
            }
            Config.VESPERTINA_TYPE -> {
                getFirstDraw(Config.VESPERTINA_TYPE, nombre)
                getVespertina(Config.VESPERTINA_HOUR, nombre)
            }
            Config.NOCTURNA_TYPE -> {
                getFirstDraw(Config.NOCTURNA_TYPE, nombre)
                getNocturna(Config.NOCTURNA_HOUR, nombre)
            }
            else -> {
            }
        }
    }

    private fun getFirstDraw(type: String, name: String) {
        val hour = when (type) {
            Config.PRIMERA_TYPE -> {
                Config.PRIMERA_HOUR
            }
            Config.MATUTINA_TYPE -> {
                Config.MATUTINA_HOUR
            }
            Config.VESPERTINA_TYPE -> {
                Config.VESPERTINA_HOUR
            }
            Config.NOCTURNA_TYPE -> {
                Config.NOCTURNA_HOUR
            }
            else -> {
                ""
            }
        }
        var thereAreNumbers = false

        val anim = AlphaAnimation(0.1f, 1.0f)
        anim.duration = 500
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE

        val sorteo1 = mutableListOf<Detalle>()
        val sorteo2 = mutableListOf<Detalle>()
        val view = inflater.inflate(R.layout.view_detail_first, containerQuinielaDetail, false)
        val txtDrawViewDetailFirst = view.findViewById<TextView>(R.id.txtDrawViewDetailFirst)
        txtDrawViewDetailFirst.text = name.toUpperCase()
        val txtTimeViewDetailFirst = view.findViewById<TextView>(R.id.txtTimeViewDetailFirst)
        txtTimeViewDetailFirst.text = hour.toUpperCase()
        val txtProvinceViewDetailFirst = view.findViewById<TextView>(R.id.txtProvinceViewDetailFirst)
        txtProvinceViewDetailFirst.text = city.toUpperCase()

        val txtMeaningViewDetail = view.findViewById<TextView>(R.id.txtMeaningViewDetailFirst)
        val imageMeaningViewDetail = view.findViewById<ImageView>(R.id.imageMeaningViewDetailFirst)
        val txtFirstNumberViewDetailFirst = view.findViewById<TextView>(R.id.txtFirstNumberViewDetailFirst)

        ViewCompat.setTransitionName(imageMeaningViewDetail, getString(R.string.imageMeaningItemDrawTransition))
        ViewCompat.setTransitionName(txtFirstNumberViewDetailFirst, getString(R.string.txtValueItemDraw))
        ViewCompat.setTransitionName(txtProvinceViewDetailFirst, getString(R.string.txtCityItemDraw))

        meaning.let {
            txtMeaningViewDetail.visibility = View.VISIBLE
            txtMeaningViewDetail.text = meaning
        }

        meaningNumber.let {
            txtFirstNumberViewDetailFirst?.text = it
        }

        meaningUrl.let {
            Glide.with(applicationContext)
                    .load(getString(R.string.url_meanings) + it)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }
                    })
                    .into(imageMeaningViewDetail)
        }
        var firstValue: String? = null
        numbers.forEachIndexed { index, number ->
            val detalle = Detalle()
            detalle.numero = number
            var value = values.get(index)
            if (value.equals("null") || value.isEmpty()) {
                value = ""
            }
            detalle.valor = value
            if (number <= 10) {
                sorteo1.add(detalle)
            } else {
                sorteo2.add(detalle)
            }
            if (!number.equals("1")) {
                if (value.isNotEmpty() && !value.equals("----")) {
                    thereAreNumbers = true
                }
            }
            if (number == 1) {
                if (!value.isEmpty()) {
                    firstValue = value
                }
            }
        }


        if (firstValue.isNullOrEmpty() || firstValue.equals("----")) {
            if (thereAreNumbers) {
                txtFirstNumberViewDetailFirst?.text = "----"
                try {
                    imageMeaningViewDetailFirst?.let {
                        Glide.with(applicationContext)
                                .load(getString(R.string.shaking))
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        supportStartPostponedEnterTransition()
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        supportStartPostponedEnterTransition()
                                        return false
                                    }
                                })
                                .into(it)
                    }
                } catch (ignored: Exception) {
                }
                txtFirstNumberViewDetailFirst?.animation = anim
            }
        }
        if (thereAreNumbers) {
            val containerListDrawViewDetail = view.findViewById<RecyclerView>(R.id.containerListDrawViewDetailFirst)
            val adapter = DrawDetailAdapter(sorteo1, sorteo2)
            containerListDrawViewDetail.layoutManager = LinearLayoutManager(this)
            containerListDrawViewDetail.adapter = adapter
            containerQuinielaDetail?.addView(view)
        }
        var chosenIdAvailable = chosenIds.split(",")
        chosenIdAvailable = chosenIdAvailable.filter {
            !it.equals(idLoteria)
        }
        chosenIds = chosenIdAvailable.joinToString(",")
    }


    private fun getPrimera(hour: String, name: String) {
        MyApplication.sInstance.apolloClient.query(GetPrimeraQuery.builder().date(currentDate).ids(chosenIds).build()).enqueue(object : ApolloCall.Callback<GetPrimeraQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetPrimeraQuery.Data>) {
                response.data()?.primera()?.let { primeras ->
                    val newPrimeras = sortPrimerasSingle(primeras, chosenIds.split(","))
                    newPrimeras.forEach { primera ->
                        var hasContent = false
                        primera.numbers()?.forEach { number ->
                            if (!number.value().isNullOrEmpty()) {
                                hasContent = true
                            }
                        }
                        applicationContext.runOnUiThread {
                            if (hasContent) {
                                buildRow(primera.city(), hour, name, primera.meaning(), primera.meaning_image(), primera.meaning_number(), primera.numbers(), null, null, null)
                            }
                        }
                    }
                }
            }

        })
    }

    private fun getMatutina(hour: String, name: String) {
        MyApplication.sInstance.apolloClient.query(GetMatutinaQuery.builder().date(currentDate).ids(chosenIds).build()).enqueue(object : ApolloCall.Callback<GetMatutinaQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetMatutinaQuery.Data>) {
                response.data()?.matutina()?.let { matutinas ->
                    val newMatutinas = sortMatutinasSingle(matutinas, chosenIds.split(","))
                    newMatutinas.forEach { matutina ->
                        var hasContent = false
                        matutina.numbers()?.forEach { number ->
                            if (!number.value().isNullOrEmpty()) {
                                hasContent = true
                            }
                        }
                        applicationContext.runOnUiThread {
                            if (hasContent) {
                                buildRow(matutina.city(), hour, name, matutina.meaning(), matutina.meaning_image(), matutina.meaning_number(), null, matutina.numbers(), null, null)
                            }
                        }
                    }
                }
            }

        })
    }

    private fun getVespertina(hour: String, name: String) {
        MyApplication.sInstance.apolloClient.query(GetVespertinaQuery.builder().date(currentDate).ids(chosenIds).build()).enqueue(object : ApolloCall.Callback<GetVespertinaQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetVespertinaQuery.Data>) {
                response.data()?.vespertina()?.let { vespertinas ->
                    val newVespertinas = sortVespertinasSingle(vespertinas, chosenIds.split(","))
                    newVespertinas.forEach { vespertina ->
                        var hasContent = false
                        vespertina.numbers()?.forEach { number ->
                            if (!number.value().isNullOrEmpty()) {
                                hasContent = true
                            }
                        }
                        applicationContext.runOnUiThread {
                            if (hasContent) {
                                buildRow(vespertina.city(), hour, name, vespertina.meaning(), vespertina.meaning_image(), vespertina.meaning_number(), null, null, vespertina.numbers(), null)
                            }
                        }
                    }

                }
            }

        })
    }

    private fun getNocturna(hour: String, name: String) {
        MyApplication.sInstance.apolloClient.query(GetNocturnaQuery.builder().date(currentDate).ids(chosenIds).build()).enqueue(object : ApolloCall.Callback<GetNocturnaQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetNocturnaQuery.Data>) {
                response.data()?.nocturna()?.let { nocturnas ->
                    val newNocturnas = sortNocturnasSingle(nocturnas, chosenIds.split(","))
                    newNocturnas.forEach { nocturna ->
                        var hasContent = false
                        nocturna.numbers()?.forEach { number ->
                            if (!number.value().isNullOrEmpty()) {
                                hasContent = true
                            }
                        }
                        applicationContext.runOnUiThread {
                            if (hasContent) {
                                buildRow(nocturna.city(), hour, name, nocturna.meaning(), nocturna.meaning_image(), nocturna.meaning_number(), null, null, null, nocturna.numbers())
                            }
                        }
                    }
                }
            }

        })
    }

    private fun buildRow(city: String?, hour: String, name: String, meaning: String?, meaning_image: String?, meaning_number: String?, primeras: MutableList<GetPrimeraQuery.Number>?, matutinas: MutableList<GetMatutinaQuery.Number>?, vespertinas: MutableList<GetVespertinaQuery.Number>?, nocturnas: MutableList<GetNocturnaQuery.Number>?) {
        var thereAreNumbersPrimera = false
        var thereAreNumbersMatutina = false
        var thereAreNumbersVespertina = false
        var thereAreNumbersNocturna = false

        val anim = AlphaAnimation(0.1f, 1.0f)
        anim.duration = 500
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE

        val sorteo1 = mutableListOf<Detalle>()
        val sorteo2 = mutableListOf<Detalle>()
        val view = inflater.inflate(R.layout.view_detail, containerQuinielaDetail, false)
        view.findViewById<TextView>(R.id.txtDrawViewDetail).text = name.toUpperCase()
        view.findViewById<TextView>(R.id.txtTimeViewDetail).text = hour.toUpperCase()
        view.findViewById<TextView>(R.id.txtProvinceViewDetail).text = city?.toUpperCase()

        val txtMeaningViewDetail = view.findViewById<TextView>(R.id.txtMeaningViewDetail)
        meaning?.let {
            txtMeaningViewDetail.visibility = View.VISIBLE
            txtMeaningViewDetail.text = meaning
        }
        val txtFirstNumberViewDetail = view.findViewById<TextView>(R.id.txtFirstNumberViewDetail)
        meaning_number?.let {
            txtFirstNumberViewDetail?.text = it
        }
        val imageMeaningViewDetail = view.findViewById<ImageView>(R.id.imageMeaningViewDetail)
        meaning_image?.let {

            Glide.with(applicationContext)
                    .load(getString(R.string.url_meanings) + meaning_image)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }
                    })
                    .into(imageMeaningViewDetail)
        }

        primeras?.forEach { number ->
            val detalle = Detalle()
            detalle.numero = number.number()!!.toInt()
            var value = number.value().toString()
            if (value.equals("null")) {
                value = ""
            }
            detalle.valor = value
            if (number.number()!!.toInt() <= 10) {
                sorteo1.add(detalle)
            } else {
                sorteo2.add(detalle)
            }

            number.number().let { num ->
                //if (!num.equals("1")) {
                if (num != 1) {
                    number.value()?.let {
                        if (it.isNotEmpty() && !it.equals("----")) {
                            thereAreNumbersPrimera = true
                        }
                    }
                }
            }
        }
        matutinas?.forEach { number ->
            val detalle = Detalle()
            detalle.numero = number.number()!!
            detalle.valor = number.value().toString()
            if (number.number()!!.toInt() <= 10) {
                sorteo1.add(detalle)
            } else {
                sorteo2.add(detalle)
            }

            number.number().let { num ->
                if (num != 1) {
                    number.value()?.let {
                        if (it.isNotEmpty() && !it.equals("----")) {
                            thereAreNumbersMatutina = true
                        }
                    }
                }
            }
        }
        vespertinas?.forEach { number ->
            val detalle = Detalle()
            detalle.numero = number.number()!!
            detalle.valor = number.value().toString()
            if (number.number()!!.toInt() <= 10) {
                sorteo1.add(detalle)
            } else {
                sorteo2.add(detalle)
            }

            number.number().let { num ->
                if (num != 1) {
                    number.value()?.let {
                        if (it.isNotEmpty() && !it.equals("----")) {
                            thereAreNumbersVespertina = true
                        }
                    }
                }
            }
        }

        nocturnas?.forEach { number ->
            val detalle = Detalle()
            detalle.numero = number.number()!!
            detalle.valor = number.value().toString()
            if (number.number()!!.toInt() <= 10) {
                sorteo1.add(detalle)
            } else {
                sorteo2.add(detalle)
            }

            number.number().let { num ->
                //if (!num.equals("1")) {
                if (num != 1) {
                    number.value()?.let {
                        if (it.isNotEmpty() && !it.equals("----")) {
                            thereAreNumbersNocturna = true
                        }
                    }
                }
            }
        }

        var value: String? = null
        primeras?.forEach { number ->
            val n = number.number()!!.toInt()
            if (n == 1) {
                number.value()?.let {
                    value = it
                    return@forEach
                }
            }
        }
        matutinas?.forEach { number ->
            val n = number.number()!!.toInt()
            if (n == 1) {
                number.value()?.let {
                    value = it
                    return@forEach
                }
            }
        }
        vespertinas?.forEach { number ->
            val n = number.number()!!.toInt()
            if (n == 1) {
                number.value()?.let {
                    value = it
                    return@forEach
                }
            }
        }
        nocturnas?.forEach { number ->
            val n = number.number()!!.toInt()
            if (n == 1) {
                number.value()?.let {
                    value = it
                    return@forEach
                }
            }
        }
        if (value.isNullOrEmpty() || value.equals("----")) {
            if (thereAreNumbersPrimera || thereAreNumbersMatutina || thereAreNumbersVespertina || thereAreNumbersNocturna) {
                txtFirstNumberViewDetail?.text = "----"
                try {
                    imageMeaningViewDetail?.let {
                        Glide.with(applicationContext)
                                .load(getString(R.string.shaking))
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        supportStartPostponedEnterTransition()
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        supportStartPostponedEnterTransition()
                                        return false
                                    }
                                })
                                .into(it)
                    }
                } catch (ignored: Exception) {
                }
                txtFirstNumberViewDetail?.animation = anim
            }
        }
        val containerListDrawViewDetail = view.findViewById<RecyclerView>(R.id.containerListDrawViewDetail)
        val adapter = DrawDetailAdapter(sorteo1, sorteo2)
        containerListDrawViewDetail.layoutManager = LinearLayoutManager(this)
        containerListDrawViewDetail.adapter = adapter
        containerQuinielaDetail?.addView(view)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                Bungee.slideRight(this)
                finish()
            }
        }
        return true
    }

    override fun onBackPressed() {
        adsNormal?.let {
            if (it.isEndInterstitialLoaded) {
                adsNormal?.showEndInterstitial()
                adsNormal?.loadNormalInterstitial()
            }
        }
        super.onBackPressed()

    }

    override fun onStart() {
        super.onStart()
        adsNormal = Ads(this)
        adsNormal?.loadNormalInterstitial()
        Ads.showBanner(this, adsDetail)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Detalle Quiniela", "Detalle Quiniela")
    }
}
