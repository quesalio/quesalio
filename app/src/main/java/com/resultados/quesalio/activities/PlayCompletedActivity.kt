package com.resultados.quesalio.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.resultados.quesalio.R
import kotlinx.android.synthetic.main.activity_play_completed.*
import spencerstudios.com.bungeelib.Bungee

class PlayCompletedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_completed)
        btnGoToMyPlays.setOnClickListener {
            backTo()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        backTo()
    }

    fun backTo() {
        val intent = Intent(this, PlaysActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        startActivity(intent)
        Bungee.slideRight(this)
        finish()
    }
}
