package com.resultados.quesalio.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.*
import com.resultados.quesalio.adapters.MyPlaysAdapter
import com.resultados.quesalio.entities.Play
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.database
import kotlinx.android.synthetic.main.activity_plays.*
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.SqlOrderDirection
import org.jetbrains.anko.db.select
import spencerstudios.com.bungeelib.Bungee
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

class PlaysActivity : AppCompatActivity() {
    var listOfMyPlays = mutableListOf<Play>()
    private lateinit var adapter: MyPlaysAdapter
    private lateinit var calendar: Calendar
    private var daySelected: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plays)
        setSupportActionBar(toolbarActivityPlays)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        btnNewPlay.setOnClickListener {
            val intent = Intent(this, WePlayingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
        containerListCurrentPlaying.setHasFixedSize(true)
        containerListCurrentPlaying.layoutManager = LinearLayoutManager(this)
    }

    override fun onStart() {
        super.onStart()
        calendar = Calendar.getInstance()
        daySelected = calendar.get(Calendar.DAY_OF_WEEK)
        getPoceada()
        getPoceadaPlus()
        getBrinco()
        getQuini()
        getLoto()
        listOfMyPlays = getMyPlays()
        adapter = MyPlaysAdapter(listOfMyPlays)
        containerListCurrentPlaying.adapter = adapter
        Ads.showBanner(this, adsPlays)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Jugadas", "Jugadas")
    }

    private fun getPoceada() {
        MyApplication.sInstance.apolloClient.query(GetPoceadaQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetPoceadaQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetPoceadaQuery.Data>) {
                val numbers = mutableListOf<String>()
                var date: Date? = Date()
                var prize = ""
                response.data()?.poceada()?.get(0).let { poceada ->
                    poceada?.numbers()?.forEach {
                        numbers.add(it.trim())
                    }
                    date = poceada?.date()
                    poceada?.prizes()?.forEach { pr ->
                        if (pr.hits()!!.toInt() == 8 && pr.winners()!!.toInt() == 0) {
                            prize = pr.prize().toString().trim()
                        }
                    }
                }
                runOnUiThread {
                    adapter.addPoceadaPrize(prize)
                    adapter.addPoceada(numbers, date)
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.POCEADA_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            adapter.addPoceadaSchedule(convertDayToDate(schedule.day().toString()), schedule.hour().toString())
                        }
                    }
                }
            }

        })
    }

    private fun getPoceadaPlus() {
        MyApplication.sInstance.apolloClient.query(GetPoceadaPlusQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetPoceadaPlusQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetPoceadaPlusQuery.Data>) {
                val numbers = mutableListOf<String>()
                var date: Date? = Date()
                var prize = ""
                response.data()?.poceada_plus()?.get(0).let { poceada_plus ->
                    poceada_plus?.numbers()?.forEach {
                        numbers.add(it.trim())
                    }
                    date = poceada_plus?.date()
                    poceada_plus?.prizes()?.forEach { pr ->
                        if (pr.hits()!!.toInt() == 8 && pr.winners()!!.toInt() == 0) {
                            prize = pr.prize().toString().trim()
                        }
                    }
                }
                runOnUiThread {
                    adapter.addPoceadaPlusPrize(prize)
                    adapter.addPoceadaPlus(numbers, date)
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.POCEADA_PLUS_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            adapter.addPoceadaPlusSchedule(convertDayToDate(schedule.day().toString()), schedule.hour().toString())
                        }
                    }
                }
            }

        })
    }

    private fun getBrinco() {
        MyApplication.sInstance.apolloClient.query(GetBrincoQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetBrincoQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetBrincoQuery.Data>) {
                val numbers = mutableListOf<String>()
                var date: Date? = Date()
                var prize = ""
                response.data()?.brinco()?.get(0).let { brinco ->
                    brinco?.numbers()?.forEach {
                        numbers.add(it.trim())
                    }
                    date = brinco?.date()
                    prize = brinco?.next_prize().toString().trim()
                }
                runOnUiThread {
                    adapter.addBrincoPrize(prize)
                    adapter.addBrinco(numbers, date)
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.BRINCO_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            adapter.addBrincoSchedule(convertDayToDate(schedule.day().toString()), schedule.hour().toString())
                        }
                    }
                }
            }

        })
    }

    private fun getQuini() {
        MyApplication.sInstance.apolloClient.query(GetQuini6Query.builder().build()).enqueue(object : ApolloCall.Callback<GetQuini6Query.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetQuini6Query.Data>) {
                val numbers = mutableListOf<String>()
                val numbersSegundaTradicional = mutableListOf<String>()
                val numbersSiempreSale = mutableListOf<String>()
                val numbersRevancha = mutableListOf<String>()
                var date: Date? = Date()
                var prize = ""
                response.data()?.quini6()?.get(0).let { quini ->
                    date = quini?.date()
                    quini?.tradicional_numbers()?.forEach {
                        numbers.add(it.trim())
                    }
                    quini?.segunda_vuelta_numbers()?.forEach {
                        numbersSegundaTradicional.add(it.trim())
                    }
                    quini?.revancha_numbers()?.forEach {
                        numbersRevancha.add(it.trim())
                    }
                    quini?.siempre_sale_numbers()?.forEach {
                        numbersSiempreSale.add(it.trim())
                    }
                    prize = quini?.prize_next_move().toString().trim()
                }
                runOnUiThread {
                    adapter.addQuiniPrize(prize)
                    adapter.addQuini(numbers, date)
                    adapter.addQuiniExtras(segundaTradicional = numbersSegundaTradicional, siempreSale = numbersSiempreSale, revancha = numbersRevancha)
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.QUINI_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            adapter.addQuiniSchedule(convertDayToDate(schedule.day().toString()), schedule.hour().toString())
                        }
                    }
                }
            }

        })
    }

    private fun getLoto() {
        MyApplication.sInstance.apolloClient.query(GetLotoQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetLotoQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetLotoQuery.Data>) {
                val numbers = mutableListOf<String>()
                val numbersSiempreSale = mutableListOf<String>()
                val numbersDesquite = mutableListOf<String>()
                val jacks = mutableListOf<String>()
                var prize = ""
                var date: Date? = Date()
                response.data()?.loto()?.get(0).let { loto ->
                    date = loto?.date()
                    loto?.tradicional_numbers()?.forEach {
                        numbers.add(it.trim())
                    }
                    loto?.desquite_numbers()?.forEach {
                        numbersDesquite.add(it.trim())
                    }
                    loto?.sale_sale_numbers()?.forEach {
                        numbersSiempreSale.add(it.trim())
                    }
                    loto?.jackpot1_tradicional()?.let {
                        jacks.add(it.trim())
                    }
                    loto?.jackpot2_tradicional()?.let {
                        jacks.add(it.trim())
                    }
                    prize = loto?.prize_next_move().toString().trim()
                    getScheduleLoto(date)
                }
                runOnUiThread {
                    adapter.addLotoPrize(prize)
                    adapter.addLoto(numbers, date)
                    adapter.addLotoExtras(siempreSale = numbersSiempreSale, desquite = numbersDesquite, jackpotsTradicional = jacks)
                }
            }
        })

    }
    private fun getScheduleLoto(date: Date?) {
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.LOTO_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            adapter.addLotoSchedule(convertDayToDate(schedule.day().toString()), schedule.hour().toString())
                        }
                    }
                }
            }

        })
    }

    private fun convertDayToDate(day: String): String {
        val calendar = Calendar.getInstance()
        var daySelected = day.toInt()
        if (day.equals("8")) {
            daySelected = 1
        }
        calendar.set(Calendar.DAY_OF_WEEK, daySelected)
        val dateFormat = SimpleDateFormat("dd/MM")
        dateFormat.timeZone = calendar.getTimeZone();
        val calendarToday = Calendar.getInstance()
        val milis1 = calendar.getTimeInMillis()
        val milis2 = calendarToday.getTimeInMillis()
        val diff = milis1 - milis2
        val diffDays = diff / (24 * 60 * 60 * 1000)
        val diferencia = BigDecimal(diffDays).intValueExact()
        var ddday = ""
        when (diferencia) {
            1 -> ddday = "Mañana"
            0 -> ddday = "Hoy"
        }
        if (ddday.isEmpty()) {
            ddday = when (daySelected) {
                1 -> "Domingo"
                2 -> "Lunes"
                3 -> "Martes"
                4 -> "Miércoles"
                5 -> "Jueves"
                6 -> "Viernes"
                7 -> "Sábado"
                else -> ""
            }
        }
        return "$ddday ${dateFormat.format(calendar.getTime())}"
    }

    private fun getMyPlays() = database.use {
        val plays = mutableListOf<Play>()
        select(Config.PLAYS_TABLE, "id,type,numbers").orderBy("id", SqlOrderDirection.DESC).parseList(object : MapRowParser<List<Play>> {
            override fun parseRow(columns: Map<String, Any?>): List<Play> {
                val id = columns.getValue("id").toString()
                val type = columns.getValue("type").toString()
                val numbers = columns.getValue("numbers").toString()
                val game = Play(type, "", "")
                game.id = id
                game.numbers = numbers
                plays.add(game)
                return plays
            }
        })
        plays
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        startActivity(intent)
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

}
