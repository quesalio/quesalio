package com.resultados.quesalio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.resultados.quesalio.MainActivity;
import com.resultados.quesalio.R;
import com.resultados.quesalio.extras.Utiles;

public class ExitActivity extends AppCompatActivity {
    Utiles utiles;
    Button lbl5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit);
        utiles = new Utiles(this);
        utiles.setLaunch(true);
        getSupportActionBar().hide();
        lbl5 = findViewById(R.id.lbl5EA);
        lbl5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    /*@Override
    protected void onRestart() {
        super.onRestart();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }*/

    @Override
    public void onBackPressed() {
        //cerrar();
        finish();
    }

    public void cerrar() {
        Intent intent = new Intent(ExitActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }
}
