package com.resultados.quesalio.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.UpdateChannelsMutation
import com.resultados.quesalio.extras.Config
import kotlinx.android.synthetic.main.activity_settings.*
import spencerstudios.com.bungeelib.Bungee

class SettingsActivity : AppCompatActivity() {
    private var channels = mutableSetOf<String>()
    private var sharedPreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbarActivitySettings)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        sharedPreferences = MyApplication.sInstance.getSharedPreferences()
        channels = sharedPreferences?.getStringSet("channels", mutableSetOf())!!
        if (channels.contains(Config.PRIMERA_TYPE)) {
            switchPrimera.isChecked = true
        }
        if (channels.contains(Config.MATUTINA_TYPE)) {
            switchMatutina.isChecked = true
        }
        if (channels.contains(Config.VESPERTINA_TYPE)) {
            switchVespertina.isChecked = true
        }
        if (channels.contains(Config.NOCTURNA_TYPE)) {
            switchNocturna.isChecked = true
        }



        switchPrimera.setOnCheckedChangeListener { _, isChecked ->
            modifySubscriptionFCM(isChecked, Config.PRIMERA_TYPE)
        }
        switchMatutina.setOnCheckedChangeListener { _, isChecked ->
            modifySubscriptionFCM(isChecked, Config.MATUTINA_TYPE)
        }
        switchVespertina.setOnCheckedChangeListener { _, isChecked ->
            modifySubscriptionFCM(isChecked, Config.VESPERTINA_TYPE)
        }
        switchNocturna.setOnCheckedChangeListener { _, isChecked ->
            modifySubscriptionFCM(isChecked, Config.NOCTURNA_TYPE)
        }
    }

    private fun modifySubscription(checked: Boolean, tag: String) {

    }

    private fun modifySubscriptionFCM(checked: Boolean, tag: String) {
        if (checked) {
            FirebaseMessaging.getInstance().subscribeToTopic(tag)
            channels.add(tag)
        } else {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(tag)
            channels.remove(tag)
        }
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val token = instanceIdResult?.token
            val chan = when (channels.size) {
                0 -> ""
                1 -> channels.elementAt(0)
                else -> channels.joinToString(",")
            }
            //Log.e("channels", "$chan")
            MyApplication.sInstance.apolloClient.mutate(
                    UpdateChannelsMutation.builder()
                            .token(token)
                            .channels(chan)
                            .build()).enqueue(object : ApolloCall.Callback<UpdateChannelsMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    //Log.e("failure", "${e.message}")

                }

                override fun onResponse(response: Response<UpdateChannelsMutation.Data>) {
                    response.data()?.update_channels()?.channels()?.let {
                        if (it.size > 0) {
                            val editor = sharedPreferences?.edit()
                            editor?.putStringSet("channels", channels)
                            editor?.apply()
                        }
                    }
                }
            })
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Configuración", "Configuración")
    }
}
