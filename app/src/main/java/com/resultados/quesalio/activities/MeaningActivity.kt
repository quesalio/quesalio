package com.resultados.quesalio.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.fetcher.ApolloResponseFetchers
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetMeaningTypeQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.MeaningTypeAdapter
import com.resultados.quesalio.entities.MeaningType
import com.resultados.quesalio.entities.MeaningTypeDetail
import com.resultados.quesalio.extras.Ads
import kotlinx.android.synthetic.main.activity_meaning.*
import spencerstudios.com.bungeelib.Bungee

class MeaningActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meaning)
        setSupportActionBar(toolbarActivityMeaning)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        containerListMeaning.layoutManager = LinearLayoutManager(this)
        containerListMeaning.setHasFixedSize(true)
        getMeaningType()
    }

    private fun getMeaningType() {
        val meaningType = mutableListOf<MeaningType>()

        MyApplication.sInstance.apolloClient.query(GetMeaningTypeQuery.builder().build()).responseFetcher(ApolloResponseFetchers.CACHE_FIRST).enqueue(object : ApolloCall.Callback<GetMeaningTypeQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetMeaningTypeQuery.Data>) {
                response.data()?.meaning_types()?.let { list ->
                    list.forEach { meaning_type ->
                        val arrayDetails = mutableListOf<MeaningTypeDetail>()
                        val isName = meaning_type.isName != 0
                        if (!isName) //TODO quitar esto para que pueda ser nombre
                            meaningType.add(MeaningType(meaning_type.id().toString(), meaning_type.name().toString(), isName, arrayDetails))
                    }
                    runOnUiThread {
                        containerListMeaning.adapter = MeaningTypeAdapter(meaningType)
                    }
                }
            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsMeaning)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Significados", "Significados")
    }
}
