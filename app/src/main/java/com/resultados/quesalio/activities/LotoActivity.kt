package com.resultados.quesalio.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Button
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetLotoQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils
import com.resultados.quesalio.extras.Utiles
import kotlinx.android.synthetic.main.activity_loto.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import spencerstudios.com.bungeelib.Bungee

class LotoActivity : AppCompatActivity() {
    private lateinit var utiles: Utiles
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loto)
        utiles = Utiles.getInstance(this)
        setSupportActionBar(toolbarActivityLoto)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        getDraw()
        btnMyPlayLoto.setOnClickListener {
            startActivity(intentFor<LotoPlayActivity>().singleTop())
            Bungee.slideLeft(this)
        }
    }

    private fun getDraw() {
        val dialog = indeterminateProgressDialog(message = getString(R.string.lbl_wait_please), title = getString(R.string.lbl_getting_data))
        dialog.show()
        MyApplication.sInstance.apolloClient.query(GetLotoQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetLotoQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                this@LotoActivity.runOnUiThread {
                    if (dialog.isShowing) {
                        try {
                            dialog.dismiss()
                        } catch (ignored: Exception) {
                        }
                    }
                }
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetLotoQuery.Data>) {
                response.data()?.loto()?.get(0)?.let { loto ->
                    this@LotoActivity.runOnUiThread {
                        lblDayLoto.text = Utiles.seleccionarFechaActual(Config.SDF.format(loto.date()))
                        lblDateLoto.text = ListUtils.getDate(Config.SDF.format(loto.date()))
                        obtenerPrimeraTradicional(loto.tradicional_numbers(), loto.tradicional_prizes(), loto.jackpot1_tradicional(), loto.jackpot2_tradicional())
                        obtenerDesquite(loto.desquite_numbers(), loto.desquite_prizes(), loto.jackpot1_desquite(), loto.jackpot2_desquite())
                        obtenerSaleSale(loto.sale_sale_numbers(), loto.sale_sale_prizes(), loto.sale_sale_duplicador(), loto.jackpot1_sale_sale(), loto.jackpot2_sale_sale(), loto.jackpot3_sale_sale(), loto.jackpot4_sale_sale())
                        obtenerPremiosTradicional(loto.tradicional_prizes())
                        loto.prize_next_move()?.let {
                            if (it.isNotEmpty()) {
                                pozoEstimatedLoto.text = Utiles.formatearNumero(it)
                            }
                        }
                        if (dialog.isShowing) {
                            try {
                                dialog.dismiss()
                            } catch (ignored: Exception) {
                            }
                        }
                    }
                }
            }
        })
    }

    private fun obtenerPremiosTradicional(prizes: List<GetLotoQuery.Tradicional_prize>?) {
        prizes?.forEachIndexed { index, prize ->
            when (index) {
                3 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto17.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto18.text = Utiles.formatearNumero(it)
                        }
                    }

                }
                4 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto20.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto21.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                5 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto23.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto24.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                6 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto26.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto27.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                7 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto29.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto30.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                8 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto32.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblLoto33.text = Utiles.formatearNumero(it)
                        }
                    }
                }
            }
        }
    }

    private fun obtenerSaleSale(numbers: MutableList<String>?, prizes: GetLotoQuery.Sale_sale_prizes?, duplicador: String?, jackpot1: String?, jackpot2: String?, jackpot3: String?, jackpot4: String?) {
        numbers?.let {
            val array_numeros_jackpot_salesale_remoto = mutableListOf<String>()
            val array_numeros_sale_sale_remoto = it
            val array_ids_sale_sale_remoto = IntArray(array_numeros_sale_sale_remoto.size)
            array_numeros_sale_sale_remoto.forEachIndexed { index, _ ->
                val name = "btnLoto" + (index + 13)
                val id = resources.getIdentifier(name, "id", getPackageName())
                array_ids_sale_sale_remoto[index] = id
                findViewById<Button>(id)?.text = array_numeros_sale_sale_remoto[index]
            }
            jackpot1?.let { jackpot11 ->
                array_numeros_jackpot_salesale_remoto.add(jackpot11)
            }
            jackpot2?.let { jackpot22 ->
                array_numeros_jackpot_salesale_remoto.add(jackpot22)
            }
            jackpot3?.let { jackpot33 ->
                array_numeros_jackpot_salesale_remoto.add(jackpot33)
            }
            jackpot4?.let { jackpot44 ->
                array_numeros_jackpot_salesale_remoto.add(jackpot44)
            }
            array_numeros_jackpot_salesale_remoto.forEachIndexed { index, jackpot ->
                val name = "btnJackPotSalesaleLoto" + (index + 1)
                val id = resources.getIdentifier(name, "id", packageName)
                //array_ids_jackpot!![index] = id
                findViewById<Button>(id)?.text = jackpot
            }
            prizes?.winners()?.let {
                if (it.isNotEmpty()) {
                    lblLoto14.text = utiles.obtenerNumeroGanadores(it)
                }
            }
            prizes?.prize()?.let {
                if (it.isNotEmpty()) {
                    lblLoto15.text = Utiles.formatearNumero(it)
                }
            }
        }
    }

    private fun obtenerDesquite(numeros: MutableList<String>?, premios: List<GetLotoQuery.Desquite_prize>?, jackpot1_desquite: String?, jackpot2_desquite: String?) {
        numeros?.let {
            val array_numeros_jackpot_desquite_remoto = mutableListOf<String>()
            val array_numeros_desquite_remoto = it
            val array_ids_desquite_remoto = IntArray(array_numeros_desquite_remoto.size)
            array_numeros_desquite_remoto.forEachIndexed { index, desquite ->
                val name = "btnLoto" + (index + 7)
                val id = resources.getIdentifier(name, "id", packageName)
                array_ids_desquite_remoto[index] = id
                findViewById<Button>(id)?.text = desquite
            }
            jackpot1_desquite?.let { jackpot11 ->
                array_numeros_jackpot_desquite_remoto.add(jackpot11)
            }
            jackpot2_desquite?.let { jackpot22 ->
                array_numeros_jackpot_desquite_remoto.add(jackpot22)
            }
            array_numeros_jackpot_desquite_remoto.forEachIndexed { index, jackpot ->
                val name = "btnJackPotDesquiteLoto" + (index + 1)
                val id = resources.getIdentifier(name, "id", packageName)
                //array_ids_jackpot_desquite!![index] = id
                findViewById<Button>(id)?.text = jackpot
            }
            premios?.forEachIndexed { index, prize ->
                when (index) {
                    0 -> {
                        prize.winners()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto11.text = utiles.obtenerNumeroGanadores(it)
                            }
                        }
                        prize.prize()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto12.text = Utiles.formatearNumero(it)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun obtenerPrimeraTradicional(numeros: MutableList<String>?, prizes: List<GetLotoQuery.Tradicional_prize>?, jackpot1_tradicional: String?, jackpot2_tradicional: String?) {
        numeros?.let {
            val array_numeros_jackpot_remoto = mutableListOf<String>()
            val array_numeros_primera_tradicional_remoto = it
            val array_ids_primera_tradicional_remoto = IntArray(array_numeros_primera_tradicional_remoto.size)
            array_numeros_primera_tradicional_remoto.forEachIndexed { index, tradicional ->
                val name = "btnLoto" + (index + 1)
                val id = resources.getIdentifier(name, "id", packageName)
                array_ids_primera_tradicional_remoto[index] = id
                findViewById<Button>(id)?.text = tradicional
            }
            jackpot1_tradicional?.let {
                array_numeros_jackpot_remoto.add(it)
            }
            jackpot2_tradicional?.let {
                array_numeros_jackpot_remoto.add(it)
            }
            val array_ids_jackpot = IntArray(array_numeros_jackpot_remoto.size)
            array_numeros_jackpot_remoto.forEachIndexed { index, jackpot ->
                val name = "btnJackPotLoto" + (index + 1)
                val id = resources.getIdentifier(name, "id", packageName)
                array_ids_jackpot[index] = id
                findViewById<Button>(id)?.text = jackpot
            }
            prizes?.forEachIndexed { index, prize ->
                when (index) {
                    0 -> {
                        prize.winners()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto2.text = utiles.obtenerNumeroGanadores(it)
                            }
                        }
                        prize.prize()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto3.text = Utiles.formatearNumero(it)
                            }
                        }
                    }
                    1 -> {
                        prize.winners()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto5.text = utiles.obtenerNumeroGanadores(it)
                            }
                        }
                        prize.prize()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto6.text = Utiles.formatearNumero(it)
                            }
                        }
                    }
                    2 -> {
                        prize.winners()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto8.text = utiles.obtenerNumeroGanadores(it)
                            }
                        }
                        prize.prize()?.let {
                            if (it.isNotEmpty()) {
                                lblLoto9.text = Utiles.formatearNumero(it)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsLoto)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Loto", "Loto")
    }
}
