package com.resultados.quesalio.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Button
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetQuini6Query
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils
import com.resultados.quesalio.extras.Utiles
import kotlinx.android.synthetic.main.activity_quini.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import spencerstudios.com.bungeelib.Bungee

class QuiniActivity : AppCompatActivity() {
    private lateinit var utiles: Utiles
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quini)
        setSupportActionBar(toolbarActivityQuini)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        utiles = Utiles.getInstance(this)
        getDraw()
        btnMyPlayQuini.setOnClickListener {
            startActivity(intentFor<QuiniPlayActivity>().singleTop())
            Bungee.slideLeft(this)
        }
    }

    private fun getDraw() {
        val dialog = indeterminateProgressDialog(message = getString(R.string.lbl_wait_please), title = getString(R.string.lbl_getting_data))
        dialog.show()
        MyApplication.sInstance.apolloClient.query(GetQuini6Query.builder().build()).enqueue(object : ApolloCall.Callback<GetQuini6Query.Data>() {
            override fun onFailure(e: ApolloException) {
                this@QuiniActivity.runOnUiThread {
                    if (dialog.isShowing) {
                        try {
                            dialog.dismiss()
                        } catch (ignored: Exception) {
                        }
                    }
                }
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetQuini6Query.Data>) {
                response.data()?.quini6()?.get(0)?.let { quini ->
                    this@QuiniActivity.runOnUiThread {
                        lblDayQuini.text = Utiles.seleccionarFechaActual(Config.SDF.format(quini.date()))
                        lblDateQuini.text = ListUtils.getDate(Config.SDF.format(quini.date()))
                        quini.prize_next_move()?.let {
                            if (it.isNotEmpty()) {
                                lblPozoQuini.text = Utiles.formatearNumero(it)
                            }
                        }
                        mostrarPrimerSorteoTradicional(quini.tradicional_numbers(), quini.tradicional_prizes())
                        mostrarSegundaVueltaTradicional(quini.segunda_vuelta_numbers(), quini.segunda_vuelta_prizes())
                        mostrarRevancha(quini.revancha_numbers(), quini.revancha_prizes())
                        mostrarSiempreSale(quini.siempre_sale_numbers(), quini.siempre_sale_prizes())
                        mostrarSuperCanasta(quini.premio_extra(), quini.premio_extra_prizes())
                        if (dialog.isShowing) {
                            try {
                                dialog.dismiss()
                            } catch (ignored: Exception) {
                            }
                        }
                    }
                }
            }

        })
    }

    private fun mostrarSuperCanasta(extra: String?, prize: GetQuini6Query.Premio_extra_prizes?) {
        extra?.let {
            if (it.isNotEmpty()) {
                lblCanastaQuini.text = Utiles.formatearNumero(it)
            }
        }
        prize?.winners()?.let {
            if (it.isNotEmpty()) {
                lblQuini26.text = utiles.obtenerNumeroGanadores(it)
            }
        }
        prize?.prize()?.let {
            if (it.isNotEmpty()) {
                lblQuini27.text = Utiles.formatearNumero(it)
            }
        }
    }

    private fun mostrarSiempreSale(numbers: MutableList<String>?, prizes: GetQuini6Query.Siempre_sale_prizes?) {
        val array_numeros_siempre_sale_remoto = numbers!!
        val array_ids_siempre_sale_remoto = IntArray(array_numeros_siempre_sale_remoto.size)
        array_numeros_siempre_sale_remoto.forEachIndexed { index, ss ->
            val name = "btnQuini" + (index + 19)
            val id = resources.getIdentifier(name, "id", packageName)
            array_ids_siempre_sale_remoto[index] = id
            findViewById<Button>(id)?.text = ss
        }
        prizes?.winners()?.let {
            if (it.isNotEmpty()) {
                lblQuini23.text = utiles.obtenerNumeroGanadores(it)
            }
        }
        prizes?.prize()?.let {
            if (it.isNotEmpty()) {
                lblQuini24.text = Utiles.formatearNumero(it)
            }
        }
    }

    private fun mostrarRevancha(numbers: MutableList<String>?, prizes: GetQuini6Query.Revancha_prizes?) {
        val array_numeros_revancha_remoto = numbers!!
        val array_ids_revancha_remoto = IntArray(array_numeros_revancha_remoto.size)
        array_numeros_revancha_remoto.forEachIndexed { index, revancha ->
            val name = "btnQuini" + (index + 13)
            val id = resources.getIdentifier(name, "id", packageName)
            array_ids_revancha_remoto[index] = id
            findViewById<Button>(id)?.text = revancha
        }
        prizes?.winners()?.let {
            if (it.isNotEmpty()) {
                lblQuini20.text = utiles.obtenerNumeroGanadores(it)
            }
        }
        prizes?.prize()?.let {
            if (it.isNotEmpty()) {
                lblQuini21.text = Utiles.formatearNumero(it)
            }
        }
    }

    private fun mostrarSegundaVueltaTradicional(segunda_vuelta_numbers: MutableList<String>?, segunda_vuelta_prizes: MutableList<GetQuini6Query.Segunda_vuelta_prize>?) {
        val array_numeros_segunda_vuelta_remoto = segunda_vuelta_numbers!!
        val array_ids_segunda_vuelta_tradicional_remoto = IntArray(array_numeros_segunda_vuelta_remoto.size)
        array_numeros_segunda_vuelta_remoto.forEachIndexed { index, segunda ->
            val name = "btnQuini" + (index + 7)
            val id = resources.getIdentifier(name, "id", packageName)
            array_ids_segunda_vuelta_tradicional_remoto[index] = id
            findViewById<Button>(id)?.text = segunda
        }
        segunda_vuelta_prizes?.forEach { prize ->
            when (prize.hits()?.toInt()) {
                4 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini11.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini12.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                5 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini14.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini15.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                6 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini17.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini18.text = Utiles.formatearNumero(it)
                        }
                    }
                }
            }
        }
    }

    private fun mostrarPrimerSorteoTradicional(tradicional_numbers: MutableList<String>?, tradicional_prizes: MutableList<GetQuini6Query.Tradicional_prize>?) {
        val array_numeros_primera_tradicional_remoto = tradicional_numbers!!
        val array_ids_primera_tradicional_remoto = IntArray(array_numeros_primera_tradicional_remoto.size)
        array_numeros_primera_tradicional_remoto.forEachIndexed { index, primera ->
            val name = "btnQuini" + (index + 1)
            val id = resources.getIdentifier(name, "id", packageName)
            array_ids_primera_tradicional_remoto[index] = id
            findViewById<Button>(id)?.text = primera
        }
        tradicional_prizes?.forEachIndexed { _, prize ->
            when (prize.hits()?.toInt()) {
                4 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini2.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini3.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                5 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini5.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini6.text = Utiles.formatearNumero(it)
                        }
                    }
                }
                6 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini8.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblQuini9.text = Utiles.formatearNumero(it)
                        }
                    }
                }
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsQuini)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Quini", "Quini")
    }
}
