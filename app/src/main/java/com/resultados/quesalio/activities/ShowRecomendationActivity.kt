package com.resultados.quesalio.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetRecomendationsQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.RecomendationAdapter
import com.resultados.quesalio.entities.Recomendacion
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils
import kotlinx.android.synthetic.main.activity_detail_quiniela.*
import kotlinx.android.synthetic.main.activity_show_recomendation.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import spencerstudios.com.bungeelib.Bungee
import java.util.*

class ShowRecomendationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_recomendation)
        setSupportActionBar(toolbarActivityShowRecomendation)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        btnShowRecomendation.setOnClickListener {
            val intent = Intent(this@ShowRecomendationActivity, RecomendationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this@ShowRecomendationActivity)
            finish()
        }
        val bsAsTimeZone = DateTimeZone.forID("America/Buenos_Aires")
        val date = DateTime.now(bsAsTimeZone).toDate()
        lblDateShowRecomendation.text = ListUtils.getDate(Config.SDF.format(date))
        getRecomendations()
    }

    private fun getRecomendations() {
        val recomendationList = mutableListOf<Recomendacion>()
        MyApplication.sInstance.apolloClient.query(GetRecomendationsQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetRecomendationsQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetRecomendationsQuery.Data>) {
                response.data()?.recomendation()?.let { recomendations ->
                    runOnUiThread {
                        recomendationList.clear()
                        recomendations.forEach { recomendation ->
                            val calendar = Calendar.getInstance();
                            //calendar.timeInMillis = recomendation.date().toString().toLong()
                            val rec = Recomendacion()
                            rec.nombre = recomendation.name()
                            rec.imagen = recomendation.image()
                            //rec.fecha = calendar.time
                            rec.recomendacion = recomendation.recomendation()
                            recomendationList.add(rec)
                        }
                        val adapter = RecomendationAdapter(recomendationList, this@ShowRecomendationActivity)
                        containerShowRecomendations.setHasFixedSize(true)
                        containerShowRecomendations.layoutManager = GridLayoutManager(this@ShowRecomendationActivity, 2)
                        containerShowRecomendations.adapter = adapter
                    }
                }
            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsShowRecomendation)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Mostrar Recomendaciones", "Mostrar Recomendaciones")
    }
}
