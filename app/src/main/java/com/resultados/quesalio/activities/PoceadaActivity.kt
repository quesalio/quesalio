package com.resultados.quesalio.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Button
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetPoceadaQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils
import com.resultados.quesalio.extras.Utiles
import kotlinx.android.synthetic.main.activity_poceada.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import spencerstudios.com.bungeelib.Bungee

class PoceadaActivity : AppCompatActivity() {
    private lateinit var utiles: Utiles
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_poceada)
        utiles = Utiles.getInstance(this)
        setSupportActionBar(toolbarActivityPoceada)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        getDraw()
        btnMyPlayPoceada.setOnClickListener {
            startActivity(intentFor<PoceadaPlayActivity>().singleTop())
            Bungee.slideLeft(this)
        }
    }

    private fun getDraw() {
        val dialog = indeterminateProgressDialog(message = getString(R.string.lbl_wait_please), title = getString(R.string.lbl_getting_data))
        dialog.show()
        MyApplication.sInstance.apolloClient.query(GetPoceadaQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetPoceadaQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                this@PoceadaActivity.runOnUiThread {
                    if (dialog.isShowing) {
                        try {
                            dialog.dismiss()
                        } catch (ignored: Exception) {
                        }
                    }
                }
            }

            override fun onResponse(response: com.apollographql.apollo.api.Response<GetPoceadaQuery.Data>) {
                response.data()?.poceada()?.get(0)?.let { poceada ->
                    this@PoceadaActivity.runOnUiThread {
                        lblDayPoceada.text = Utiles.seleccionarFechaActual(Config.SDF.format(poceada.date()))
                        lblDatePoceada.text = ListUtils.getDate(Config.SDF.format(poceada.date()))
                        poceada.prizes()?.let {
                            getPrizes(poceada.prizes())
                        }
                        poceada.numbers()?.let {
                            getNumbers(poceada.numbers())
                        }
                        if (dialog.isShowing) {
                            try {
                                dialog.dismiss()
                            } catch (ignored: Exception) {
                            }
                        }
                    }
                }
            }
        })
    }

    private fun getNumbers(numbers: List<String>?) {
        numbers?.let {
            it.forEachIndexed { index, number ->
                val name = "btnPoceada" + (index + 1)
                val id = resources.getIdentifier(name, "id", packageName)
                findViewById<Button>(id)?.text = number
            }
        }
    }

    private fun getPrizes(prizes: List<GetPoceadaQuery.Prize>?) {
        prizes?.forEach { prize ->

            when (prize.hits()?.toInt()) {
                6 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblPoceada2.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    val prizeString = "${prize.prize()}"
                    lblPoceada3?.text = Utiles.formatearNumero(prizeString)
                }
                7 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblPoceada5.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    val prizeString = "${prize.prize()}"
                    lblPoceada6?.text = Utiles.formatearNumero(prizeString)
                }
                8 -> {
                    prize.winners()?.let {
                        if (it.isNotEmpty()) {
                            lblPoceada8?.text = utiles.obtenerNumeroGanadores(it)
                        }
                    }
                    prize.prize()?.let {
                        if (it.isNotEmpty()) {
                            lblPoceada9?.text = Utiles.formatearNumero(it)
                        }
                    }
                    if (prize.winners().equals("0")) {
                        prize.prize()?.let {
                            if (it.isNotEmpty()) {
                                lblPozoPoceada.text = Utiles.formatearNumero(it)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsPoceada)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Poceada", "Poceada")
    }
}
