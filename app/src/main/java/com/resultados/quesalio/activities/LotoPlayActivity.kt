package com.resultados.quesalio.activities

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.GamePlayAdapter
import com.resultados.quesalio.entities.GameItem
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.database
import kotlinx.android.synthetic.main.activity_loto_play.*
import org.jetbrains.anko.toast
import spencerstudios.com.bungeelib.Bungee

class LotoPlayActivity : AppCompatActivity() {
    val numberList = mutableListOf<GameItem>()
    val numberListJack = mutableListOf<GameItem>()
    var finalList = mutableListOf<String>()
    var finalListJack = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loto_play)
        setSupportActionBar(toolbarActivityLotoPlay)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        containerListLotoPlay.layoutManager = GridLayoutManager(this, 10)
        containerListLotoPlay.setHasFixedSize(true)
        for (i in 0..41) {
            val value: String = if (i < 10) "0$i" else "$i"
            val item = GameItem(value)
            item.type = Config.LOTO_TYPE
            numberList.add(item)
        }
        val adapter = GamePlayAdapter(numberList)
        containerListLotoPlay.adapter = adapter
        containerListLotoJackPlay.layoutManager = GridLayoutManager(this, 10)
        containerListLotoJackPlay.setHasFixedSize(true)
        for (i in 0..9) {
            val item = GameItem(i.toString())
            item.type = Config.LOTO_TYPE_JACK
            numberListJack.add(item)
        }
        val adapterJack = GamePlayAdapter(numberListJack)
        containerListLotoJackPlay.adapter = adapterJack
        btnContinueLotoPlay.setOnClickListener {
            finalList = adapter.finalList
            finalListJack = adapterJack.finalList
            if (finalList.size < 6) {
                toast(getString(R.string.lblSelectSixNumbers))
                return@setOnClickListener
            }
            if (finalListJack.size < 2) {
                toast(getString(R.string.lblSelectTwoJacksNumbers))
                return@setOnClickListener
            }
            database.use {
                val numbers = finalList.joinToString(",") + "|" + finalListJack.joinToString(",")
                val values = ContentValues()
                values.put("type", Config.LOTO_TYPE)
                values.put("numbers", numbers)
                this.insert(Config.PLAYS_TABLE, null, values)
            }
            val intent = Intent(this, PlayCompletedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            finish()
        }
        imageAzarLoto.setOnClickListener {
            val randomList = mutableListOf<GameItem>()
            val randomListJack = mutableListOf<GameItem>()
            do {
                val randomInteger = (0..41).shuffled().first()
                val value: String = if (randomInteger < 10) "0$randomInteger" else "$randomInteger"
                val item = GameItem(value)
                item.type = Config.LOTO_TYPE
                if (!randomList.contains(item)) {
                    randomList.add(item)
                }
            } while (randomList.size < 6)
            do {
                val randomInteger = (0..9).shuffled().first()
                val value = "$randomInteger"
                val item = GameItem(value)
                item.type = Config.LOTO_TYPE_JACK
                if (!randomListJack.contains(item)) {
                    randomListJack.add(item)
                }
            } while (randomListJack.size < 2)
            adapter.setRandomValues(randomList)
            adapterJack.setRandomValues(randomListJack)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsLotoPlay)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Jugada Loto", "Jugada Loto")
    }
}