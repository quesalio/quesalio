package com.resultados.quesalio.activities

import android.content.ContentValues
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.GamePlayAdapter
import com.resultados.quesalio.entities.GameItem
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.database
import kotlinx.android.synthetic.main.activity_poceada_play.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import org.jetbrains.anko.toast
import spencerstudios.com.bungeelib.Bungee

class PoceadaPlayActivity : AppCompatActivity() {
    val numberList = mutableListOf<GameItem>()
    var finalList = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_poceada_play)
        setSupportActionBar(toolbarActivityPoceadaPlay)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        containerListPoceadaPlay.layoutManager = GridLayoutManager(this, 10)
        containerListPoceadaPlay.setHasFixedSize(true)
        for (i in 0..99) {
            val value: String = if (i < 10) "0$i" else "$i"
            val item = GameItem(value)
            item.type = Config.POCEADA_TYPE
            numberList.add(item)
        }
        val adapter = GamePlayAdapter(numberList)
        containerListPoceadaPlay.adapter = adapter
        btnContinuePoceadaPlay.setOnClickListener {
            finalList = adapter.finalList
            if (finalList.size < 8) {
                toast(getString(R.string.lblSelectEightNumbers))
                return@setOnClickListener
            }
            database.use {
                val numbers = finalList.joinToString(",")
                val values = ContentValues()
                values.put("type", Config.POCEADA_TYPE)
                values.put("numbers", numbers)
                this.insert(Config.PLAYS_TABLE, null, values)
            }
            startActivity(intentFor<PlayCompletedActivity>().singleTop())
            finish()
        }
        imageAzarPoceada.setOnClickListener {
            val randomList = mutableListOf<GameItem>()
            do {
                val randomInteger = (0..99).shuffled().first()
                val value: String = if (randomInteger < 10) "0$randomInteger" else "$randomInteger"
                val item = GameItem(value)
                item.type = Config.POCEADA_TYPE
                if (!randomList.contains(item)) {
                    randomList.add(item)
                }
            } while (randomList.size < 8)
            adapter.setRandomValues(randomList)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsPoceadaPlay)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Jugada Poceada", "Jugada Poceada")
    }
}