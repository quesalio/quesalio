package com.resultados.quesalio.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Button
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetBrincoQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ListUtils
import com.resultados.quesalio.extras.Utiles
import kotlinx.android.synthetic.main.activity_brinco.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import spencerstudios.com.bungeelib.Bungee

class BrincoActivity : AppCompatActivity() {
    private lateinit var utiles: Utiles
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brinco)
        utiles = Utiles.getInstance(this)
        setSupportActionBar(toolbarActivityBrinco)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        getDraw()
        btnMyPlayBrinco.setOnClickListener {
            startActivity(intentFor<BrincoPlayActivity>().singleTop())
            Bungee.slideLeft(this)
        }
    }

    private fun getDraw() {
        val dialog = indeterminateProgressDialog(message = getString(R.string.lbl_wait_please), title = getString(R.string.lbl_getting_data))
        dialog.show()
        MyApplication.sInstance.apolloClient.query(GetBrincoQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetBrincoQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                if (dialog.isShowing) {
                    try {
                        dialog.dismiss()
                    } catch (ignored: Exception) {
                    }
                }
            }

            override fun onResponse(response: Response<GetBrincoQuery.Data>) {
                response.data()?.brinco()?.get(0)?.let {
                    this@BrincoActivity.runOnUiThread {
                        lblDayBrinco.text = Utiles.seleccionarFechaActual(Config.SDF.format(it.date()))
                        lblDateBrinco.text = ListUtils.getDate(Config.SDF.format(it.date()))


                        it.numbers()?.forEachIndexed { index, number ->
                            val name = "btnBrinco" + (index + 1)
                            val id = resources.getIdentifier(name, "id", packageName)
                            findViewById<Button>(id)?.text = number
                        }

                        it.prizes()?.forEachIndexed { _, prize ->
                            val aciertos = prize.hits()
                            var ganadores = "${prize.winners()}"
                            if (ganadores.equals("VACANTE")) {
                                ganadores = "0"
                            }
                            val premio = "$" + prize.prize()
                            if (aciertos.equals("3")) {
                                lblBrinco2?.text = utiles.obtenerNumeroGanadores(ganadores)
                                lblBrinco3?.text = premio
                            }
                            if (aciertos.equals("4")) {
                                lblBrinco5?.text = utiles.obtenerNumeroGanadores(ganadores)
                                lblBrinco6?.text = premio
                            }
                            if (aciertos.equals("5")) {
                                lblBrinco8?.text = utiles.obtenerNumeroGanadores(ganadores)
                                lblBrinco9?.text = premio
                            }
                            if (aciertos.equals("6")) {
                                lblBrinco11?.text = utiles.obtenerNumeroGanadores(ganadores)
                                lblBrinco12?.text = premio
                            }
                        }
                        if (dialog.isShowing) {
                            try {
                                dialog.dismiss()
                            } catch (ignored: Exception) {
                            }
                        }
                    }

                }
            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsBrinco)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Brinco", "Brinco")
    }
}
