package com.resultados.quesalio.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.fetcher.ApolloResponseFetchers
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetMeaningTypeDetailQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.MeaningAdapter
import com.resultados.quesalio.entities.MeaningTypeDetail
import com.resultados.quesalio.extras.Ads
import kotlinx.android.synthetic.main.activity_detail_meaning.*
import spencerstudios.com.bungeelib.Bungee

class DetailMeaningActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_meaning)
        setSupportActionBar(toolbarActivityDetailMeaning)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val bundle = intent.extras
        bundle?.let { b ->
            containerListDetailMeaning.layoutManager = GridLayoutManager(this, 2)
            containerListDetailMeaning.setHasFixedSize(true)
            txtTitleActivityDetailMeaning.text = b.getString("name")
            getMeaning(b.getString("id"))
        }
    }

    private fun getMeaning(id: String?) {
        val arrayMeanings = arrayListOf<MeaningTypeDetail>()
        MyApplication.sInstance.apolloClient.query(GetMeaningTypeDetailQuery.builder().id(id!!).build()).responseFetcher(ApolloResponseFetchers.CACHE_FIRST).enqueue(object : ApolloCall.Callback<GetMeaningTypeDetailQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetMeaningTypeDetailQuery.Data>) {
                response.data()?.meaning_type()?.get(0)?.let { meaning_type ->
                    meaning_type.details()?.forEach { detail ->
                        arrayMeanings.add(MeaningTypeDetail(detail.image().toString(), detail.number().toString(), detail.meaning().toString()))
                    }
                    runOnUiThread {
                        containerListDetailMeaning.adapter = MeaningAdapter(arrayMeanings)
                    }
                }
            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        Ads.showBanner(this, adsDetailMeaning)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Detalle Significado", "Detalle Significado")
    }
}
