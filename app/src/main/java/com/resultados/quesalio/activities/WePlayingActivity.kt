package com.resultados.quesalio.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.resultados.quesalio.GetDifferentDrawsQuery
import com.resultados.quesalio.GetScheduleDrawQuery
import com.resultados.quesalio.MyApplication
import com.resultados.quesalio.R
import com.resultados.quesalio.adapters.GamesAdapter
import com.resultados.quesalio.entities.Game
import com.resultados.quesalio.extras.Ads
import com.resultados.quesalio.extras.Config
import kotlinx.android.synthetic.main.activity_we_playing.*
import spencerstudios.com.bungeelib.Bungee
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

class WePlayingActivity : AppCompatActivity() {
    private lateinit var calendar: Calendar
    private var daySelected: Int = 0
    private lateinit var gamesAdapter: GamesAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_we_playing)
        setSupportActionBar(toolbarActivityWePlay)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        containerListWePlaying.layoutManager = LinearLayoutManager(applicationContext)
        containerListWePlaying.setHasFixedSize(true)
        gamesAdapter = GamesAdapter(mutableListOf<Game>())
        getWePlaying()
    }

    fun getWePlaying() {
        val wePlayingList = mutableListOf<Game>()
        MyApplication.sInstance.apolloClient.query(GetDifferentDrawsQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetDifferentDrawsQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetDifferentDrawsQuery.Data>) {
                response.data()?.let { draws ->
                    draws.quini6()?.get(0)?.let { quini ->
                        val game = Game(Config.QUINI_TYPE, quini.prize_next_move().toString(), "", false)
                        quini.tradicional_prizes()?.forEach { prize ->
                            if (prize.hits()!!.toInt() == 6 && prize.winners()!!.toInt() > 0) {
                                game.winners = prize.winners().toString()
                            }
                        }
                        wePlayingList.add(game)
                    }
                    draws.loto()?.get(0)?.let { loto ->
                        val game = Game(Config.LOTO_TYPE, loto.prize_next_move().toString(), "", false)
                        loto.tradicional_prizes()?.forEach { prize ->
                            if (prize.label().equals("6 aciertos + 2 Jackpot") && prize.winners()!!.toInt() > 0) {
                                game.winners = prize.winners().toString()
                            }
                        }
                        wePlayingList.add(game)
                    }
                    draws.poceada()?.get(0)?.let { poceada ->
                        var nextPrize = ""
                        var isTaken = false
                        var winners = ""
                        poceada.prizes()?.forEach { prize ->
                            if (prize.hits()!!.toInt() == 8) {
                                nextPrize = prize.prize().toString()
                                isTaken = !(prize.hits()!!.toInt() == 8 && prize.winners()!!.toInt() == 0)
                                winners = prize.winners().toString()
                            }
                        }
                        val game = Game(Config.POCEADA_TYPE, nextPrize, "", isTaken)
                        game.winners = winners
                        wePlayingList.add(game)
                    }
                    draws.poceada_plus()?.get(0)?.let { poceada_plus ->
                        var nextPrize = ""
                        var isTaken = false
                        var winners = ""
                        poceada_plus.prizes()?.forEach { prize ->
                            if (prize.hits()!!.toInt() == 8) {
                                nextPrize = prize.prize().toString()
                                isTaken = !(prize.hits()!!.toInt() == 8 && prize.winners()!!.toInt() == 0)
                                winners = prize.winners().toString()
                            }
                        }
                        val game = Game(Config.POCEADA_PLUS_TYPE, nextPrize, "", isTaken)
                        game.winners = winners
                        wePlayingList.add(game)
                    }
                    draws.brinco()?.get(0)?.let { brinco ->
                        val game = Game(Config.BRINCO_TYPE, brinco.next_prize().toString(), "", false)
                        brinco.prizes()?.forEach { prize ->
                            if (prize.hits()!!.toInt() == 6 && prize.winners()!!.toInt() > 0) {
                                game.winners = prize.winners().toString()
                            }
                        }
                        wePlayingList.add(game)
                    }
                    runOnUiThread {
                        gamesAdapter = GamesAdapter(wePlayingList)
                        containerListWePlaying.adapter = gamesAdapter
                        getDatesPlays()
                    }
                }
            }

        })
    }

    private fun getDatesPlays() {
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.POCEADA_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            gamesAdapter.addDateGamePoceada("Sortea ${convertDayToDate(schedule.day().toString())} ${schedule.hour().toString()}Hs")
                        }
                    }
                }
            }

        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.POCEADA_PLUS_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            gamesAdapter.addDateGamePoceadaPlus("Sortea ${convertDayToDate(schedule.day().toString())} ${schedule.hour().toString()}Hs")
                        }
                    }
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.LOTO_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            gamesAdapter.addDateGameLoto("Sortea ${convertDayToDate(schedule.day().toString())} ${schedule.hour().toString()}Hs")
                        }
                    }
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.BRINCO_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            gamesAdapter.addDateGameBrinco("Sortea ${convertDayToDate(schedule.day().toString())} ${schedule.hour().toString()}Hs")
                        }
                    }
                }
            }
        })
        MyApplication.sInstance.apolloClient.query(GetScheduleDrawQuery.builder().draw(Config.QUINI_TYPE).day(daySelected).build()).enqueue(object : ApolloCall.Callback<GetScheduleDrawQuery.Data>() {
            override fun onFailure(e: ApolloException) {

            }

            override fun onResponse(response: Response<GetScheduleDrawQuery.Data>) {
                if (response.data()?.schedule_draw()?.size!! > 0) {
                    response.data()?.schedule_draw()?.get(0)?.let { schedule ->
                        runOnUiThread {
                            gamesAdapter.addDateGameQuini("Sortea ${convertDayToDate(schedule.day().toString())} ${schedule.hour().toString()}Hs")
                        }
                    }
                }
            }

        })
    }

    private fun convertDayToDate(day: String): String {
        val calendar = Calendar.getInstance()
        var daySelected = day.toInt()
        if (day.equals("8")) {
            daySelected = 1
        }
        calendar.set(Calendar.DAY_OF_WEEK, daySelected)
        val dateFormat = SimpleDateFormat("dd/MM")
        dateFormat.timeZone = calendar.getTimeZone();
        val calendarToday = Calendar.getInstance()
        val milis1 = calendar.getTimeInMillis()
        val milis2 = calendarToday.getTimeInMillis()
        val diff = milis1 - milis2
        val diffDays = diff / (24 * 60 * 60 * 1000)
        val diferencia = BigDecimal(diffDays).intValueExact()
        var ddday = ""
        when (diferencia) {
            1 -> ddday = "Mañana"
            0 -> ddday = "Hoy"
        }
        if (ddday.isEmpty()) {
            ddday = when (daySelected) {
                1 -> "Domingo"
                2 -> "Lunes"
                3 -> "Martes"
                4 -> "Miércoles"
                5 -> "Jueves"
                6 -> "Viernes"
                7 -> "Sábado"
                else -> ""
            }
        }
        return "$ddday ${dateFormat.format(calendar.getTime())}"
    }

    override fun onStart() {
        super.onStart()
        calendar = Calendar.getInstance()
        daySelected = calendar.get(Calendar.DAY_OF_WEEK)
        Ads.showBanner(this, adsWePlaying)
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Qué Jugamos", "Qué Jugamos")
    }

    override fun onBackPressed() {
        val intent = Intent(this, PlaysActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        startActivity(intent)
        //super.onBackPressed()
        Bungee.slideRight(this)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                finish()
            }
        }
        return true
    }

}
