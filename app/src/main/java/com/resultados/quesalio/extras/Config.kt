package com.resultados.quesalio.extras

import java.text.SimpleDateFormat

object Config {
    val APP_ID = "5f696885085549b0d7d10853daeeb41a"
    val CLIENT_KEY = "4ed2eabb5859baab76b1751a45ee2a65"
    val SERVER_URL = "http://51.68.133.227:1337/parse"
    val GRAPHQL_URL = "https://production.quesalio.com/graphql"
    val GRAPHQL_SUBSCRIPTION_URL = "ws://production.quesalio.com:443/graphql"
    val EPHEMERIDES_IMAGE_URL = "https://api.quesalio.com/"

    val PLAYS_TABLE = "MyPlays"
    val CITIES_TABLE = "Cities"
    val CITY_IDS_TABLE = "CityIDs"
    val PRIMERA_TYPE = "primera"
    val MATUTINA_TYPE = "matutina"
    val VESPERTINA_TYPE = "vespertina"
    val NOCTURNA_TYPE = "nocturna"
    val PRIMERA_HOUR = "11:30Hs"
    val MATUTINA_HOUR = "14:00Hs"
    val VESPERTINA_HOUR = "17:30Hs"
    val NOCTURNA_HOUR = "21:00Hs"

    val BRINCO_TYPE = "brinco"
    val POCEADA_TYPE = "poceada"
    val POCEADA_PLUS_TYPE = "poceada_plus"
    val LOTO_TYPE = "loto"
    val LOTO_TYPE_JACK = "quini_jack"
    val QUINI_TYPE = "quini"
    val SDF = SimpleDateFormat("yyyy-MM-dd")
    val SDF_HOUR = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val SDF_RECOMENDATION = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    val SDFLetter = SimpleDateFormat("dd 'de' MMMM")
}
