package com.resultados.quesalio.extras

import android.support.v7.util.DiffUtil
import android.util.Log
import com.resultados.quesalio.entities.Draw

class DrawDiffCallback : DiffUtil.ItemCallback<Draw>() {
    override fun areItemsTheSame(oldItem: Draw, newItem: Draw): Boolean {
        Log.e("old & new", "${oldItem.id} - ${newItem.id}")
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Draw, newItem: Draw): Boolean {
        /*return oldItem.primeraNumbers!!.equals(newItem.primeraNumbers) &&
                oldItem.matutinaNumbers!!.equals(newItem.matutinaNumbers) &&
                oldItem.vespertinaNumbers!!.equals(newItem.vespertinaNumbers) &&
                oldItem.nocturnaNumbers!!.equals(newItem.nocturnaNumbers) &&
                oldItem.value.equals(newItem.value)*/


        return oldItem.id == newItem.id &&
                oldItem.primeraNumbers == newItem.primeraNumbers &&
                oldItem.matutinaNumbers == newItem.matutinaNumbers &&
                oldItem.vespertinaNumbers == newItem.vespertinaNumbers &&
                oldItem.nocturnaNumbers == newItem.nocturnaNumbers &&
                oldItem.value == newItem.value


        //return oldItem.value == newItem.value
    }
}