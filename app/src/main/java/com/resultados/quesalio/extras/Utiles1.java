package com.resultados.quesalio.extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.preference.PreferenceManager;

import com.squareup.picasso.Transformation;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;


public class Utiles1 {
    private static Utiles1 mInstance = null;
    private Context context;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private static final String PROPERTY_APP_VERSION = "1";

    public static Utiles1 getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Utiles1(context);
        }
        return mInstance;
    }

    public void setLaunch(boolean launched) {
        editor.putBoolean("launched", launched);
        editor.apply();
    }

    public boolean getLaunch() {
        return prefs.getBoolean("launched", false);
    }

    public Utiles1(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();
    }

    public void guardarFechaActual(String fecha) {
        editor.putString("fecha_actual", fecha);
        editor.apply();
    }

    public String obtenerFechaActual() {
        return prefs.getString("fecha_actual", "");
    }

    public void guardarCiudad(int posicion) {
        editor.putInt("posicion", posicion);
        editor.apply();
    }

    public int obtenerCiudad() {
        return prefs.getInt("posicion", 0);
    }


    public String obtenerNumeroGanadores(String ganadores) {
        if (ganadores.equals("1")) {
            return "UN GANADOR";
        } else if (ganadores.equals("0")) {
            return "POZO VACANTE";
        }
        return ganadores + " GANADORES";

    }

    public static String formatearNumero(String numero) throws Exception {
        numero = String.format("$%,.2f", Double.parseDouble(numero));
        numero = numero.replace(".", "-");
        numero = numero.replace(",", ".");
        numero = numero.replace("-", ",");
        return numero;
    }

    public static String seleccionarFechaActual(String fechaElegida) {
        String dias = "";
        try {
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(new Date());
            Calendar cal2 = Calendar.getInstance();
            String[] fe = fechaElegida.split("-");
            int anio, mes, dia;
            anio = Integer.parseInt(fe[0]);
            mes = Integer.parseInt(fe[1]) - 1;
            dia = Integer.parseInt(fe[2]);
            cal2.set(anio, mes, dia);
            long milis1 = cal1.getTimeInMillis();
            long milis2 = cal2.getTimeInMillis();
            long diff = milis1 - milis2;
            long diffDays = (diff / (24 * 60 * 60 * 1000));
            int diferencia = new BigDecimal(diffDays).intValueExact();
            switch (diferencia) {
                case 0:
                    dias = "HOY";
                    break;
                case 1:
                    dias = "AYER";
                    break;
                case 2:
                    dias = "ANTES DE AYER";
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    dias = "HACE " + diferencia + " DIAS";
                    break;
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                    dias = "HACE 1 SEMANA";
                    break;
                case 14:
                case 15:
                    dias = "HACE 2 SEMANAS";
                    break;
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                    dias = "HACE 3 SEMANAS";
                    break;
                case 31:
                    dias = "HACE 1 MES";
                    break;
                default:
                    break;
            }
        } catch (Exception ignored) {
        }
        return dias;
    }

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

}
