package com.resultados.quesalio.extras

import com.resultados.quesalio.*
import java.math.BigDecimal
import java.util.*

object ListUtils {
    fun getDates(numeroDias: Int): List<String> {
        val results = ArrayList<String>()
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_MONTH, numeroDias)
        val currentDate = selectCurrentDate(calendar)

        val day = calendar.get(Calendar.DAY_OF_WEEK)
        val dayday = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)

        var dia = ""
        when (day) {
            Calendar.SUNDAY -> dia = "Domingo"
            Calendar.MONDAY -> dia = "Lunes"
            Calendar.TUESDAY -> dia = "Martes"
            Calendar.WEDNESDAY -> dia = "Miercoles"
            Calendar.THURSDAY -> dia = "Jueves"
            Calendar.FRIDAY -> dia = "Viernes"
            Calendar.SATURDAY -> dia = "Sabado"
            else -> {
            }
        }

        /*val mes = when (month) {
            Calendar.JANUARY -> "Enero"
            Calendar.FEBRUARY -> "Febrero"
            Calendar.MARCH -> "Marzo"
            Calendar.APRIL -> "Abril"
            Calendar.MAY -> "Mayo"
            Calendar.JUNE -> "Junio"
            Calendar.JULY -> "Julio"
            Calendar.AUGUST -> "Agosto"
            Calendar.SEPTEMBER -> "Setiembre"
            Calendar.OCTOBER -> "Octubre"
            Calendar.NOVEMBER -> "Noviembre"
            Calendar.DECEMBER -> "Diciembre"
            else -> {
            }
        }*/
        //resultados.add("$dia, $dayday de $mes")
        results.add("$currentDate $dia")
        results.add(calendar.get(Calendar.YEAR).toString() + "-" + String.format("%02d", month + 1) + "-" + dayday)
        return results
    }

    fun getDate(fecha: String): String {
        val calendar = Calendar.getInstance()
        val fe = fecha.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val aanio: Int
        val mmes: Int
        val ddia: Int
        aanio = Integer.parseInt(fe[0])
        mmes = Integer.parseInt(fe[1]) - 1
        ddia = Integer.parseInt(fe[2])
        calendar.set(aanio, mmes, ddia)

        val day = calendar.get(Calendar.DAY_OF_WEEK)
        val dayday = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)

        var dia = ""
        when (day) {
            Calendar.SUNDAY -> dia = "Domingo"
            Calendar.MONDAY -> dia = "Lunes"
            Calendar.TUESDAY -> dia = "Martes"
            Calendar.WEDNESDAY -> dia = "Miercoles"
            Calendar.THURSDAY -> dia = "Jueves"
            Calendar.FRIDAY -> dia = "Viernes"
            Calendar.SATURDAY -> dia = "Sabado"
            else -> {
            }
        }
        var mes = ""
        when (month) {
            Calendar.JANUARY -> mes = "Enero"
            Calendar.FEBRUARY -> mes = "Febrero"
            Calendar.MARCH -> mes = "Marzo"
            Calendar.APRIL -> mes = "Abril"
            Calendar.MAY -> mes = "Mayo"
            Calendar.JUNE -> mes = "Junio"
            Calendar.JULY -> mes = "Julio"
            Calendar.AUGUST -> mes = "Agosto"
            Calendar.SEPTEMBER -> mes = "Setiembre"
            Calendar.OCTOBER -> mes = "Octubre"
            Calendar.NOVEMBER -> mes = "Noviembre"
            Calendar.DECEMBER -> mes = "Diciembre"
        }
        return "$dia, $dayday de $mes"
        //resultados.add(calendar.get(Calendar.YEAR) + "-" + String.format("%02d", (month + 1)) + "-" + dayday);
        //return resultados;
    }

    fun selectCurrentDate(calendar: Calendar): String {
        val calendarNow = Calendar.getInstance()
        calendarNow.time = Date()
        val milis1 = calendarNow.timeInMillis
        val milis2 = calendar.timeInMillis
        val diff = milis1 - milis2
        val diffDays = diff / (24 * 60 * 60 * 1000)
        val diferencia = BigDecimal(diffDays).intValueExact()
        return when (diferencia) {
            0 -> "Hoy"
            1 -> "Ayer"
            2 -> "Antes de Ayer"
            3, 4, 5, 6 -> "Hace $diferencia Dias"
            7, 8, 9, 10, 11, 12, 13 -> "Hace 1 Semana"
            14, 15 -> "Hace 2 Semanas"
            else -> ""
        }
    }

    fun getDateMyPlays(calendar: Calendar): String {
        val dayString = selectCurrentDate(calendar)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        var dday = day.toString()
        if (day < 10) dday = "0$day"
        var mmonth = month.toString()
        if (month < 10) mmonth = "0$month"
        var dia = ""
        when (day) {
            Calendar.SUNDAY -> dia = "Domingo"
            Calendar.MONDAY -> dia = "Lunes"
            Calendar.TUESDAY -> dia = "Martes"
            Calendar.WEDNESDAY -> dia = "Miercoles"
            Calendar.THURSDAY -> dia = "Jueves"
            Calendar.FRIDAY -> dia = "Viernes"
            Calendar.SATURDAY -> dia = "Sabado"
            else -> {
            }
        }
        return "$dayString $dia $dday/$mmonth".trim()
    }


    fun sortPrimeras(primeras: List<GetAllDrawsQuery.Primera>, array_ids: List<String>): MutableList<GetAllDrawsQuery.Primera> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetAllDrawsQuery.Primera> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            primeras.forEachIndexed { index, primera ->

                if (primera.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(primeras.get(id))
        }
        return list
    }

    fun sortPrimerasSingle(primeras: List<GetPrimeraQuery.Primera>, array_ids: List<String>): MutableList<GetPrimeraQuery.Primera> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetPrimeraQuery.Primera> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            primeras.forEachIndexed { index, primera ->

                if (primera.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(primeras.get(id))
        }
        return list
    }


    fun sortMatutinas(matutinas: List<GetAllDrawsQuery.Matutina>, array_ids: List<String>): MutableList<GetAllDrawsQuery.Matutina> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetAllDrawsQuery.Matutina> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            matutinas.forEachIndexed { index, matutina ->
                if (matutina.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(matutinas.get(id))
        }
        return list
    }

    fun sortMatutinasSingle(matutinas: List<GetMatutinaQuery.Matutina>, array_ids: List<String>): MutableList<GetMatutinaQuery.Matutina> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetMatutinaQuery.Matutina> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            matutinas.forEachIndexed { index, matutina ->
                if (matutina.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(matutinas.get(id))
        }
        return list
    }


    fun sortVespertinas(vespertinas: List<GetAllDrawsQuery.Vespertina>, array_ids: List<String>): MutableList<GetAllDrawsQuery.Vespertina> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetAllDrawsQuery.Vespertina> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            vespertinas.forEachIndexed { index, vespertina ->
                if (vespertina.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(vespertinas.get(id))
        }
        return list
    }

    fun sortVespertinasSingle(vespertinas: List<GetVespertinaQuery.Vespertina>, array_ids: List<String>): MutableList<GetVespertinaQuery.Vespertina> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetVespertinaQuery.Vespertina> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            vespertinas.forEachIndexed { index, vespertina ->
                if (vespertina.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(vespertinas.get(id))
        }
        return list
    }


    fun sortNocturnas(nocturnas: List<GetAllDrawsQuery.Nocturna>, array_ids: List<String>): MutableList<GetAllDrawsQuery.Nocturna> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetAllDrawsQuery.Nocturna> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            nocturnas.forEachIndexed { index, nocturna ->
                if (nocturna.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(nocturnas.get(id))
        }
        return list
    }

    fun sortNocturnasSingle(nocturnas: List<GetNocturnaQuery.Nocturna>, array_ids: List<String>): MutableList<GetNocturnaQuery.Nocturna> {
        val ids: MutableList<Int> = arrayListOf()
        val list: MutableList<GetNocturnaQuery.Nocturna> = mutableListOf()
        array_ids.forEachIndexed { _, id ->
            nocturnas.forEachIndexed { index, nocturna ->
                if (nocturna.id_loteria().equals(id)) {
                    ids.add(index)
                }
            }
        }
        ids.forEach { id ->
            list.add(nocturnas.get(id))
        }
        return list
    }

}