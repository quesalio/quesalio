package com.resultados.quesalio.extras;

import android.app.Activity;
import android.content.Intent;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.resultados.quesalio.R;
import com.resultados.quesalio.activities.ExitActivity;


public class Ads {

    private static InterstitialAd interstitial;
    private com.facebook.ads.InterstitialAd interstitialAd;

    private Activity activity;

    public Ads(Activity activity) {
        this.activity = activity;
    }

    public static void showInterstitial(final Activity activity) {
        interstitial = new InterstitialAd(activity);
        interstitial.setAdUnitId(activity.getResources().getString(R.string.interstitial_id));

        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                interstitial.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                final com.facebook.ads.InterstitialAd interstitialAd = new com.facebook.ads.InterstitialAd(activity, activity.getResources().getString(R.string.interstitial_idf));
                interstitialAd.setAdListener(new InterstitialAdListener() {
                    @Override
                    public void onInterstitialDisplayed(Ad ad) {

                    }

                    @Override
                    public void onInterstitialDismissed(Ad ad) {

                    }

                    @Override
                    public void onError(Ad ad, AdError adError) {

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        interstitialAd.show();
                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {

                    }
                });
                interstitialAd.loadAd();
            }
        });
        interstitial.loadAd(adRequestBuilder.build());

    }

    public void loadNormalInterstitial() {
        interstitial = new InterstitialAd(activity);
        interstitial.setAdUnitId(activity.getResources().getString(R.string.interstitial_id));
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                interstitialAd = new com.facebook.ads.InterstitialAd(activity, activity.getResources().getString(R.string.interstitial_idf));
                interstitialAd.loadAd();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }
        });
        interstitial.loadAd(adRequestBuilder.build());
    }


    public boolean isEndInterstitialLoaded() {
        return interstitial.isLoaded();
    }

    public void loadEndInterstitial() {
        interstitial = new InterstitialAd(activity);
        interstitial.setAdUnitId(activity.getResources().getString(R.string.interstitial_id));

        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                interstitialAd = new com.facebook.ads.InterstitialAd(activity, activity.getResources().getString(R.string.interstitial_idf));
                interstitialAd.loadAd();
            }

            @Override
            public void onAdClosed() {
                launchActivity();
                super.onAdClosed();
            }

            @Override
            public void onAdLeftApplication() {
                //launchActivity();
                super.onAdLeftApplication();
            }
        });
        interstitial.loadAd(adRequestBuilder.build());
    }

    public void showEndInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
        try {
            if (interstitialAd.isAdLoaded()) {
                interstitialAd.show();
            }
        } catch (Exception ignored) {

        }

    }

    private void launchActivity() {
        Intent intent = new Intent(activity, ExitActivity.class);
        activity.startActivity(intent);
    }


    public static void showBanner(final Activity activity, final LinearLayout bannerLayout) {
        final AdView adView = new AdView(activity);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(activity.getResources().getString(R.string.banner_id));
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                com.facebook.ads.AdView adViewF = new com.facebook.ads.AdView(activity, activity.getResources().getString(R.string.banner_idf), com.facebook.ads.AdSize.BANNER_HEIGHT_50);
                bannerLayout.removeAllViews();
                bannerLayout.addView(adViewF);
                adViewF.loadAd();
            }
        });
        bannerLayout.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    public static void onDestroy() {

    }
}