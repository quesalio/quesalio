package com.resultados.quesalio.extras;

import com.apollographql.apollo.response.CustomTypeAdapter;
import com.apollographql.apollo.response.CustomTypeValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ISO8601Adapter implements CustomTypeAdapter<Date> {
    private static final SimpleDateFormat ISO8601 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    @Override
    public Date decode(CustomTypeValue value) {
        try {
            return ISO8601.parse(value.value.toString());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CustomTypeValue encode(Date value) {
        return new CustomTypeValue.GraphQLString(ISO8601.format(value));
    }

}
