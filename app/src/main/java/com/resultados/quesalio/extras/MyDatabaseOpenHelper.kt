package com.resultados.quesalio.extras

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*
import java.util.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "Salio", null, 2) {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null
        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {

        db.createTable(Config.PLAYS_TABLE, true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "type" to TEXT,
                "numbers" to TEXT
        )
        //val values = ContentValues()
        //values.put("type", "test")
        //values.put("numbers", "test")
        //db.insert(Config.PLAYS_TABLE, null, values)
        //db.delete(Config.PLAYS_TABLE, whereClause = "type={type}", args = "type" to "rtest")
        //db.delete(Config.PLAYS_TABLE, whereClause = "type='test'")

        db.createTable(Config.CITIES_TABLE, true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "name" to TEXT)
        val arrayCities = arrayListOf<String>()
        arrayCities.add("Buenos Aires")
        arrayCities.add("Entre Ríos")
        arrayCities.add("Mendoza")
        arrayCities.add("Córdoba")
        arrayCities.add("Corrientes")
        arrayCities.add("Chaco")
        arrayCities.add("Santiago")
        arrayCities.add("Neuquén")
        arrayCities.add("San Luis")
        arrayCities.add("Salta")
        arrayCities.add("Jujuy")
        arrayCities.add("Tucumán")
        arrayCities.add("Chubut")
        arrayCities.add("Formosa")
        arrayCities.add("Misiones")
        arrayCities.add("Catamarca")
        arrayCities.add("San Juan")
        arrayCities.add("La Rioja")
        arrayCities.forEach { city ->
            val values = ContentValues()
            values.put("name", city)
            db.insert(Config.CITIES_TABLE, null, values)
        }

        db.createTable(Config.CITY_IDS_TABLE, true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "day" to INTEGER,
                "position" to INTEGER,
                "ids" to TEXT)

        val arrayMondays = arrayListOf<String>()
        arrayMondays.add("25,24,38,23,28")//arrayMondays.add("25,24,38,23")
        arrayMondays.add("25,24,39")
        arrayMondays.add("25,24,53")
        arrayMondays.add("25,24,28")
        arrayMondays.add("25,24,42")
        arrayMondays.add("25,24,52")
        arrayMondays.add("25,24,48")
        arrayMondays.add("25,24,41")
        arrayMondays.add("25,24,49,53,28")
        arrayMondays.add("25,24,51,50,55")
        arrayMondays.add("25,24,50")
        arrayMondays.add("25,24,55")
        arrayMondays.add("25,24,56")
        arrayMondays.add("25,24,59")
        arrayMondays.add("25,24,38,60")
        arrayMondays.add("25,24,61")
        arrayMondays.add("25,24,62")
        arrayMondays.add("25,24,63")
        arrayMondays.forEachIndexed { index, monday ->
            val values = ContentValues()
            values.put("day", Calendar.MONDAY)
            values.put("position", index + 1)
            values.put("ids", monday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }

        val arrayTuesdays = arrayListOf<String>()
        arrayTuesdays.add("25,24,38,23,28")//arrayTuesdays.add("25,24,38,23")
        arrayTuesdays.add("25,24,39,23")
        arrayTuesdays.add("25,24,53")
        arrayTuesdays.add("25,24,28")
        arrayTuesdays.add("25,24,42")
        arrayTuesdays.add("25,24,52")
        arrayTuesdays.add("25,24,48")
        arrayTuesdays.add("25,24,41")
        arrayTuesdays.add("25,24,49,53,28")
        arrayTuesdays.add("25,24,51,50,55")
        arrayTuesdays.add("25,24,50")
        arrayTuesdays.add("25,24,55")
        arrayTuesdays.add("25,24,56")
        arrayTuesdays.add("25,24,59")
        arrayTuesdays.add("25,24,38,60")
        arrayTuesdays.add("25,24,61")
        arrayTuesdays.add("25,24,62")
        arrayTuesdays.add("25,24,63")

        arrayTuesdays.forEachIndexed { index, tuesday ->
            val values = ContentValues()
            values.put("day", Calendar.TUESDAY)
            values.put("position", index + 1)
            values.put("ids", tuesday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }

        val arrayWednesdays = arrayListOf<String>()
        arrayWednesdays.add("25,24,38,23,28")//arrayWednesdays.add("25,24,38,23,28")
        arrayWednesdays.add("25,24,39")
        arrayWednesdays.add("25,24,53")
        arrayWednesdays.add("25,24,28")
        arrayWednesdays.add("25,24,42")
        arrayWednesdays.add("25,24,52")
        arrayWednesdays.add("25,24,48")
        arrayWednesdays.add("25,24,41")
        arrayWednesdays.add("25,24,49,53,28")
        arrayWednesdays.add("25,24,51,50,55")
        arrayWednesdays.add("25,24,50")
        arrayWednesdays.add("25,24,55")
        arrayWednesdays.add("25,24,56")
        arrayWednesdays.add("25,24,59")
        arrayWednesdays.add("25,24,38,60")
        arrayWednesdays.add("25,24,61")
        arrayWednesdays.add("25,24,62")
        arrayWednesdays.add("25,24,63")

        arrayWednesdays.forEachIndexed { index, wednesday ->
            val values = ContentValues()
            values.put("day", Calendar.WEDNESDAY)
            values.put("position", index + 1)
            values.put("ids", wednesday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }

        val arrayThursdays = arrayListOf<String>()
        arrayThursdays.add("25,24,38,23,28")//arrayThursdays.add("25,24,38,23")
        arrayThursdays.add("25,24,39")
        arrayThursdays.add("25,24,53")
        arrayThursdays.add("25,24,28")
        arrayThursdays.add("25,24,42")
        arrayThursdays.add("25,24,52")
        arrayThursdays.add("25,24,48")
        arrayThursdays.add("25,24,41")
        arrayThursdays.add("25,24,49,53,28")
        arrayThursdays.add("25,24,51,50,55")
        arrayThursdays.add("25,24,50")
        arrayThursdays.add("25,24,55")
        arrayThursdays.add("25,24,56")
        arrayThursdays.add("25,24,59")
        arrayThursdays.add("25,24,38,60")
        arrayThursdays.add("25,24,61")
        arrayThursdays.add("25,24,62")
        arrayThursdays.add("25,24,63")

        arrayThursdays.forEachIndexed { index, thursday ->
            val values = ContentValues()
            values.put("day", Calendar.THURSDAY)
            values.put("position", index + 1)
            values.put("ids", thursday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }

        val arrayFridays = arrayListOf<String>()
        arrayFridays.add("25,24,38,23,48,28")//arrayFridays.add("25,24,38,23,48")
        arrayFridays.add("25,24,39")
        arrayFridays.add("25,24,53")
        arrayFridays.add("25,24,28,48")
        arrayFridays.add("25,24,42")
        arrayFridays.add("25,24,52")
        arrayFridays.add("25,24,48")
        arrayFridays.add("25,24,41")
        arrayFridays.add("25,24,49,53,28")
        arrayFridays.add("25,24,51,50,55")
        arrayFridays.add("25,24,50")
        arrayFridays.add("25,24,55")
        arrayFridays.add("25,24,56")
        arrayFridays.add("25,24,59")
        arrayFridays.add("25,24,38,60")
        arrayFridays.add("25,24,61")
        arrayFridays.add("25,24,62")
        arrayFridays.add("25,24,63")
        arrayFridays.forEachIndexed { index, friday ->
            val values = ContentValues()
            values.put("day", Calendar.FRIDAY)
            values.put("position", index + 1)
            values.put("ids", friday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }

        val arraySaturdays = arrayListOf<String>()
        arraySaturdays.add("25,24,38,23,53,28")//arraySaturdays.add("25,24,38,23,53")
        arraySaturdays.add("25,24,39,23")
        arraySaturdays.add("25,24,53")
        arraySaturdays.add("25,24,28")
        arraySaturdays.add("25,24,42")
        arraySaturdays.add("25,24,52")
        arraySaturdays.add("25,24,48")
        arraySaturdays.add("25,24,41")
        arraySaturdays.add("25,24,49,53,28")
        arraySaturdays.add("25,24,51,50,55")
        arraySaturdays.add("25,24,50")
        arraySaturdays.add("25,24,55")
        arraySaturdays.add("25,24,56")
        arraySaturdays.add("25,24,59")
        arraySaturdays.add("25,24,38,60")
        arraySaturdays.add("25,24,61")
        arraySaturdays.add("25,24,62")
        arraySaturdays.add("25,24,63")
        arraySaturdays.forEachIndexed { index, saturday ->
            val values = ContentValues()
            values.put("day", Calendar.SATURDAY)
            values.put("position", index + 1)
            values.put("ids", saturday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }

        val arraySundays = arrayListOf<String>()
        arraySundays.add("25,24,38,23,53,28")//arraySundays.add("25,24,38,23,53")
        arraySundays.add("25,24,39,23")
        arraySundays.add("25,24,53")
        arraySundays.add("25,24,28")
        arraySundays.add("25,24,42")
        arraySundays.add("25,24,52")
        arraySundays.add("25,24,48")
        arraySundays.add("25,24,41")
        arraySundays.add("25,24,49,53,28")
        arraySundays.add("25,24,51,50,55")
        arraySundays.add("25,24,50")
        arraySundays.add("25,24,55")
        arraySundays.add("25,24,56")
        arraySundays.add("25,24,59")
        arraySundays.add("25,24,38,60")
        arraySundays.add("25,24,61")
        arraySundays.add("25,24,62")
        arraySundays.add("25,24,63")
        arraySundays.forEachIndexed { index, sunday ->
            val values = ContentValues()
            values.put("day", Calendar.SUNDAY)
            values.put("position", index + 1)
            values.put("ids", sunday)
            db.insert(Config.CITY_IDS_TABLE, null, values)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(Config.PLAYS_TABLE, true)
        db.dropTable(Config.CITIES_TABLE, true)
        db.dropTable(Config.CITY_IDS_TABLE, true)
    }


}

val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)



