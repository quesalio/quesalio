package com.resultados.quesalio.extras

import android.content.Context
import android.content.SharedPreferences
import android.graphics.*
import android.preference.PreferenceManager
import com.squareup.picasso.Transformation
import java.math.BigDecimal
import java.util.*


class Utiles(context: Context) {
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val editor: SharedPreferences.Editor

    var launch: Boolean
        get() = prefs.getBoolean("launched", false)
        set(launched) {
            editor.putBoolean("launched", launched)
            editor.apply()
        }

    init {
        editor = prefs.edit()
    }

    fun guardarFechaActual(fecha: String) {
        editor.putString("fecha_actual", fecha)
        editor.apply()
    }

    fun obtenerFechaActual(): String {
        return prefs.getString("fecha_actual", "")!!
    }

    fun guardarCiudad(posicion: Int) {
        editor.putInt("posicion", posicion)
        editor.apply()
    }

    fun obtenerCiudad(): Int {
        return prefs.getInt("posicion", 0)
    }


    fun obtenerNumeroGanadores(ganadores: String): String {
        if (ganadores == "1") {
            return "UN GANADOR"
        } else if (ganadores == "0") {
            return "POZO VACANTE"
        }
        return "$ganadores GANADORES"

    }

    class CircleTransform : Transformation {

        private var x: Int = 0
        private var y: Int = 0

        override fun transform(source: Bitmap): Bitmap {
            val size = Math.min(source.width, source.height)

            x = (source.width - size) / 2
            y = (source.height - size) / 2

            val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)
            if (squaredBitmap !== source) source.recycle()
            val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)

            val canvas = Canvas(bitmap)
            val paint = Paint()
            val shader = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            paint.shader = shader
            paint.isAntiAlias = true

            val r = size / 2f
            canvas.drawCircle(r, r, r, paint)

            squaredBitmap.recycle()
            return bitmap
        }

        override fun key() = "circle(x=$x,y=$y)"
    }

    companion object {
        private var mInstance: Utiles? = null
        private val PROPERTY_APP_VERSION = "1"

        fun getInstance(context: Context): Utiles {
            if (mInstance == null) {
                mInstance = Utiles(context)
            }
            return mInstance as Utiles
        }

        @Throws(Exception::class)
        fun formatearNumero(numero: String): String {
            var numero = numero
            numero = String.format("$%,.2f", java.lang.Double.parseDouble(numero))
            numero = numero.replace(".", "-")
            numero = numero.replace(",", ".")
            numero = numero.replace("-", ",")
            return numero
        }

        fun seleccionarFechaActual(fechaElegida: String): String {
            var dias = ""
            try {
                val cal1 = Calendar.getInstance()
                cal1.time = Date()
                val cal2 = Calendar.getInstance()
                val fe = fechaElegida.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val anio: Int
                val mes: Int
                val dia: Int
                anio = Integer.parseInt(fe[0])
                mes = Integer.parseInt(fe[1]) - 1
                dia = Integer.parseInt(fe[2])
                cal2.set(anio, mes, dia)
                val milis1 = cal1.timeInMillis
                val milis2 = cal2.timeInMillis
                val diff = milis1 - milis2
                val diffDays = diff / (24 * 60 * 60 * 1000)
                val diferencia = BigDecimal(diffDays).intValueExact()
                when (diferencia) {
                    0 -> dias = "HOY"
                    1 -> dias = "AYER"
                    2 -> dias = "ANTES DE AYER"
                    3, 4, 5, 6 -> dias = "HACE $diferencia DIAS"
                    7, 8, 9, 10, 11, 12, 13 -> dias = "HACE 1 SEMANA"
                    14, 15 -> dias = "HACE 2 SEMANAS"
                    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 -> dias = "HACE 3 SEMANAS"
                    31 -> dias = "HACE 1 MES"
                    else -> {
                    }
                }
            } catch (ignored: Exception) {
            }

            return dias
        }
    }

     fun formatDate(date: Date): String {
        var dias = ""
        val cal1 = Calendar.getInstance()
        cal1.time = Date()
        val cal2 = Calendar.getInstance()
        cal2.time = date
        val milis1 = cal1.timeInMillis
        val milis2 = cal2.timeInMillis
        val diff = milis1 - milis2
        val diffDays = diff / (24 * 60 * 60 * 1000)
        val diferencia = BigDecimal(diffDays).intValueExact()
        when (diferencia) {
            0 -> dias = "HOY"
            1 -> dias = "AYER"
            2 -> dias = "ANTES DE AYER"
            3, 4, 5, 6 -> dias = "HACE $diferencia DIAS"
            7, 8, 9, 10, 11, 12, 13 -> dias = "HACE 1 SEMANA"
            14, 15 -> dias = "HACE 2 SEMANAS"
            16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 -> dias = "HACE 3 SEMANAS"
            31 -> dias = "HACE 1 MES"
            else -> {
            }
        }
        return dias
    }

}
