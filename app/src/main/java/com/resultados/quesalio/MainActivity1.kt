package com.resultados.quesalio


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.transition.Fade
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.AdapterView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloSubscriptionCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.api.cache.http.HttpCachePolicy
import com.apollographql.apollo.exception.ApolloException
import com.google.firebase.analytics.FirebaseAnalytics
import com.parse.ParseQuery
import com.resultados.quesalio.activities.*
import com.resultados.quesalio.adapters.*
import com.resultados.quesalio.entities.Draw
import com.resultados.quesalio.entities.Ephemerides
import com.resultados.quesalio.entities.Fecha
import com.resultados.quesalio.entities.Recomendacion
import com.resultados.quesalio.extras.*
import com.resultados.quesalio.extras.ListUtils.sortMatutinas
import com.resultados.quesalio.extras.ListUtils.sortNocturnas
import com.resultados.quesalio.extras.ListUtils.sortPrimeras
import com.resultados.quesalio.extras.ListUtils.sortVespertinas
import com.resultados.quesalio.models.Horoscope
import com.resultados.quesalio.models.Prediction
import com.resultados.quesalio.viewmodels.MatutinaViewModel
import com.resultados.quesalio.viewmodels.NocturnaViewModel
import com.resultados.quesalio.viewmodels.PrimeraViewModel
import com.resultados.quesalio.viewmodels.VespertinaViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.SqlOrderDirection
import org.jetbrains.anko.db.select
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import java.util.concurrent.TimeUnit


class MainActivity1 : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var activityStarted: Boolean = false

    private lateinit var utiles: Utiles
    private var cities = mutableListOf<String>()
    private val dates = ArrayList<Fecha>()

    private var ids = mutableListOf<String>()
    private var chosenIds = ""
    private var chosenDate: Fecha = Fecha()
    private lateinit var activity: AppCompatActivity

    private val ephemeridesList = mutableListOf<Ephemerides>()
    private var recomendationList = mutableListOf<Recomendacion>()

    private lateinit var primeraAdapter: DrawAdapter
    private lateinit var matutinaAdapter: DrawAdapter
    private lateinit var vespertinaAdapter: DrawAdapter
    private lateinit var nocturnaAdapter: DrawAdapter
    private var ads: Ads? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.getBooleanExtra("EXIT", false)) {
            finish()
        }
        if (activityStarted
                && intent != null
                && intent.flags and Intent.FLAG_ACTIVITY_REORDER_TO_FRONT != 0) {
            finish()
            return
        }
        activityStarted = true

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

        activity = this
        utiles = Utiles.getInstance(applicationContext)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val fade = Fade()
            fade.excludeTarget(R.id.appBar, true)
            fade.excludeTarget(android.R.id.statusBarBackground, true)
            fade.excludeTarget(android.R.id.navigationBarBackground, true)
            window.enterTransition = fade
            window.exitTransition = fade
        }
        dates.clear()
        for (i in 0..4) {
            val resultados = ListUtils.getDates(-1 * i)
            val fecha = Fecha()
            fecha.letras = resultados[0]
            val numeros = resultados[1].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            numeros[2] = String.format("%02d", Integer.parseInt(numeros[2]))
            fecha.numeros = Arrays.toString(numeros).replace(", ", "-").replace("[\\[\\]]".toRegex(), "")
            dates.add(fecha)
        }

        val dateAdapter = FechaAdapter(applicationContext, dates)
        spinnerDays?.adapter = dateAdapter


        ids = getCityIds()
        cities = getCities()
        val adapterCity = CityAdapter(applicationContext, cities)
        spinnerCities.adapter = adapterCity

        //todo sacar el spinner despuÃ©s de coger la configuraciÃ³n
        /*spinnerCities.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                utiles.obtenerFechaActual()?.let {
                    if (it.isNotEmpty()) {
                        utiles.guardarCiudad(position)
                        getData()
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }*/
        layout4.setOnClickListener {
            val intent = Intent(this, LotoActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
        layout3.setOnClickListener {
            val intent = Intent(this, QuiniActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
        layout2.setOnClickListener {
            val intent = Intent(this, PoceadaPlusActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
        layout1.setOnClickListener {
            val intent = Intent(this, PoceadaActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
        btnSeeAllRecomendations.setOnClickListener {
            val intent = Intent(this, ShowRecomendationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
        btnShowHoroscope.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=socialgold.horoscopotuestrella")))
            } catch (e: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=socialgold.horoscopotuestrella")))
            }
        }

    }


    override fun onStart() {
        super.onStart()
        ads = Ads(this)
        ads?.loadEndInterstitial()
        Ads.showBanner(activity, adsMain)
        subscriptionConfiguration()
        //subscriptionPrimera()
        //subscriptionMatutina()
        //subscriptionVespertina()
        //subscriptionNocturna()
        loadEphemerides()
        obtainDolarValue()
        getRandomSign()
        loadRecomendations()
        getDrawDates()
        getConfiguration()
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics.setCurrentScreen(this, "Principal", "Principal")
    }

    private fun subscriptionPrimera() {
        val primeraViewModel = ViewModelProviders.of(this).get(PrimeraViewModel::class.java)
        primeraViewModel.listen().observe(this, android.arch.lifecycle.Observer { arrayList ->
            val listPrimeras = arrayListOf<Draw>()
            arrayList?.forEach {
                val array_ids = chosenIds.split(",")
                if (array_ids.contains(it.id_loteria)) {
                    listPrimeras.add(it)
                }
            }
            primeraAdapter.submitList(listPrimeras)
        })


        MyApplication.sInstance.apolloClient.subscribe(GetPrimeraNotifiedSubscription.builder().build()).execute(object : ApolloSubscriptionCall.Callback<GetPrimeraNotifiedSubscription.Data> {
            override fun onFailure(e: ApolloException) {
                //runOnUiThread { longToast("primera failure: ${e.message}") }
                //Log.e("primera error", e.message)
            }

            override fun onResponse(response: Response<GetPrimeraNotifiedSubscription.Data>) {
                primeraViewModel.load(response, chosenDate.numeros)
            }

            override fun onCompleted() {
                //Log.e("old", "completed")
            }

        })
    }

    private fun subscriptionMatutina() {
        val matutinaViewModel = ViewModelProviders.of(this).get(MatutinaViewModel::class.java)
        matutinaViewModel.listen().observe(this, android.arch.lifecycle.Observer { arrayList ->
            val listMatutinas = arrayListOf<Draw>()
            arrayList?.forEach {
                val array_ids = chosenIds.split(",")
                if (array_ids.contains(it.id_loteria)) {
                    listMatutinas.add(it)
                }
            }

            matutinaAdapter.submitList(listMatutinas)
        })


        MyApplication.sInstance.apolloClient.subscribe(GetMatutinaNotifiedSubscription.builder().build()).execute(object : ApolloSubscriptionCall.Callback<GetMatutinaNotifiedSubscription.Data> {
            override fun onFailure(e: ApolloException) {
                //runOnUiThread { longToast("matutina failure: ${e.message}") }
                //Log.e("matutina error", e.message)
            }

            override fun onResponse(response: Response<GetMatutinaNotifiedSubscription.Data>) {
                matutinaViewModel.load(response, chosenDate.numeros)
            }

            override fun onCompleted() {
            }

        })
    }

    private fun subscriptionVespertina() {
        val vespertinaViewModel = ViewModelProviders.of(this).get(VespertinaViewModel::class.java)
        vespertinaViewModel.listen().observe(this, android.arch.lifecycle.Observer { arrayList ->
            val listVespertinas = arrayListOf<Draw>()
            arrayList?.forEach {
                val array_ids = chosenIds.split(",")
                if (array_ids.contains(it.id_loteria)) {
                    listVespertinas.add(it)
                }
            }
            vespertinaAdapter.submitList(listVespertinas)
        })


        MyApplication.sInstance.apolloClient.subscribe(GetVespertinaNotifiedSubscription.builder().build()).execute(object : ApolloSubscriptionCall.Callback<GetVespertinaNotifiedSubscription.Data> {
            override fun onFailure(e: ApolloException) {
                //runOnUiThread { longToast("vespertina failure: ${e.message}") }
                //Log.e("vespertina error", e.message)
            }

            override fun onResponse(response: Response<GetVespertinaNotifiedSubscription.Data>) {
                vespertinaViewModel.load(response, chosenDate.numeros)
            }

            override fun onCompleted() {
            }

        })
    }

    private fun subscriptionNocturna() {
        val nocturnaViewModel = ViewModelProviders.of(this).get(NocturnaViewModel::class.java)
        nocturnaViewModel.listen().observe(this, android.arch.lifecycle.Observer { arrayList ->
            val listNocturnas = arrayListOf<Draw>()
            arrayList?.forEach {
                val array_ids = chosenIds.split(",")
                if (array_ids.contains(it.id_loteria)) {
                    listNocturnas.add(it)
                }
            }

            nocturnaAdapter.submitList(listNocturnas)
        })


        MyApplication.sInstance.apolloClient.subscribe(GetNocturnaNotifiedSubscription.builder().build()).execute(object : ApolloSubscriptionCall.Callback<GetNocturnaNotifiedSubscription.Data> {
            override fun onFailure(e: ApolloException) {
                //runOnUiThread { longToast("nocturna failure: ${e.message}") }
                //Log.e("nocturna error", e.message)
            }

            override fun onResponse(response: Response<GetNocturnaNotifiedSubscription.Data>) {
                nocturnaViewModel.load(response, chosenDate.numeros)
            }

            override fun onCompleted() {
            }

        })
    }

    private fun subscriptionConfiguration() {
        MyApplication.sInstance.apolloClient.subscribe(GetConfigurationChangedSubscription.builder().build()).execute(object : ApolloSubscriptionCall.Callback<GetConfigurationChangedSubscription.Data> {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetConfigurationChangedSubscription.Data>) {
                response.data()?.configuration_changed?.let { config ->
                    utiles.guardarFechaActual(Config.SDF.format(config.date))
                    val showEphemerides = config.showEphemerides.equals("1")
                    val showNumbers = config.showNumbers.equals("1")
                    val showRecomendations = config.showRecomendations.equals("1")
                    val showBlink = config.blink.equals("1")
                    runOnUiThread {
                        if (showEphemerides) {
                            loadEphemerides()
                            containerCardEphemerides?.visibility = View.VISIBLE
                        } else {
                            containerCardEphemerides?.visibility = View.GONE
                        }
                        if (showNumbers) {
                            loadNumbers()
                            containerCardWeeklyData?.visibility = View.VISIBLE
                        } else {
                            containerCardWeeklyData?.visibility = View.GONE
                        }
                        if (showRecomendations) {
                            loadRecomendations()
                            containerCardDailyRecomendations?.visibility = View.VISIBLE
                        } else {
                            containerCardDailyRecomendations?.visibility = View.GONE
                        }
                        if (showBlink) {
                            showBlinkMessage(config.blink_message().toString())
                        } else {
                            txtBlink?.text = ""
                            txtBlink?.visibility = View.GONE
                        }
                        //selectDate()
                        //getData()
                    }
                }
            }

            override fun onCompleted() {
            }

        })
    }


    private fun getCities() = database.use {
        val cities = mutableListOf<String>()
        select(Config.CITIES_TABLE, "name").parseList(object : MapRowParser<List<String>> {
            override fun parseRow(columns: Map<String, Any?>): List<String> {
                val city = columns.getValue("name").toString()
                cities.add(city)
                return cities
            }
        })
        cities
    }

    private fun getCityIds(dayOfWeek: Int = -1) = database.use {
        val calendar = Calendar.getInstance()
        var day = dayOfWeek
        if (day == -1) {
            day = calendar.get(Calendar.DAY_OF_WEEK)
        }
        val ids = mutableListOf<String>()
        select(Config.CITY_IDS_TABLE, "ids")
                .whereArgs("day={day}", "day" to day)
                .orderBy("position", SqlOrderDirection.ASC).parseList(object : MapRowParser<List<String>> {
                    override fun parseRow(columns: Map<String, Any?>): List<String> {
                        val id = columns.getValue("ids").toString()
                        ids.add(id)
                        return ids
                    }
                })
        ids
    }


    private fun changeIds() {
        val calendar = Calendar.getInstance()
        chosenDate.numeros?.isNotEmpty()?.let {
            calendar.time = Config.SDF.parse(chosenDate.numeros)
        }
        ids = getCityIds(calendar.get(Calendar.DAY_OF_WEEK))
    }

    private fun getData() {
        val listPrimeras = mutableListOf<Draw>()
        val listMatutinas = mutableListOf<Draw>()
        val listVespertinas = mutableListOf<Draw>()
        val listNocturnas = mutableListOf<Draw>()

        chosenIds = try {
            ids[utiles.obtenerCiudad()]
        } catch (ignored: Exception) {
            ids[0]
        }
        chosenDate.numeros?.let {
            if (it.isNotEmpty()) {
                val arrayIds = chosenIds.split(",")
                MyApplication.sInstance.apolloClient
                        .query(GetAllDrawsQuery.builder().date(it).ids(chosenIds).build())
                        .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(1, TimeUnit.MINUTES))
                        .enqueue(object : ApolloCall.Callback<GetAllDrawsQuery.Data>() {
                            override fun onFailure(e: ApolloException) {
                            }

                            override fun onResponse(response: Response<GetAllDrawsQuery.Data>) {
                                response.data()?.let { alldata ->
                                    alldata.primera?.let { primeras ->
                                        if (primeras.size > 0) {
                                            val newPrimeras = sortPrimeras(primeras, arrayIds)
                                            newPrimeras.forEach { primera ->
                                                val primeraObject = Draw(primera.id_draw())
                                                primeraObject.id = primera.id
                                                primeraObject.city = primera.city
                                                primeraObject.date = primera.date
                                                primeraObject.value = primera.value
                                                primeraObject.id_loteria = primera.id_loteria
                                                primeraObject.meaning = primera.meaning
                                                primeraObject.meaning_url = primera.meaning_image
                                                primeraObject.meaning_number = primera.meaning_number()
                                                val numbers = mutableListOf<GetPrimeraQuery.Number>()
                                                primera.numbers?.forEach { number ->
                                                    numbers.add(GetPrimeraQuery.Number(number.__typename, number.number, number.value))
                                                }
                                                primeraObject.primeraNumbers = numbers
                                                listPrimeras.add(primeraObject)
                                            }
                                            runOnUiThread {
                                                primeraAdapter = DrawAdapter(chosenIds, chosenDate, Config.PRIMERA_TYPE)
                                                containerListPrimera.setHasFixedSize(true)
                                                containerListPrimera.itemAnimator = DefaultItemAnimator()
                                                containerListPrimera.layoutManager = LinearLayoutManager(applicationContext)
                                                containerListPrimera.adapter = primeraAdapter
                                                primeraAdapter.submitList(listPrimeras)
                                            }
                                        }
                                    }
                                    alldata.matutina?.let { matutinas ->
                                        if (matutinas.size > 0) {
                                            val newMatutinas = sortMatutinas(matutinas, arrayIds)
                                            newMatutinas.forEach { matutina ->
                                                val matutinaObject = Draw(matutina.id_draw())
                                                matutinaObject.id = matutina.id
                                                matutinaObject.city = matutina.city
                                                matutinaObject.date = matutina.date
                                                matutinaObject.value = matutina.value
                                                matutinaObject.id_loteria = matutina.id_loteria
                                                matutinaObject.meaning = matutina.meaning
                                                matutinaObject.meaning_url = matutina.meaning_image
                                                matutinaObject.meaning_number = matutina.meaning_number()
                                                val numbers = mutableListOf<GetMatutinaQuery.Number>()
                                                matutina.numbers?.forEach { number ->
                                                    numbers.add(GetMatutinaQuery.Number(number.__typename, number.number, number.value))
                                                }
                                                matutinaObject.matutinaNumbers = numbers
                                                listMatutinas.add(matutinaObject)
                                            }
                                            runOnUiThread {
                                                matutinaAdapter = DrawAdapter(chosenIds, chosenDate, Config.MATUTINA_TYPE)
                                                containerListMatutina.setHasFixedSize(true)
                                                containerListMatutina.itemAnimator = DefaultItemAnimator()
                                                containerListMatutina.layoutManager = LinearLayoutManager(applicationContext)
                                                containerListMatutina.adapter = matutinaAdapter
                                                matutinaAdapter.submitList(listMatutinas)
                                            }
                                        }
                                    }
                                    alldata.vespertina?.let { vespertinas ->
                                        if (vespertinas.size > 0) {
                                            val newVespertinas = sortVespertinas(vespertinas, arrayIds)
                                            newVespertinas.forEach { vespertina ->
                                                val vespertinaObject = Draw(vespertina.id_draw())
                                                vespertinaObject.id = vespertina.id
                                                vespertinaObject.city = vespertina.city
                                                vespertinaObject.date = vespertina.date
                                                vespertinaObject.value = vespertina.value
                                                vespertinaObject.id_loteria = vespertina.id_loteria
                                                vespertinaObject.meaning = vespertina.meaning
                                                vespertinaObject.meaning_url = vespertina.meaning_image
                                                vespertinaObject.meaning_number = vespertina.meaning_number()
                                                val numbers = mutableListOf<GetVespertinaQuery.Number>()
                                                vespertina.numbers?.forEach { number ->
                                                    numbers.add(GetVespertinaQuery.Number(number.__typename, number.number, number.value))
                                                }
                                                vespertinaObject.vespertinaNumbers = numbers
                                                listVespertinas.add(vespertinaObject)
                                            }
                                            runOnUiThread {
                                                vespertinaAdapter = DrawAdapter(chosenIds, chosenDate, Config.VESPERTINA_TYPE)
                                                containerListVespertina.setHasFixedSize(true)
                                                containerListVespertina.itemAnimator = DefaultItemAnimator()
                                                containerListVespertina.layoutManager = LinearLayoutManager(applicationContext)
                                                containerListVespertina.adapter = vespertinaAdapter
                                                vespertinaAdapter.submitList(listVespertinas)
                                            }
                                        }
                                    }
                                    alldata.nocturna?.let { nocturnas ->
                                        if (nocturnas.size > 0) {
                                            val newNocturnas = sortNocturnas(nocturnas, arrayIds)
                                            newNocturnas.forEach { nocturna ->
                                                val nocturnaObject = Draw(nocturna.id_draw())
                                                nocturnaObject.id = nocturna.id
                                                nocturnaObject.city = nocturna.city
                                                nocturnaObject.date = nocturna.date
                                                nocturnaObject.value = nocturna.value
                                                nocturnaObject.id_loteria = nocturna.id_loteria
                                                nocturnaObject.meaning = nocturna.meaning
                                                nocturnaObject.meaning_url = nocturna.meaning_image
                                                nocturnaObject.meaning_number = nocturna.meaning_number()
                                                val numbers = mutableListOf<GetNocturnaQuery.Number>()
                                                nocturna.numbers?.forEach { number ->
                                                    numbers.add(GetNocturnaQuery.Number(number.__typename, number.number, number.value))
                                                }
                                                nocturnaObject.nocturnaNumbers = numbers
                                                listNocturnas.add(nocturnaObject)
                                            }
                                            runOnUiThread {
                                                nocturnaAdapter = DrawAdapter(chosenIds, chosenDate, Config.NOCTURNA_TYPE)
                                                containerListNocturna.setHasFixedSize(true)
                                                containerListNocturna.itemAnimator = DefaultItemAnimator()
                                                containerListNocturna.layoutManager = LinearLayoutManager(applicationContext)
                                                containerListNocturna.adapter = nocturnaAdapter
                                                nocturnaAdapter.submitList(listNocturnas)
                                            }
                                        }

                                    }

                                }
                            }

                        })
            }
        }
    }

    private fun loadEphemerides() {
        MyApplication.sInstance.apolloClient
                .query(GetEphemeridesQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(12, TimeUnit.HOURS))
                .enqueue(object : ApolloCall.Callback<GetEphemeridesQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetEphemeridesQuery.Data>) {
                        response.data()?.ephemerides()?.let { ephemeridesGraphQLList ->
                            ephemeridesList.clear()
                            ephemeridesGraphQLList.forEach { ephemeride ->
                                val ephemerides = Ephemerides()
                                ephemerides.number = ephemeride.number()!!
                                ephemerides.description = ephemeride.description()!!
                                ephemerides.image = ephemeride.image()!!
                                ephemeridesList.add(ephemerides)
                            }
                            runOnUiThread {
                                val adapter = EphemeridesAdapter(ephemeridesList)
                                containerEphemerides.layoutManager = GridLayoutManager(applicationContext, 2)
                                containerEphemerides.setHasFixedSize(true)
                                containerEphemerides.adapter = adapter
                            }
                        }
                    }

                })
    }


    private fun obtainDolarValue() {
        MyApplication.sInstance.apolloClient
                .query(GetDollarQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(6, TimeUnit.HOURS))
                .enqueue(object : ApolloCall.Callback<GetDollarQuery.Data>() {
                    override fun onFailure(e: ApolloException) {

                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetDollarQuery.Data>) {
                        response.data()?.dollar?.let { dollar ->
                            //val dollar = it.get(0)
                            runOnUiThread {
                                txtValorCompra?.text = dollar.buy()
                                txtValorVenta?.text = dollar.sell()
                            }
                        }
                    }

                })
    }

    private fun getRandomSign() {
        val query = ParseQuery.getQuery<Horoscope>("Horoscope")
        query.orderByDescending("createdAt")
        query.whereEqualTo("language", "es")
        query.getFirstInBackground { horoscope, e ->
            if (e == null) {
                val q = ParseQuery.getQuery<Prediction>("Prediction")
                q.whereEqualTo("horoscope", horoscope)
                q.findInBackground { predictionList, ee ->
                    if (ee == null) {
                        if (predictionList.size > 0) {
                            try {
                                val prediction = getRandomPrediction(predictionList)
                                lblSign?.text = prediction.sign
                                lblNumberOne?.text = prediction.numberOne
                                lblNumberTwo?.text = prediction.numberTwo
                                if (prediction.general.length < 150) {
                                    lblGeneral?.text = prediction.general
                                } else {
                                    lblGeneral?.text = prediction.general.substring(0, 114) + "..."
                                }
                                try {
                                    Picasso.with(applicationContext).load(selectImage(prediction.sign.toLowerCase())).into(imageSign)
                                } catch (ignored: Exception) {
                                }
                            } catch (ignored: Exception) {

                            }
                        }
                    } else {
                        Log.e("error prediction", ee.message)
                    }
                }
            } else {
                Log.e("error horoscope", e.message)
            }
        }
    }

    private fun selectImage(sign: String): Int {
        var selected = 0
        when (sign) {
            "aries" -> selected = R.drawable.aries
            "tauro" -> selected = R.drawable.taurus
            "gÃ©minis", "geminis" -> selected = R.drawable.gemini
            "cancer", "cÃ¡ncer" -> selected = R.drawable.cancer
            "leo" -> selected = R.drawable.leo
            "virgo" -> selected = R.drawable.virgo
            "libra" -> selected = R.drawable.libra
            "escorpio" -> selected = R.drawable.scorpio
            "sagitario" -> selected = R.drawable.sagittarius
            "capricornio" -> selected = R.drawable.capricorn
            "acuario" -> selected = R.drawable.aquarius
            "piscis" -> selected = R.drawable.pisces
        }
        return selected
    }

    private fun getRandomPrediction(predictions: List<Prediction>): Prediction {
        val index = Random().nextInt(predictions.size)
        return predictions[index]
    }


    private fun loadRecomendations() {
        MyApplication.sInstance.apolloClient
                .query(GetRecomendationsQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(1, TimeUnit.MINUTES))
                .enqueue(object : ApolloCall.Callback<GetRecomendationsQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetRecomendationsQuery.Data>) {
                        response.data()?.recomendation?.let { recomendations ->

                            runOnUiThread {
                                recomendationList.clear()
                                recomendations.forEach { recomendation ->
                                    //Log.e("entra", "${recomendation.date}")
                                    //val calendar = Calendar.getInstance()
                                    //calendar.timeInMillis = recomendation.date.toString().toLong()
                                    val rec = Recomendacion()
                                    rec.nombre = recomendation.name()
                                    rec.imagen = recomendation.image()
                                    rec.fecha = recomendation.date()!!
                                    rec.recomendacion = recomendation.recomendation()
                                    recomendationList.add(rec)
                                }
                                if (recomendationList.size >= 4) {
                                    recomendationList = recomendationList.subList(0, 4)
                                }
                                val adapter = RecomendationAdapter(recomendationList, applicationContext)
                                containerRecomendations.layoutManager = GridLayoutManager(applicationContext, 2)
                                containerRecomendations.setHasFixedSize(true)
                                containerRecomendations.adapter = adapter
                            }

                        }
                    }

                })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_notifications -> {
                startActivity(intentFor<SettingsActivity>().singleTop())
                Bungee.slideLeft(this)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_quiniela -> {
                browse(getString(R.string.url_site))
            }
            R.id.nav_poceada -> {
                val intent = Intent(this, PoceadaActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_poceada_plus -> {
                val intent = Intent(this, PoceadaPlusActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_quini -> {
                val intent = Intent(this, QuiniActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_loto -> {
                val intent = Intent(this, LotoActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_brinco -> {
                val intent = Intent(this, BrincoActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_notifications -> {
                val intent = Intent(this, SettingsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_today -> {
                val intent = Intent(this, ShowRecomendationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_plays -> {
                val intent = Intent(this, PlaysActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
            R.id.nav_rate -> {
                rateApp()
            }
            R.id.nav_share -> {
                shareApp()
            }
            R.id.nav_meanings -> {
                val intent = Intent(this, MeaningActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun shareApp() {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        val shareBody = "TenÃ© los resultados de las quinielas al instante Â¡GRATIS! https://play.google.com/store/apps/details?id=com.resultados.quesalio"
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Â¿QuÃ© SaliÃ³?")
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, "Compartir"))
    }

    private fun rateApp() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$packageName")))
        } catch (e: android.content.ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=$packageName")))
        }
    }

    private fun getConfiguration() {
        MyApplication.sInstance.apolloClient.query(GetConfigurationQuery.builder().build()).enqueue(object : ApolloCall.Callback<GetConfigurationQuery.Data>() {
            override fun onFailure(e: ApolloException) {
            }

            override fun onResponse(response: Response<GetConfigurationQuery.Data>) {
                response.data()?.configuration?.let { config ->
                    utiles.guardarFechaActual(Config.SDF.format(config.date))
                    val showEphemerides = config.showEphemerides.equals("1")
                    val showNumbers = config.showNumbers.equals("1")
                    val showRecomendations = config.showRecomendations.equals("1")
                    val showBlink = config.blink.equals("1")
                    runOnUiThread {
                        if (showEphemerides) {
                            loadEphemerides()
                            containerCardEphemerides?.visibility = View.VISIBLE
                        } else {
                            containerCardEphemerides?.visibility = View.GONE
                        }
                        if (showNumbers) {
                            loadNumbers()
                            containerCardWeeklyData?.visibility = View.VISIBLE
                        } else {
                            containerCardWeeklyData?.visibility = View.GONE
                        }
                        if (showRecomendations) {
                            loadRecomendations()
                            containerCardDailyRecomendations?.visibility = View.VISIBLE
                        } else {
                            containerCardDailyRecomendations?.visibility = View.GONE
                        }
                        if (showBlink) {
                            showBlinkMessage(config.blink_message().toString())
                        } else {
                            txtBlink?.text = ""
                            txtBlink?.visibility = View.GONE
                        }

                        spinnerDays?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                utiles.obtenerFechaActual()?.let {
                                    if (it.isNotEmpty()) {
                                        chosenDate = dates[position]
                                        changeIds()
                                        getData()
                                    }
                                }
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {

                            }
                        }

                        spinnerCities.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                utiles.obtenerFechaActual()?.let {
                                    if (it.isNotEmpty()) {
                                        utiles.guardarCiudad(position)
                                        getData()
                                    }
                                }
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {

                            }
                        }
                        selectDate()
                        getData()
                    }

                }
            }
        })
    }

    private fun selectDate() {
        val counter = dates.size
        for (i in 0 until counter) {
            utiles.obtenerFechaActual()?.let {
                if (dates[i].numeros.equals(it)) {
                    chosenDate = dates[i]
                    runOnUiThread {
                        spinnerDays?.setSelection(i, true)
                    }
                    return@selectDate
                }
            }
        }
    }

    private fun loadNumbers() {
        MyApplication.sInstance.apolloClient
                .query(GetWeeklyQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(1, TimeUnit.DAYS))
                .enqueue(object : ApolloCall.Callback<GetWeeklyQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetWeeklyQuery.Data>) {
                        response.data()?.weekly?.let { weeklies ->
                            val weekly = weeklies.get(0)
                            runOnUiThread {
                                flyerNumber1?.text = weekly.number1
                                flyerNumber2?.text = weekly.number2
                                flyerNumber3?.text = weekly.number3
                                flyerNumber4?.text = weekly.number4
                                flyerNumber5?.text = weekly.number5
                                flyerNumber6?.text = weekly.number6
                                flyerTitle?.text = getString(R.string.valid_until) + " " + Config.SDFLetter.format(weekly.date)
                            }
                        }
                    }

                })
    }

    private fun showBlinkMessage(message: String) = runOnUiThread {
        txtBlink?.text = message
        txtBlink?.visibility = View.VISIBLE
        val anim = AlphaAnimation(0.1f, 1.0f)
        anim.duration = 500
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        txtBlink?.animation = anim
    }

    private fun getDrawDates() {
        MyApplication.sInstance.apolloClient
                .query(GetQuini6ShortQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(12, TimeUnit.HOURS))
                .enqueue(object : ApolloCall.Callback<GetQuini6ShortQuery.Data>() {
                    override fun onFailure(e: ApolloException) {

                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetQuini6ShortQuery.Data>) {
                        response.data()?.quini6?.let { quinis ->
                            val date = quinis.get(0).date;
                            runOnUiThread {
                                lblx6?.text = Utiles.seleccionarFechaActual(Config.SDF.format(date))
                            }
                        }
                    }
                })
        MyApplication.sInstance.apolloClient
                .query(GetPoceadaShortQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(12, TimeUnit.HOURS))
                .enqueue(object : ApolloCall.Callback<GetPoceadaShortQuery.Data>() {
                    override fun onFailure(e: ApolloException) {

                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetPoceadaShortQuery.Data>) {
                        response.data()?.poceada?.let { poceadas ->
                            val date = poceadas.get(0).date;
                            runOnUiThread {
                                lblx2?.text = Utiles.seleccionarFechaActual(Config.SDF.format(date))
                            }
                        }
                    }
                })
        MyApplication.sInstance.apolloClient
                .query(GetPoceadaPlusShortQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(12, TimeUnit.HOURS))
                .enqueue(object : ApolloCall.Callback<GetPoceadaPlusShortQuery.Data>() {
                    override fun onFailure(e: ApolloException) {

                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetPoceadaPlusShortQuery.Data>) {
                        response.data()?.poceada_plus?.let { poceadas ->
                            val date = poceadas.get(0).date;
                            runOnUiThread {
                                lblx4?.text = Utiles.seleccionarFechaActual(Config.SDF.format(date))
                            }
                        }
                    }
                })
        MyApplication.sInstance.apolloClient
                .query(GetLotoShortQuery.builder().build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(12, TimeUnit.HOURS))
                .enqueue(object : ApolloCall.Callback<GetLotoShortQuery.Data>() {
                    override fun onFailure(e: ApolloException) {

                    }

                    override fun onResponse(response: com.apollographql.apollo.api.Response<GetLotoShortQuery.Data>) {
                        response.data()?.loto?.let { lotos ->
                            val date = lotos.get(0).date;
                            runOnUiThread {
                                lblx8?.text = Utiles.seleccionarFechaActual(Config.SDF.format(date))
                            }
                        }
                    }
                })
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
        ads?.let {
            if (it.isEndInterstitialLoaded) {
                ads?.showEndInterstitial()
            } else {
                finish()
            }
        }
    }
}
