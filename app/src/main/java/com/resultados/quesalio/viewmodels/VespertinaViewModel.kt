package com.resultados.quesalio.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.apollographql.apollo.api.Response
import com.resultados.quesalio.GetVespertinaNotifiedSubscription
import com.resultados.quesalio.GetVespertinaQuery
import com.resultados.quesalio.entities.Draw
import com.resultados.quesalio.extras.Config

class VespertinaViewModel : ViewModel() {
    private var items = ArrayList<Draw>()
    private var mutableList = MutableLiveData<ArrayList<Draw>>()

    fun listen(): MutableLiveData<ArrayList<Draw>> = mutableList

    fun load(response: Response<GetVespertinaNotifiedSubscription.Data>, stringDate: String?) {
        val chosenDate = Config.SDF.parse(stringDate)
        response.data()?.vespertina_modified()?.let { vespertinas ->
            val vesp = vespertinas.filter { vespertina ->
                chosenDate.equals(vespertina.date())
            }
            val result = vesp.map { vespertina ->
                val vespertinaObject = Draw(vespertina.id_draw())
                vespertinaObject.id = vespertina.id()
                vespertinaObject.city = vespertina.city()
                vespertinaObject.date = vespertina.date()
                vespertinaObject.value = vespertina.value()
                vespertinaObject.id_loteria = vespertina.id_loteria()
                vespertinaObject.meaning = vespertina.meaning()
                vespertinaObject.meaning_url = vespertina.meaning_image()
                vespertinaObject.meaning_number = vespertina.meaning_number()
                val numbers = mutableListOf<GetVespertinaQuery.Number>()
                vespertina.numbers()?.forEach {
                    numbers.add(GetVespertinaQuery.Number(it.__typename(), it.number(), it.value()))
                }
                vespertinaObject.vespertinaNumbers = numbers
                vespertinaObject
            }
            items = ArrayList(result)
            mutableList.postValue(items)
        }
    }

}