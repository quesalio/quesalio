package com.resultados.quesalio.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.apollographql.apollo.api.Response
import com.resultados.quesalio.GetNocturnaNotifiedSubscription
import com.resultados.quesalio.GetNocturnaQuery
import com.resultados.quesalio.entities.Draw
import com.resultados.quesalio.extras.Config

class NocturnaViewModel : ViewModel() {
    private var items = ArrayList<Draw>()
    private var mutableList = MutableLiveData<ArrayList<Draw>>()

    fun listen(): MutableLiveData<ArrayList<Draw>> = mutableList

    fun load(response: Response<GetNocturnaNotifiedSubscription.Data>, stringDate: String?) {
        val chosenDate = Config.SDF.parse(stringDate)
        response.data()?.nocturna_modified()?.let { nocturnas ->
            val noct = nocturnas.filter { nocturna ->
                chosenDate.equals(nocturna.date())
            }
            val result = noct.map { nocturna ->
                val nocturnaObject = Draw(nocturna.id_draw())
                nocturnaObject.id = nocturna.id()
                nocturnaObject.city = nocturna.city()
                nocturnaObject.date = nocturna.date()
                nocturnaObject.value = nocturna.value()
                nocturnaObject.id_loteria = nocturna.id_loteria()
                nocturnaObject.meaning = nocturna.meaning()
                nocturnaObject.meaning_url = nocturna.meaning_image()
                nocturnaObject.meaning_number = nocturna.meaning_number()
                val numbers = mutableListOf<GetNocturnaQuery.Number>()
                nocturna.numbers()?.forEach {
                    numbers.add(GetNocturnaQuery.Number(it.__typename(), it.number(), it.value()))
                }
                nocturnaObject.nocturnaNumbers = numbers
                nocturnaObject
            }
            items = ArrayList(result)
            mutableList.postValue(items)
        }
    }

}