package com.resultados.quesalio.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.apollographql.apollo.api.Response
import com.resultados.quesalio.GetMatutinaNotifiedSubscription
import com.resultados.quesalio.GetMatutinaQuery
import com.resultados.quesalio.entities.Draw
import com.resultados.quesalio.extras.Config

class MatutinaViewModel : ViewModel() {
    private var items = ArrayList<Draw>()
    private var mutableList = MutableLiveData<ArrayList<Draw>>()

    fun listen(): MutableLiveData<ArrayList<Draw>> = mutableList

    fun load(response: Response<GetMatutinaNotifiedSubscription.Data>, stringDate: String?) {
        val chosenDate = Config.SDF.parse(stringDate)
        response.data()?.matutina_modified()?.let { matutinas ->
            val mat = matutinas.filter { matutina ->
                chosenDate.equals(matutina.date())
            }
            val result = mat.map { matutina ->
                val matutinaObject = Draw(matutina.id_draw())
                matutinaObject.id = matutina.id()
                matutinaObject.city = matutina.city()
                matutinaObject.date = matutina.date()
                matutinaObject.value = matutina.value()
                matutinaObject.id_loteria = matutina.id_loteria()
                matutinaObject.meaning = matutina.meaning()
                matutinaObject.meaning_url = matutina.meaning_image()
                matutinaObject.meaning_number = matutina.meaning_number()
                val numbers = mutableListOf<GetMatutinaQuery.Number>()
                matutina.numbers()?.forEach {
                    numbers.add(GetMatutinaQuery.Number(it.__typename(), it.number(), it.value()))
                }
                matutinaObject.matutinaNumbers = numbers
                matutinaObject
            }
            items = ArrayList(result)
            mutableList.postValue(items)
        }
    }

}