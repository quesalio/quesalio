package com.resultados.quesalio.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.apollographql.apollo.api.Response
import com.resultados.quesalio.GetPrimeraNotifiedSubscription
import com.resultados.quesalio.GetPrimeraQuery
import com.resultados.quesalio.entities.Draw
import com.resultados.quesalio.extras.Config

class PrimeraViewModel : ViewModel() {
    private var items = ArrayList<Draw>()
    private var mutableList = MutableLiveData<ArrayList<Draw>>()

    fun listen(): MutableLiveData<ArrayList<Draw>> = mutableList

    fun load(response: Response<GetPrimeraNotifiedSubscription.Data>, stringDate: String?) {
        val chosenDate = Config.SDF.parse(stringDate)
        response.data()?.primera_modified()?.let { primeras ->
            val prim = primeras.filter { primera ->
                chosenDate.equals(primera.date())
            }
            Log.e("eee", "${prim.size}")
            val result = prim.map { primera ->
                val primeraObject = Draw(primera.id_draw())
                primeraObject.id = primera.id()
                primeraObject.city = primera.city()
                primeraObject.date = primera.date()
                primeraObject.value = primera.value()
                primeraObject.id_loteria = primera.id_loteria()
                primeraObject.meaning = primera.meaning()
                primeraObject.meaning_url = primera.meaning_image()
                primeraObject.meaning_number = primera.meaning_number()
                val numbers = mutableListOf<GetPrimeraQuery.Number>()
                primera.numbers()?.forEach {
                    numbers.add(GetPrimeraQuery.Number(it.__typename(), it.number(), it.value()))
                }
                primeraObject.primeraNumbers = numbers
                primeraObject
            }
            items = ArrayList(result)
            mutableList.postValue(items)
        }
    }

}