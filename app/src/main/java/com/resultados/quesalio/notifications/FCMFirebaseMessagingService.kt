package com.resultados.quesalio.notifications

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.jakewharton.picasso.OkHttp3Downloader
import com.resultados.quesalio.MainActivity
import com.resultados.quesalio.R
import com.squareup.picasso.Picasso
import java.util.concurrent.TimeUnit


class FCMFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        remoteMessage?.let {
            showNotification(it.data)
        }
    }

    private fun showNotification(data: Map<String, String>) {
        val message = data.get("message").toString()
        val imageUrl = data.get("image").toString()
        //Log.e("mensaje", message)
        Log.e("image", imageUrl)
        if (imageUrl.isNotEmpty() && message.isNotEmpty()) {
            /*val futureTarget = Glide.with(applicationContext)
                    .asBitmap()
                    .load(imageUrl)
                    .submit()
            val bitmap = futureTarget.get()*/
            val okHttp3Client = okhttp3.OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build()

            val okHttp3Downloader = OkHttp3Downloader(okHttp3Client)
            val picasso = Picasso.Builder(applicationContext)
                    .downloader(okHttp3Downloader)
                    .build()
            val bitmap = picasso.load(imageUrl).get()
            bitmap?.let { bit ->
                val intent = Intent(applicationContext, MainActivity::class.java)
                val pendingIntent: PendingIntent? = TaskStackBuilder.create(applicationContext).run {
                    addNextIntent(intent)
                    getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                }
                val splittedMessage = message.split("Ciudad");
                var first = message
                var second = ""
                if (splittedMessage.size > 1) {
                    first = splittedMessage[0]
                    second = "Ciudad${splittedMessage[1]}"
                }
                val vibration = longArrayOf(0, 100, 200, 300)
                val notification = NotificationCompat.Builder(applicationContext, getString(R.string.default_notification_channel_id))
                        .setVibrate(vibration)
                        .setContentTitle(first)
                        .setContentText(second)
                        .setLights(Color.RED, 500, 500)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setStyle(NotificationCompat.BigPictureStyle()
                                .bigPicture(bit)
                                .bigLargeIcon(null))
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    notification.setSmallIcon(R.drawable.notification_icon_lollipop)
                    notification.color = ContextCompat.getColor(applicationContext, R.color.notification_lollipop)
                } else {
                    notification.setSmallIcon(R.drawable.notification_icon)
                }
                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(6969, notification.build())
            }
        }
    }
}