package com.resultados.quesalio.entities

data class MeaningTypeDetail(var image: String, var number: String, var meaning: String)
data class MeaningType(var id: String, var name: String, var isName: Boolean, var details: MutableList<MeaningTypeDetail>)
