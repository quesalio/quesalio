package com.resultados.quesalio.entities

class DrawerItem {
    var name: String? = null
    var iconId: Int = 0

    constructor(name: String, iconId: Int) {
        this.name = name
        this.iconId = iconId
    }

    constructor(name: String) {
        this.name = name
    }
}
