package com.resultados.quesalio.entities

data class Game(var type: String, var prize: String, var date: String, var isTaken: Boolean) {
    var winners: String = ""
}

data class GameItem(var number: String) {
    var selected: Boolean = false
    var type: String = ""
}