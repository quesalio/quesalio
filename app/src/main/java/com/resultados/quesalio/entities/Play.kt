package com.resultados.quesalio.entities

data class Play(var type: String, var prize: String, var date: String) {
    var id: String = ""
    var numbers: String = ""
    var hits:Int=0
}