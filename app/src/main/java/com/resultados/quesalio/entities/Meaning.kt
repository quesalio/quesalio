package com.resultados.quesalio.entities

data class Meaning(var image: String, var number: String, var meaning: String, var idMeaningType: Int)
