package com.resultados.quesalio.entities


class Note {
    var id: String = ""
    var title: String = ""
    var description: String = ""
    var avatar: String = ""
    var icon: String = ""
    var url: String = ""
}
