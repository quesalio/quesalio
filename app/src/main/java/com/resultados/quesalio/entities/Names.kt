package com.resultados.quesalio.entities

class Names {
    var id: String
    var letter: String
    var name: String
    var number: String
    var idMeaningType: String

    init {
        id = ""
        number = ""
        letter = ""
        name = ""
        idMeaningType = ""
    }
}
