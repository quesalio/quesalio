package com.resultados.quesalio.entities

class Sorteo {
    var id: String
    var ciudad: String
    var primera: String
    var matutina: String
    var vespertina: String
    var nocturna: String
    var id_quiniela: String

    init {
        id_quiniela = ""
        id = ""
        ciudad = ""
        nocturna = "----"
        vespertina = nocturna
        matutina = vespertina
        primera = matutina
    }
}
