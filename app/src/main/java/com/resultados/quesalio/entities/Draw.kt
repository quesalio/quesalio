package com.resultados.quesalio.entities

import com.resultados.quesalio.GetMatutinaQuery
import com.resultados.quesalio.GetNocturnaQuery
import com.resultados.quesalio.GetPrimeraQuery
import com.resultados.quesalio.GetVespertinaQuery
import java.util.*

data class Draw(var id_draw: String? = null) {
    var id: String? = null
    var id_loteria: String? = null
    var city: String? = null
    var value: String? = null
    var meaning: String? = null
    var meaning_url: String? = null
    var meaning_number: String? = null
    var date: Date? = null
    var primeraNumbers: List<GetPrimeraQuery.Number>? = null
    var matutinaNumbers: List<GetMatutinaQuery.Number>? = null
    var vespertinaNumbers: List<GetVespertinaQuery.Number>? = null
    var nocturnaNumbers: List<GetNocturnaQuery.Number>? = null
}