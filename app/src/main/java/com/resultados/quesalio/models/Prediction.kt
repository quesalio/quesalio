package com.resultados.quesalio.models

import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Prediction")
class Prediction : ParseObject() {

    val horoscope: Horoscope
        get() = getParseObject(HOROSCOPE) as Horoscope

    val sign: String
        get() = getString(SIGN)

    val dateRange: String
        get() = getString(DATE_RANGE)

    val general: String
        get() = getString(GENERAL)

    val health: String
        get() = getString(HEALTH)

    val money: String
        get() = getString(MONEY)

    val work: String
        get() = getString(WORK)

    val love: String
        get() = getString(LOVE)

    val single: String
        get() = getString(SINGLE)

    val numberOne: String
        get() = getString(NUMBER_ONE)

    val numberTwo: String
        get() = getString(NUMBER_TWO)

    companion object {
        private val HOROSCOPE = "horoscope"
        private val SIGN = "sign"
        private val DATE_RANGE = "date_range"
        private val GENERAL = "general"
        private val HEALTH = "health"
        private val MONEY = "money"
        private val WORK = "work"
        private val LOVE = "love"
        private val SINGLE = "single"
        private val NUMBER_ONE = "number_one"
        private val NUMBER_TWO = "number_two"
    }
}
