package com.resultados.quesalio.models

import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Horoscope")
class Horoscope : ParseObject() {

    val predictionDay: String
        get() = getString(PREDICTION_DAY)

    companion object {
        private val PREDICTION_DAY = "day"
    }
}
