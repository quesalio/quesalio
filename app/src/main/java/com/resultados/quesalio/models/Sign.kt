package com.resultados.quesalio.models

import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Sign")
class Sign : ParseObject()
