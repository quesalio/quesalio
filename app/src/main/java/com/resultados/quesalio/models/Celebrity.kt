package com.resultados.quesalio.models

import com.parse.ParseClassName
import com.parse.ParseObject

@ParseClassName("Celebrity")
class Celebrity : ParseObject() {

    val sign: Sign
        get() = getParseObject(SIGN) as Sign

    val name: String
        get() = getString(NAME)

    val image: String
        get() = getString(IMAGE)

    companion object {
        private val SIGN = "sign"
        private val NAME = "name"
        private val IMAGE = "url"
    }
}
