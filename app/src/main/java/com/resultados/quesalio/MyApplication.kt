package com.resultados.quesalio


import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.preference.PreferenceManager
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.cache.http.ApolloHttpCache
import com.apollographql.apollo.cache.http.DiskLruHttpCacheStore
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.subscription.WebSocketSubscriptionTransport
import com.facebook.ads.AudienceNetworkAds
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.ads.MobileAds
import com.google.firebase.iid.FirebaseInstanceId
import com.parse.Parse
import com.parse.ParseObject
import com.resultados.quesalio.extras.Config
import com.resultados.quesalio.extras.ISO8601Adapter
import com.resultados.quesalio.extras.Utiles
import com.resultados.quesalio.models.Horoscope
import com.resultados.quesalio.models.Prediction
import com.resultados.quesalio.models.Sign
import com.resultados.quesalio.type.CustomType
import net.danlew.android.joda.JodaTimeAndroid
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.util.*


class MyApplication : MultiDexApplication() {
    lateinit var apolloClient: ApolloClient
    lateinit var utiles: Utiles

    private var mPrefs: SharedPreferences? = null
    override fun onCreate() {
        super.onCreate()
        sInstance = this
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this)
        //Fabric.with(this, Crashlytics())
        JodaTimeAndroid.init(this)
        AppEventsLogger.activateApp(this)
        ParseObject.registerSubclass(Sign::class.java)
        ParseObject.registerSubclass(Horoscope::class.java)
        ParseObject.registerSubclass(Prediction::class.java)
        Parse.initialize(Parse.Configuration.Builder(this)
                .applicationId(Config.APP_ID)
                .server(Config.SERVER_URL)
                .clientKey(Config.CLIENT_KEY)
                .build()
        )
        AudienceNetworkAds.initialize(this)
        AudienceNetworkAds.isInAdsProcess(this)
        MobileAds.initialize(this, getString(R.string.app_admob_id))
        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG)
        /*OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(CustomNotificationOpening())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()*/
        //apollo
        //val interceptor = HttpLoggingInterceptor()
        //interceptor.level = HttpLoggingInterceptor.Level.BODY

        val interceptor = Interceptor { chain ->
            val orig = chain.request()
            val builder = orig.newBuilder()
                    .method(orig.method(), orig.body())
            //.header("authorization", "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ3aGVuIjoxNTE0MjY5Mjk5NDEzLCJyb2xlIjoidXNlciIsImxhc3RMb2dpbiI6MTUxNDI2MDg1MDAwNiwidXNlcklkIjoiNWE0MWM5NTcyN2FmYjQwNmU3MzRlZTBiIiwiaWF0IjoxNTE0MjY5Mjk5LCJleHAiOjE1MjIwNDUyOTl9.VIyV8WcU_6E3cLW29913WkQk7xpoGkGCI4Qv8tmwsYwgMMCkWauROouo9KUZgBESmu74I9LhLSNU9hUGhm0DBg")
            //.header("X-User-Token", "hano")
            //.header("jwtKey", "hano")
            //.header("jwtAlgo", "HS512")
            //.addHeader("content-type", "application/json")

            chain.proceed(builder.build())
        }

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

        val cacheStore = DiskLruHttpCacheStore(cacheDir, 1024 * 1024);
        apolloClient = ApolloClient.builder()
                .serverUrl(Config.GRAPHQL_URL)
                .okHttpClient(okHttpClient)
                .httpCache(ApolloHttpCache(cacheStore))
                //.defaultHttpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(1, TimeUnit.MINUTES))
                .addCustomTypeAdapter<Date>(CustomType.DATE, ISO8601Adapter())
                .subscriptionTransportFactory(WebSocketSubscriptionTransport.Factory(Config.GRAPHQL_SUBSCRIPTION_URL, okHttpClient))
                .build()

        val firstLaunch = mPrefs?.getBoolean("first_launch", true)!!
        if (firstLaunch) {
            //OneSignal.sendTag(Config.PRIMERA_TYPE, "1")
            //OneSignal.sendTag(Config.MATUTINA_TYPE, "1")
            //OneSignal.sendTag(Config.VESPERTINA_TYPE, "1")
            //OneSignal.sendTag(Config.NOCTURNA_TYPE, "1")
            //OneSignal.sendTag("brinco", "1")
            //OneSignal.sendTag("poceada", "1")
            //OneSignal.sendTag("poceada_plus", "1")
            //OneSignal.sendTag(Config.QUINI_TYPE, "1")
            //OneSignal.sendTag(Config.LOTO_TYPE, "1")
            val editor = mPrefs?.edit()
            editor?.putBoolean("first_launch", false)
            editor?.apply()
        }
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val token = instanceIdResult?.token
            //Log.e("token", "${token}")
            MyApplication.sInstance.apolloClient.mutate(
                    SaveInstallationMutation.builder()
                            .token(token)
                            .build()).enqueue(object : ApolloCall.Callback<SaveInstallationMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    //Log.e("failure", "${e.message}")

                }

                override fun onResponse(response: Response<SaveInstallationMutation.Data>) {
                    response.data()?.save_installation()?.channels()?.let {
                        if (it.size > 0) {
                            val editor = mPrefs?.edit()
                            editor?.putStringSet("channels", it.toMutableSet())
                            //editor?.putBoolean("is_subscribed", true)
                            editor?.apply()
                        }

                    }
                }
            })
        }


        /*ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        onNewMessage = Emitter.Listener { args ->
            runOnUiThread {
                try {
                    val jsonObject = args[0] as JSONObject
                    if (!jsonObject.isNull("image") && !jsonObject.isNull("message")) {
                        val draw = jsonObject.getString("draw")
                        val image = jsonObject.getString("image")
                        val message = jsonObject.getString("message")
                        buildNotification(image, message, draw)
                    }
                    Log.e("json", jsonObject.toString())
                } catch (ignored: Exception) {
                    Log.e("exce", ignored?.message)
                }
            }
        }
        mSocket = IO.socket(Config.NOTIFICATIONS_URL)
        mSocket?.on(notificationTag, onNewMessage)
        mSocket?.connect()
        */
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //val channelId = "com.resultadosquesalio.ANDROID"
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(getString(R.string.default_notification_channel_id), name, importance).apply {
                description = descriptionText
                lightColor = Color.RED
                vibrationPattern = longArrayOf(0, 1000, 500, 1000)
                enableVibration(true)
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun checkSubscriptions() {
        /*val primera = FirebaseMessaging.getInstance().subscribeToTopic(Config.PRIMERA_TYPE).isComplete
        val matutina = FirebaseMessaging.getInstance().subscribeToTopic(Config.MATUTINA_TYPE).isComplete
        val vespertina = FirebaseMessaging.getInstance().subscribeToTopic(Config.VESPERTINA_TYPE).isComplete
        val nocturna = FirebaseMessaging.getInstance().subscribeToTopic(Config.NOCTURNA_TYPE).isComplete
        Log.e("primera", "$primera $matutina $vespertina $nocturna")
        if (primera && matutina && vespertina && nocturna) {*/
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val token = instanceIdResult?.token
            Log.e("token", token)
            MyApplication.sInstance.apolloClient.mutate(
                    SaveInstallationMutation.builder()
                            .token(token)
                            .build()).enqueue(object : ApolloCall.Callback<SaveInstallationMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.e("failure", "${e.message}")
                }

                override fun onResponse(response: Response<SaveInstallationMutation.Data>) {
                    response.data()?.save_installation()?.channels()?.let {
                        val editor = mPrefs?.edit()
                        editor?.putStringSet("channels", it.toMutableSet())
                        editor?.apply()
                    }
                    response.data()?.save_installation()?.token().let {
                        //token
                        Log.e("token", token)
                    }
                }

            })

        }
        //}
    }
    /*private fun buildNotification(image: String?, message: String?, draw: String?) {
        val target = object : com.squareup.picasso.Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.e("error", "errrororororor")
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                val intent = Intent(applicationContext, MainActivity::class.java)
                val pendingIntent = PendingIntent.getActivity(applicationContext, 100, intent, PendingIntent.FLAG_ONE_SHOT)
                val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notif = Notification.Builder(applicationContext)
                        .setContentIntent(pendingIntent)
                        .setContentTitle(message)
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(bitmap)
                        .setStyle(Notification.BigPictureStyle().bigPicture(bitmap))
                        .build()
                notif.flags = Notification.FLAG_AUTO_CANCEL
                //notif.flags |= Notification.FLAG_AUTO_CANCEL;
                notificationManager.notify(1, notif)
            }
        }
        Log.e("image", "${Config.NOTIFICATIONS_URL}/$image")
        Picasso.with(this).load("${Config.NOTIFICATIONS_URL}/$image").into(target)
    }
    */


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        lateinit var sInstance: MyApplication
            private set
    }

    public fun getSharedPreferences(): SharedPreferences? {
        return mPrefs;
    }


    /*@OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroyListener() {
        mSocket?.disconnect();
        mSocket?.off(notificationTag, onNewMessage)
    }*/
}